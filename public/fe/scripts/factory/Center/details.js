app.factory('detailsFactory', function($http, $q, Config){
    return {
         loadlocation: function(centerslugs,callback){
            $http({
                url: Config.ApiURL + "/fe/center/page/"+ centerslugs,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
         },
         loadworkshoplocation: function(workshopid, callback) {
            $http({
                url: Config.ApiURL + "/workshop/location/"+ workshopid,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
         }
    }  
})