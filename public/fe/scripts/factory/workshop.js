app.factory('WorkshopFactory', function($http, Config){
	return {
     	lists: function(callback) {
			$http({
				url: Config.ApiURL + "/workshop/lists",
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function (data, status, headers, config) {
				data = data;
                callback(data);
			});
		},
		loadcity: function(statecode, callback) {
			$http({
				url: Config.ApiURL + "/workshop/loadcity/viastatecode/" + statecode,
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function (data, status, headers, config) {
				data = data;
                callback(data);
			});
		},
		schedulelist: function (titleslugs, offset, filter, callback) {
			$http({
				url: Config.ApiURL + "/workshop/schedulelist/"+titleslugs+"/"+offset,
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(filter)
			}).success(function (data, status, headers, config) {
				data = data;
                callback(data);
			});
		},
		workshopschedulepagination: function(workshopS, titleslugs, offset, callback) {
			workshopS['titleslugs'] = titleslugs;
			workshopS['offset'] = offset;

			$http({
				url: Config.ApiURL + "/workshop/schedule/pagination",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(workshopS)
			}).success(function (data, status, headers, config) {
				data = data;
                callback(data);
			});
		},
		registrationinfo: function(workshopid, callback) {
			$http({
				url: Config.ApiURL + "/workshop/registrationinfo/" + workshopid,
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function (data, status, headers, config) {
				data = data;
                callback(data);
			});
		},
		registrant: function(wsreg, callback) {
			$http({
				url: Config.ApiURL + "/workshop/registrant",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(wsreg)
			}).success(function (data, status, headers, config) {
				data = data;
                callback(data);
			});
		},
		overview: function(callback){
			$http({
				url: Config.ApiURL + "/workshop/overview",
				method: "GET",
			}).success(function (data, status, headers, config) {
				data = data;
                callback(data);
			});
		},
		
	} 
})
