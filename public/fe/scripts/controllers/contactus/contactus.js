'use strict';

app.controller('Sendctrl', function ($scope, $http, Config, CenterFactory) {

	$scope.state_code = function (state_code) {
  		CenterFactory.loadCenterViaState(state_code, function(data) {
  			$scope.centers = data;
  		});
  		CenterFactory.pagedata(centerslugs, function(data) {
  			$scope.testimony.center = data.centerid;
  		})
  	}
  	
  // LOAD THE CENTER LIST from FACTORY
	CenterFactory.FEloadstate(function(data) {
		$scope.states = data;
	});

	$scope.centerViaState = function(state_code) {
		CenterFactory.loadCenterViaState(state_code, function(data) {
			$scope.centers = data;
		});
	}	
	
	$scope.submitted = false;
	var captcha = document.getElementById("txtCaptcha").value;
	$scope.contactsend = function(form){
		$scope.submitted = true;
		if(captcha == $scope.str2){
			$http({
				url: Config.ApiURL + "/contactsend/form",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(form)
			}).success(function (data, status, headers, config) {
				$scope.contact="";
				$scope.str2 = "";
				$scope.str1 = "";
				$scope.cOntactus.$setPristine(true);
				Materialize.toast('Thank you for Contacting Us! Message Sent!', 2500);
				setTimeout("location.reload(true);",1000);
			}).error(function (data, status, headers, config) {
				Materialize.toast('WE BROKE SOMETHING!', 4000);
			});
		}
		else{
			Materialize.toast('Captcha code is Wrong!', 4000);
		}
	}

})