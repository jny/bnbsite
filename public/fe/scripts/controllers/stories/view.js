'use strict';

app.controller("viewstoryCtrl", function ($scope, Config, SuccessFactory) {
	var url = window.location.href;
    var schedulepage = url.match(/\/successstories\/view\/(.*)+/);
    var removelastslash = schedulepage['input'].replace(/\/$/, '');
    var explodedurl = removelastslash.split("/");
    var tagname = explodedurl[5].replace(/%20+/g, " ");
    console.log(tagname);

    SuccessFactory.storydetails(tagname, function(data){
    	$scope.story = data.story;
    	$scope.relatedstory = data.relatedstory;
    });

})