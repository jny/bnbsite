'use strict';

/* Controllers */

app.controller('BeneplaceCtrl', function($scope, $http, Config, _BeneplaceFactory, anchorSmoothScroll,deviceDetector,$location,$window){
 
 _BeneplaceFactory.viewreferrallink(function(data){
  if (document.referrer != data.link) {
    console.log(data.link);
    $window.location.href = "/";
  }
})
 



	$scope.firststep = true;
 //  $scope.secondstep = true;
 //  $scope.thirdstep = true;
	// $scope.invalidpaypal = true;

	var sessionclass = '';
	var sessiontype = '';
	var centerid = '';
	var centertitle = '';
	var centeraddress = '';
	var centerzip = '';
	var centeremail = '';
	var centerphone = '';
	var itemprice = 0;
	var totalprice = 0;
	var fullname = '';
	var email = '';
	var phone1 = '';
	var phone2 = '';
	var phone3 = '';
	var classday = '';
	var classhour = '';
	var comment = '';
	var qty = 1;
	var sumdate = '';
	var latitude = '';
	var longitude = '';
	var device = '';
	var deviceos = '';
	var devicebrowser = '';
	var paypalid = '';
	var authorizeid = '';
	var authorizekey = '';
	var introofflinerate = '';
	var introonlinerate = '';
	var groupofflinerate = '';
	var grouponlinerate = '';
	var finalonlinerate = '';
	var myloclat = '';
	var myloclon = '';

  var orig1monthprice = '';
  var dis1monthprice = '';
  var orig3monthprice ='';
  var dis3monthprice = '';
  var dummy = '';

	if(deviceDetector.isDesktop() == true){
		device = 'desktop';
	}
	else{
		device = deviceDetector.device;
	}

  deviceos = deviceDetector.os;
  devicebrowser = deviceDetector.browser;

  $scope.backtosecondstep = function(){
    sessionclass = '';
    $scope.firststep = false;
    $scope.secondstep = true;
    $scope.thirdstep = false;
    $scope.result = false;
    $scope.paymentcheck = false;
    $scope.btnpickcenter = false;
    anchorSmoothScroll.scrollTo('gototop');
    $scope.itemprice = '';
    qty = 1;
    $scope.itemquantity = 1;
    $scope.totalprice = '';
  }

  $scope.backtofirststep = function(){
    sessionclass = '';
    $scope.firststep = true;
    $scope.secondstep = false;
    $scope.thirdstep = false;
    $scope.result = false;
    $scope.btnpickcenter = false;
    $scope.paymentcheck = false;
    $scope.itemprice = '';
    $scope.totalprice = '';
    qty = 1;
    $scope.itemquantity = 1;
    anchorSmoothScroll.scrollTo('gototop');
  }

   $scope.gotoschedule = function(){
    if(centertitle != ''){
      $scope.firststep = false;
      $scope.secondstep = true;
      anchorSmoothScroll.scrollTo('gototop');
    }
    else{
      $scope.pickcentermsg = 'Please Select Center First!';
    }
  }


  	var validatepaypalbutton = function(){
    if(fullname == '' || centerid == '' || email == '' ||  phone1 == '' || phone2 == ''|| phone3 == '' || classday == '' || classhour == '' || qty == '' ){

      $scope.validpaypal = false;
      $scope.invalidpaypal = true;
      $scope.device = device;
      $scope.deviceos = deviceos;
      $scope.devicebrowser = devicebrowser;
      $scope.lon = longitude;
      $scope.lat = latitude;
    }
    else
    {
      $scope.validpaypal = true;
      $scope.invalidpaypal = false;
      $scope.classhourwarning = false;
      $scope.device = device;
      $scope.deviceos = deviceos;
      $scope.devicebrowser = devicebrowser;
      $scope.lon = longitude;
      $scope.lat = latitude;
      $scope.sessionclass = sessionclass;
      $scope.sessiontype = sessiontype;
      $scope.finalonlinerate = finalonlinerate;


    }
  }

  $scope.putname = function(name){
    console.log(name);
    fullname = name;
    $scope.submitfullname = fullname;
    $scope.submitcenterid = centerid;
    validatepaypalbutton();
  }

  $scope.putemail = function(txtemail){
    email = txtemail;
    $scope.submitemail = email;
    $scope.submitcenterid = centerid;
    validatepaypalbutton();
  }

  $scope.putphone1 = function(txtphone1){
    phone1 = txtphone1;
    $scope.submitphonenumber  = '('+phone1+') '+phone2+'-'+phone3;
    validatepaypalbutton();
  }

  $scope.putphone2 = function(txtphone2){
    phone2 = txtphone2;
    $scope.submitphonenumber  = '('+phone1+') '+phone2+'-'+phone3;
    validatepaypalbutton();
  }

  $scope.putphone3 = function(txtphone3){
    phone3 = txtphone3;
    $scope.submitphonenumber  = '('+phone1+') '+phone2+'-'+phone3;
    validatepaypalbutton();
  }

  $scope.puttime = function(txttime){
  
    classhour = txttime;
    $scope.submittime = classhour;
    validatepaypalbutton();
  }

  $scope.putcomment = function(txtcomment){
    comment = txtcomment;
    $scope.submitcomment = comment;
    validatepaypalbutton();
  }

	// navigator.geolocation.getCurrentPosition(function(pos) {
	// 	myloclat = pos.coords.latitude;
	// 	myloclon = pos.coords.longitude;
	// });

	$scope.searchcenter = function(search){
	  angular.element('.chrono').removeClass('hidden');
	  if(search != undefined){
	    _BeneplaceFactory.searchlocation(search, function(data){
	 
	      if(data.length == 0){
	        $scope.result = false;
	        $scope.noresults = true;
	        angular.element('.chrono').addClass('hidden');
	      }
	      else{
	        $scope.result = true;
	        $scope.noresults = false;
	       
	        var markers = [];
	        $scope.randomMarkers = [];
	        $scope.map = {center: {latitude: data[0].lat, longitude: data[0].lon }, zoom: 7 };
	        $scope.options = {scrollwheel: true};

	        markers.push({id: 'Location',latitude: myloclat, longitude: myloclon, title: 'My Current Location ',icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|69b9de'})
	        var newcenterlist = []
	        var items = 0

	        var distance =  function(lat1, lon1, lat2, lon2, unit) {
	          var radlat1 = Math.PI * lat1/180
	          var radlat2 = Math.PI * lat2/180
	          var radlon1 = Math.PI * lon1/180
	          var radlon2 = Math.PI * lon2/180
	          var theta = lon1-lon2
	          var radtheta = Math.PI * theta/180
	          var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
	          dist = Math.acos(dist)
	          dist = dist * 180/Math.PI
	          dist = dist * 60 * 1.1515
	          return parseInt(dist, 10)
	        }

	      


	        data.forEach(function(list){
	            newcenterlist.push({
	              authorizeid: list.authorizeid,
	              authorizekey: list.authorizekey,
	              centeraddress: list.centeraddress,
	              centercity: list.centercity,
	              centerdistrict: list.centercity,
	              centerid: list.centerid,
	              centermanager: list.centermanager,
	              centerregion: list.centerregion,
	              centerslugs: list.centerslugs,
	              centerstate: list.centerstate,
	              centertelnumber: list.centertelnumber,
	              centertitle: list.centertitle,
	              centertype: list.centertype,
	              centerzip: list.centerzip,
                orig1monthprice: list.orig1monthprice,
                dis1monthprice: list.dis1monthprice,
                orig3monthprice: list.orig3monthprice,
                dis3monthprice: list.dis3monthprice,
	              datecreated: list.datecreated,
	              dateedited: list.dateedited,
	              email: list.email,
	              lat: list.lat,
	              locationid: list.locationid,
	              lon: list.lon,
	              metadesc: list.metadesc,
	              metatitle: list.metatitle,
	              paypalid: list.paypalid,
	              phonenumber: list.phonenumber,
	              status: list.status,
	              // 'distance':distance(myloclat,myloclon,list.lat,list.lon,'M')
                distance: list.fdistance
	            })

	            items = items+1;
	        })

	         $scope.locationlist = newcenterlist;
	         console.log(newcenterlist);

	        data.forEach(function(entry) {
	          markers.push({id: entry.centerid,latitude: entry.lat, longitude: entry.lon, title: 'Address ' + entry.centertitle + ': ' +  entry.centeraddress + ', ' + entry.centerzip})
	        });

	        

	        $scope.randomMarkers = markers;
	        angular.element('.chrono').addClass('hidden');
	        $scope.result = true;
	      }





	      }); //end of fucktory

	  }
	  else{
	    $scope.locationresults = false;
	    $scope.noresults = true;
	    angular.element('.chrono').addClass('hidden');
	  }

	}

	$scope.centerpick = function(centeridpick,centertitlepick,centeraddresspick,centerzippick,centerphonepick,centerlat,centerlon,pickcenteremail,pickpaypalid,pickauthorizeid,pickauthorizekey,pickorig1monthprice,pickdis1monthprice,pickorig3monthprice,pickdis3monthprice){
 
    centerid = centeridpick;
    centertitle = centertitlepick;
    centeraddress = centeraddresspick;
    centerzip = centerzippick;
    centerphone = centerphonepick;
    latitude = centerlat;
    longitude = centerlon;
    centeremail = pickcenteremail;
    paypalid = pickpaypalid;
    authorizeid = pickauthorizeid;
    authorizekey = pickauthorizekey;

    orig1monthprice = pickorig1monthprice;
    dis1monthprice =  pickdis1monthprice;
    orig3monthprice = pickorig3monthprice;
    dis3monthprice = pickdis3monthprice;

    $scope.orig1monthprice = orig1monthprice;
    $scope.dis1monthprice =  dis1monthprice;
    $scope.orig3monthprice = orig3monthprice;
    $scope.dis3monthprice = dis3monthprice;

    $scope.paypalid = paypalid;
    $scope.authorizeid = authorizeid;
    $scope.authorizekey = authorizekey;
    $scope.centeremail = centeremail;
    $scope.centertitle = centertitle;
    $scope.centerid = centerid;
    $scope.centeraddress = centeraddress;
    $scope.centerzip = centerzip;
    $scope.centerphone = centerphone;
    $scope.centerlon = longitude;
    $scope.centerlat = latitude;
    $scope.firststep = false;
    $scope.secondstep = true;
    anchorSmoothScroll.scrollTo('gototop');
    loadschedule(centerid);
  }

  $scope.centerradiopick = function(centeridpick,centertitlepick,centeraddresspick,centerzippick,centerphonepick,centerlat,centerlon,pickcenteremail,pickpaypalid,pickauthorizeid,pickauthorizekey,pickorig1monthprice,pickdis1monthprice,pickorig3monthprice,pickdis3monthprice){
    console.log(pickdis1monthprice);
    centerid = centeridpick;
    centertitle = centertitlepick;
    centeraddress = centeraddresspick;
    centerzip = centerzippick;
    centerphone = centerphonepick;
    latitude = centerlat;
    longitude = centerlon;
    centeremail = pickcenteremail;
    paypalid = pickpaypalid;
    authorizeid = pickauthorizeid;
    authorizekey = pickauthorizekey;

    orig1monthprice = pickorig1monthprice;
    dis1monthprice =  pickdis1monthprice;
    orig3monthprice = pickorig3monthprice;
    dis3monthprice = pickdis3monthprice;

    $scope.orig1monthprice = orig1monthprice;
    $scope.dis1monthprice =  dis1monthprice;
    $scope.orig3monthprice = orig3monthprice;
    $scope.dis3monthprice = dis3monthprice;

    console.log(dis1monthprice + '/' + dis3monthprice);

    $scope.paypalid = paypalid;
    $scope.authorizeid = authorizeid;
    $scope.authorizekey = authorizekey;
    $scope.centeremail = centeremail;
    $scope.centerlon = longitude;
    $scope.centerlat = latitude;
    $scope.btnpickcenter = true;
    $scope.centertitle = centertitle;
    $scope.centerid = centerid;
    $scope.centeraddress = centeraddress;
    $scope.centerzip = centerzip;
    $scope.centerphone = centerphone;
    loadschedule(centerid);

  }

   var loadschedule = function(centerid){
    _BeneplaceFactory.loadschedule(centerid, function(data){
      $scope.schedlelist = data;
    });
  }

  $scope.selectsession = function(session){

    if(session == '1'){
      sessionclass = '1 MONTH';
      $scope.sessionclass = sessionclass;
      $scope.classhour = true;
      sessiontype = 0;
      finalonlinerate = dis1monthprice;
      $scope.finalonlinerate = finalonlinerate;
      totalprice = qty * finalonlinerate;
      $scope.totalprice = qty * finalonlinerate;
      $scope.itemprice = dis1monthprice;
    }
    else{
      sessionclass = '3 MONTHS';
      $scope.sessionclass = sessionclass;
      $scope.classhour = false;
      sessiontype = 1;
      finalonlinerate = dis3monthprice;
      $scope.finalonlinerate = finalonlinerate;
      totalprice = qty * finalonlinerate;
      $scope.totalprice = qty * finalonlinerate;
      $scope.itemprice = dis3monthprice;
    }
    
    $scope.thirdstep = true;
    $scope.secondstep = false;
     validatepaypalbutton();
    anchorSmoothScroll.scrollTo('gototop');
  }

  function isPastDate(value) {
    var now    = new Date;
    var target = new Date(value);

    if (now.getFullYear() > target.getFullYear()) {
      return true;
    }
    else if (now.getMonth() > target.getMonth()) {
      return true;
    }
    else if (now.getDate() > target. getDate()) {
      return true;
    }

    return false;
  }


//   $scope.checkdate = function(scheduledate){
//     var date = new Date(scheduledate);
//     var finaldate = date.getFullYear()+'-'+("0" + (date.getMonth()+1)).slice(-2)+'-'+(date.getUTCDate()+1);
//     var datetoday = new Date();
//     var finaldatetoday = datetoday.getFullYear()+'-'+("0" + (date.getMonth()+1)).slice(-2)+'-'+(datetoday.getDate()+1);
//     sumdate = finaldate;
//     if(isPastDate(scheduledate) == true){
//      $scope.dateerror = true;
//      $scope.dateerrormsg = 'Please choose a date after ' + finaldatetoday;
//      $scope.validpaypal = false;
//      $scope.invalidpaypal = true;
//      // $scope.scheduleform.txtDate.$setValidity("maxLength",false);
//    }
//    else{
//     $scope.dateerror = false;
//     classday = scheduledate;
   
//     $scope.submitschedday = classday;
//     // $scope.scheduleform.txtDate.$setValidity("maxLength",true);
//     validatepaypalbutton();

//     if(date.getDay() == 0){
//       $scope.today = 'Sunday';
//     }
//     else if(date.getDay() == 1){
//       $scope.today = 'Monday';
//     }
//     else if(date.getDay() == 2){
//       $scope.today = 'Tuesday';
//     }
//     else if(date.getDay() == 3){
//       $scope.today = 'Wednesday';
//     }
//     else if(date.getDay() == 4){
//       $scope.today = 'Thursday';
//     }
//     else if(date.getDay() == 5){
//       $scope.today = 'Friday';
//     }
//     else if(date.getDay() == 6){
//       $scope.today = 'Saturday';
//     }

//     loadschedule(centerid);
    
//   }
// }

$scope.checkdate = function(scheduledate){
    classhour = '';
    var inputDate = new Date(scheduledate);
    var todaysDate = new Date();
    if(inputDate.setHours(0,0,0,0) < todaysDate.setHours(0,0,0,0))
    {
       $scope.dateerror = true;
     $scope.dateerrormsg = 'Please choose a date after ' + finaldatetoday;
     $scope.validpaypal = false;
     $scope.invalidpaypal = true;
     $scope.scheduleform.txtDate.$setValidity("maxLength",false);
    }
    else{
     $scope.dateerror = false;
     classday = scheduledate;

     $scope.submitschedday = classday;
     $scope.scheduleform.txtDate.$setValidity("maxLength",true);
     validatepaypalbutton();

     var date = new Date(scheduledate);
     var finaldate = scheduledate;
     var datetoday = new Date();
     var finaldatetoday = scheduledate;
     sumdate = finaldate;

    if(date.getDay() == 0){
      $scope.today = 'Sunday';
    }
    else if(date.getDay() == 1){
      $scope.today = 'Monday';
    }
    else if(date.getDay() == 2){
      $scope.today = 'Tuesday';
    }
    else if(date.getDay() == 3){
      $scope.today = 'Wednesday';
    }
    else if(date.getDay() == 4){
      $scope.today = 'Thursday';
    }
    else if(date.getDay() == 5){
      $scope.today = 'Friday';
    }
    else if(date.getDay() == 6){
      $scope.today = 'Saturday';
    }

    loadschedule(centerid);
    }


   
   //  if(isPastDate(scheduledate) == true){
    
   // }
   // else{
   
    
  // }

}

$scope.calculateprice = function(itemqty){
	console.log(itemqty);
  qty = itemqty;
  $scope.itemprice = finalonlinerate;
  $scope.totalprice = itemqty * finalonlinerate;
  console.log(itemqty * finalonlinerate);
  totalprice = itemqty * finalonlinerate;
  $scope.itemquantity = qty;
  validatepaypalbutton();
}

$scope.paymentsubmit = function(){
  if(classhour != ''){
    $scope.thirdstep = false;
    $scope.paymentcheck = true;
    $scope.card.billfullname = fullname;
    $scope.card.billphone = '('+phone1+') '+phone2+'-'+phone3;

    $scope.sumsession = sessionclass;
    $scope.sumname = fullname;
    $scope.sumcentername = centertitle;
    $scope.sumlocation = centeraddress;
    $scope.sumzip = centerzip;
    $scope.sumdate = sumdate;
    $scope.sumtime = classhour;
    $scope.sumtotal = totalprice;
    anchorSmoothScroll.scrollTo('gototop');
     $scope.classhourwarning = false;
  }
  else{
    anchorSmoothScroll.scrollTo('gotooffer_sche');
    $scope.classhourwarning = true;
  }
}

$scope.submitcheckout = function(card){
  console.log($scope.acceptterm);
  if($scope.acceptterm == false){
    $scope.checkthis = true;
    anchorSmoothScroll.scrollTo('gototerm');
  }
  else{
    $scope.checkthis = false;
      angular.element('.chrono').removeClass('hidden');


      card['centerid'] = centerid;
      card['centertitle'] = centertitle;
      card['centeraddress'] = centeraddress;
      card['centerphone'] = centerphone;
      card['fullname'] = fullname;
      card['email'] = email;
      card['phone'] = '('+phone1+') '+phone2+'-'+phone3;
      card['classday'] = classday;
      card['classhour'] = classhour;
      card['comment'] = comment;
      card['qty'] = qty;
      card['totalprice'] = totalprice;
      card['itemprice'] = finalonlinerate;
      card['sessionclass'] = sessionclass;
      card['device'] = device;
      card['deviceos'] = deviceos;
      card['devicebrowser'] = devicebrowser;
      card['sessiontype'] = sessiontype;
      card['authorizeid'] = authorizeid;
      card['authorizekey'] = authorizekey;
      card['moduletype'] = 'beneplace';
     console.log(card);

      _BeneplaceFactory.authorize(card,function(data){
        console.log(data);

        if(data[0] != '1' && data[2] == '6'){
          $scope.cardmsg = data[3];
          angular.element('.chrono').addClass('hidden');
          anchorSmoothScroll.scrollTo('gototop');
        }
        else if(data[0] != '1' && data[2] == '8'){
          $scope.cardmsg = data[3];
          angular.element('.chrono').addClass('hidden');
          anchorSmoothScroll.scrollTo('gototop');
        }
        else if(data[0] == '1'){
          angular.element('.chrono').addClass('hidden');
          anchorSmoothScroll.scrollTo('gototop');
          $scope.paymentcheck = false;
          $scope.finalstep = true;
          $scope.fullname = fullname;
          $scope.centerphone = centerphone;
          $scope.centertitle = centertitle;
          $scope.centeraddress = centeraddress;
          $scope.centerzip = centerzip;
          $scope.centeremail = centeremail;
          $scope.scheddate = new Date(sumdate);
          $scope.schedhour = classhour;
          $scope.totalprice = totalprice;
          $scope.confirmnumber = data[6];
          $scope.paymenttype = data[51];
          $scope.sessionclass = sessionclass;

          var markers = [];
          $scope.randomMarkers = [];
          $scope.map = {center: {latitude: latitude, longitude: longitude }, zoom: 18 };
          $scope.options = {scrollwheel: true};
          
          markers.push({id: centerid,latitude: latitude, longitude: longitude, title: 'Address ' + centertitle + ': ' +  centeraddress + ', ' + centerzip})

          $scope.randomMarkers = markers;


          
        }
        else{
          $scope.cardmsg = 'Something went wrong please try again later!';
          angular.element('.chrono').addClass('hidden');
          anchorSmoothScroll.scrollTo('gototop');
        }

      });
}

}



});

