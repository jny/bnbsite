'use strict';

app.controller("pageCtrl", function ($scope, Config, PageFactory ){
	PageFactory.initialawakening(function (data) {
		if(data.initawakening.sale == 0 || data.initawakening.sale == '' || data.initawakening.sale == null) {
			$scope.sale = false;
		} else {
			$scope.sale = true;
		}
		if(data.date_s == undefined) {
			$scope.existing = false;
		} else {
			$scope.initawakening = data.initawakening;
			$scope.date = data.date_s;
			$scope.existing = true;
		}
	});

	var configulr = Config.BaseURL;
	var re = new RegExp(configulr+'\/(.*)');
  	var url = window.location.href;
 	var auth = url.match(re);

 	var related = auth[1].substring(auth[1].lastIndexOf('/')+1);
 	console.log(related);

	if(auth != null || auth != '' ) {
		PageFactory.related(related, function (data) {
			$scope.stories = data.stories;
			if(data.articles == null || data.articles == '' ) {
				$scope.norelated = true;
			} else {
				$scope.norelated = false;
				$scope.articles = data.articles;
			}
		});
	}
})