'use strict';

/* Controllers */

app.controller('mapCtrl', function($scope, $http, Config, uiGmapGoogleMapApi, detailsFactory, $timeout){

  console.log('+++++++++++++++++heyaaa');
  var configulr = Config.regexURL;
  var re = new RegExp(configulr+'\/(.*)');
  var re2 = new RegExp(configulr+'\/(.*)/');
  var re3 = new RegExp(configulr+'\/(.*)');
  var url = window.location.href;
  var auth = url.match(re);
  var auth2 = url.match(re2);
  var auth3 = url.match(/\/workshops\/view\/detail\/(.*)+/);
  var confirmation = url.match(/\/workshops\/registration\/(.*)+/);
  var datalat = '';
  var datalon = '';
  if(auth != null) {

   detailsFactory.loadlocation(auth[1],function(data){
    $scope.openmap = function(){

    if(data.centerlocation.lat == ''){
      datalat = '39.8422862102958';
      datalon = '-104.95239295312501';
      $scope.map = {center: {latitude: datalat, longitude: datalon }, zoom: 17 };
      $scope.options = {scrollwheel: true};
      $scope.coordsUpdates = 0;
      $scope.dynamicMoveCtr = 0;
      $scope.marker = {
        id: 0,
        coords: {
          latitude: datalat,
          longitude: datalon
        },

      options: { 
        draggable: false
      },

      }
    }else{
      datalat = data.centerlocation.lat;
      datalon = data.centerlocation.lon;
     
      $scope.map = {center: {latitude: datalat, longitude: datalon }, zoom: 17 };
      $scope.options = {scrollwheel: true};
      $scope.coordsUpdates = 0;
      $scope.dynamicMoveCtr = 0;
      $scope.marker = {
        id: 0,
        coords: {
          latitude: datalat,
          longitude: datalon
        },

      options: { 
        draggable: false 
      }

      }

      } //end of else

      }  //end openmap
   })
}

if(auth2 != null) {

detailsFactory.loadlocation(auth2[1],function(data){

 
    if(data.centerlocation.lat == ''){
      datalat = '39.8422862102958';
      datalon = '-104.95239295312501';
      $scope.map = {center: {latitude: datalat, longitude: datalon }, zoom: 17 };
      $scope.options = {scrollwheel: true};
      $scope.coordsUpdates = 0;
      $scope.dynamicMoveCtr = 0;
      $scope.marker = {
        id: 0,
        coords: {
          latitude: datalat,
          longitude: datalon
        },

      options: { 
        draggable: false
      },

      }
    }else{
      datalat = data.centerlocation.lat;
      datalon = data.centerlocation.lon;
     
      $scope.map = {center: {latitude: datalat, longitude: datalon }, zoom: 17 };
      $scope.options = {scrollwheel: true};
      $scope.coordsUpdates = 0;
      $scope.dynamicMoveCtr = 0;
      $scope.marker = {
        id: 0,
        coords: {
          latitude: datalat,
          longitude: datalon
        },

      options: { 
        draggable: false 
      }

      }

      } //end of else

 
      

   })

   
}

  var workshopmap = function(workshopid) {
      detailsFactory.loadworkshoplocation(workshopid,function(data){
     

        if(data.workshoplocation.latitude != null){
            datalat = data.workshoplocation.latitude;
            datalon = data.workshoplocation.longtitude;
        }
        else{
            datalat = data.workshoplocation.lat;
            datalon = data.workshoplocation.lon;
        }
        
       
        $scope.map = {center: {latitude: datalat, longitude: datalon }, zoom: 17 };
        $scope.options = {scrollwheel: true};
        $scope.coordsUpdates = 0;
        $scope.dynamicMoveCtr = 0;
        $scope.marker = {
          id: 0,
          coords: {
            latitude: datalat,
            longitude: datalon
          },
          options: { 
            draggable: false 
          }
        }
  

        $scope.openworkshopmap = function() {


           if(data.workshoplocation.latitude != null){
            datalat = data.workshoplocation.latitude;
            datalon = data.workshoplocation.longtitude;
        }
        else{
            datalat = data.workshoplocation.lat;
            datalon = data.workshoplocation.lon;
        }
        
       
        $scope.map2 = {center2: {latitude: datalat, longitude: datalon }, zoom2: 17 };
        $scope.options2 = {scrollwheel: true};
        $scope.coordsUpdates2 = 0;
        $scope.dynamicMoveCtr2 = 0;
        $scope.marker2 = {
          id2: 0,
          coords2: {
            latitude: datalat,
            longitude: datalon
          },
          options2: { 
            draggable: false 
          }
        }

        }

       })


  }

  if(auth3 != null) {
    workshopmap(auth3[1]);
    
  }



})