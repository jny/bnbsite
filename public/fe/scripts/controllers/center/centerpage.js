
app.controller("centerpageCtrl", function($scope, Config, CenterFactory,$window, store,$localStorage){

	var configulr = Config.BaseURL;
  	var re = new RegExp(configulr+'\/(.*)');
  	var url = window.location.href;
 	var auth = url.match(re);
 	var centerslugs = auth[1];

 	store.set('centerslugs', centerslugs);

 	CenterFactory.pagedata(centerslugs, function (data) {

 		$scope.offers = data.offers;
 		$scope.offersLen = data.offers.length;

		console.log(data.offers.length)

 		if(data.sociallinks.length > 0) {
 			$scope.socialexist = true;
 			$scope.sociallinks = data.sociallinks;
 			angular.forEach(data.sociallinks, function(value, key) {
 					if(value.title === 'facebook') {
 						$scope.facebookurl = value.linkurl
 					} else if (value.title === 'twitter') {
 						$scope.twitterurl = value.linkurl;
 					} else if (value.title === 'google') {
 						$scope.googleurl = value.linkurl;
 					} else if (value.title === 'yelp') {
 						$scope.yelpurl = value.linkurl;
 					}
 			})
 		}
 	});

 	$scope.windowopen = function(offerid){
 		store.set('offerid', offerid);
 		$window.open(Config.BaseURL+'/center/viewoffer', 'C-Sharpcorner', 'width=1024,height=900');
 	}





});
