'use strict';

/* Controllers */

app.controller('MainCtrl', function($scope, Login, store, $window){

    console.log('==== Main Page ====');

    Login.getUserFromToken(function(data){
        if(data.hasOwnProperty('username')){
            $scope.loginmenu = data.username;
        }else{
            $scope.loginmenu = '';
        }
    })

    $scope.logout = function(){
        store.remove('jwt');
        $window.location = '/';
    }

})