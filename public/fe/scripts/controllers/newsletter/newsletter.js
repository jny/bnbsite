'use strict';

app.controller('Newsletterctrl', function ($scope, $http, Config) {
	
	console.log("HELLO");

	$http({
		url: Config.ApiURL + "/fe/enewsletter/popularnews/listsuccesssotries",
		method: "GET",
		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
	}).success(function (data, status, headers, config) {
		$scope.printmainnews = data.mainnews;
		$scope.dataz = data.storyprop;
	});
	//LIST ALL Newsletter
  $scope.data = {};
  var num = 10;
  var off = 1;
  var paginate = function (off) {
    $http({
      url: Config.ApiURL+"/fe/newsletterlist/" + num + '/' + off,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      $scope.data = data.data;
      $scope.maxSize = 5;
      $scope.bigTotalItems = data.total_items;
      $scope.bigCurrentPage = data.index;
    }).error(function (data, status, headers, config) {
    });
  }
  $scope.numpages = function (off) {
    paginate(off);
  }
  $scope.setPage = function (off) {
    paginate(off);
  };
  paginate(off);
});
app.controller('Pdfnewsletterctrl', function ($scope, $http, Config) {
	//LIST ALL pdfNewsletter
  $scope.data = {};
  var num = 10;
  var off = 1;
  var paginate = function (off) {
    $http({
      url: Config.ApiURL+"/fe/pdfnewsletterlist/" + num + '/' + off,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      $scope.data = data.data;
      $scope.maxSize = 5;
      $scope.bigTotalItems = data.total_items;
      $scope.bigCurrentPage = data.index;
    }).error(function (data, status, headers, config) {
    });
  }
  $scope.numpages = function (off) {
    paginate(off);
  }
  $scope.setPage = function (off) {
    paginate(off);
  };
  paginate(off);



});
app.controller('Newslettershowctrl', function ($scope, $http, Config) {
	
	console.log("HEllow~!");
	$http({
		url: Config.ApiURL + "/fe/enewsletter/popularnews/listsuccesssotries",
		method: "GET",
		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
	}).success(function (data, status, headers, config) {
		$scope.printmainnews = data.mainnews;
		$scope.data = data.storyprop;
	});

});
////
//////////////////////////////////////FILTER ORDINAL DATE
app.filter('dateSuffix', function($filter) {
  var suffixes = ["th", "st", "nd", "rd"];
  return function(input) {
    var dtfilter = $filter('date')(input, 'MMMM dd');
    var day = parseInt(dtfilter.slice(-2));
    var relevantDigits = (day < 30) ? day % 20 : day % 30;
    var suffix = (relevantDigits <= 3) ? suffixes[relevantDigits] : suffixes[0];
    return dtfilter+suffix;
  };
});