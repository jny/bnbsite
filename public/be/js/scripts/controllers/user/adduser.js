'use strict';

/* Controllers */

app.controller('AddUserCtrl', function($scope, $http, $modal, Usersfactory, Config, Upload) {
    $scope.sampledata = 'rainier';
    var userid = "this-is-dummy-but-useful";

	angular.element( '#formsubmit_hid' ).hide();

	$scope.user = {
      username: "",
      email: "",
      password: "",
      conpass: "",
      userrole: "Administrator",
      fname: "",
      lname: "",
      bday: "",
      gender: "Male",
      status: 0,
      file: ""
    };
    var oriUser = angular.copy($scope.user);

	$scope.chkusername =function() {
		var username = $scope.user.username;
		$http({
			url:Config.ApiURL + "/validate/username/"+username,
			method: "GET",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			// data: $.param(dlt)
		}).success(function (data, status, headers, config) {
			$scope.usrname= data.exists;
			var result = $scope.usrname;
			if(result == false) {
				$scope.form.$invalid = true;
			}
		})
	};
	//VALIDATE EMAIL
	$scope.chkemail =function(){
		var email = $scope.user.email;

		$http({
			url: Config.ApiURL + "/validate/email/"+email,
			method: "GET",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			// data: $.param(dlt)
		}).success(function (data, status, headers, config) {
			$scope.usremail= data.exists;
            var result = $scope.usrname;
            if(result == false) {
                $scope.form.$invalid = true;
            }
		})
	};
	
	//VALIDATE PASSWORD IF MATCH
	$scope.confirmpass =function(){
		var password = $scope.user.password;
		var confirmpass = $scope.user.conpass;

		if(password!=confirmpass){
			$scope.confpass = "Please match your passwords";
            $scope.form.$invalid = true;
		}else{
			$scope.confpass = "";
		}
	};

    $scope.closeAlert = function(index, type) {
        if(type=='default') {
            $scope.alerts.splice(index, 1);
        } else if (type=='image') {
            $scope.imagealert.splice(index, 1);
        }
    };
	//SAVE DATA
	$scope.submitData = function(user, file){
		$scope.alerts = [];
		
		//PROFILE PIC
		$scope.upload(user,$scope.file);

	};

	$scope.upload = function (user,files) 
    {
        if(files === undefined || files === null || files === ''){
            Usersfactory.register(user, function(data) {
                $scope.alerts.push({ type: 'success', msg: 'New User has been successfuly Created!' });
                $scope.isSaving = true;
                    //$scope.reset(); 
                    $scope.user = angular.copy(oriUser);
                    $scope.form.$setPristine(true);
                    $scope.isSaving = false;
                    msgmodal("New User has been successfully created");
                    REGDISCEN();

                    $scope.file = undefined;
                    angular.element('#blah').attr('src','/img/default_profile_pic.jpg');
                    $scope.processing = false;
            });
        }
        else{


        $scope.processing = true;
        $scope.imagealert = [];
        var filename
        var filecount = 0;
        if (files && files.length) 
        {

            for (var i = 0; i < files.length; i++) 
            {
                var file = files[i];

                    if (file.size >= 6000000) //6MB
                    {
                        $scope.imagealert.push({type: 'danger', msg: 'File ' + file.name + ' is too big. (Maximum of 6MB)'});
                        $scope.processing = false;
                    }
                    else
                    {
                        var promises;
                            
                            promises = Upload.upload({
                                
                                url: Config.amazonlink, //S3 upload url including bucket name
                                method: 'POST',
                                transformRequest: function (data, headersGetter) {
                                //Headers change here
                                var headers = headersGetter();
                                delete headers['Authorization'];
                                return data;
                                },
                                fields : {
                                  key: 'uploads/userimages/' + file.name, // the key to store the file on S3, could be file name or customized
                                  AWSAccessKeyId: Config.AWSAccessKeyId,
                                  acl: 'private', // sets the access to the uploaded file in the bucket: private or public 
                                  policy: Config.policy, // base64-encoded json policy (see article below)
                                  signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                                  "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                                },
                                file: file
                            })
                            promises.then(function(data){

                                    filecount = filecount + 1;
                                    filename = data.config.file.name;

									user['profile_pic_name'] = filename;

                                //{MODAL**
                                    var msgCTRL = function ($scope, $modalInstance, $state, message) {
                                        $scope.message = message;
                                        $scope.cancel = function () {
                                            $modalInstance.dismiss('cancel');
                                        };
                                    }
                                    var msgmodal = function(msg){
                                        var message ="neil";
                                        var modalInstance = $modal.open({
                                            templateUrl: 'success.html',
                                            controller: msgCTRL,
                                            resolve: {
                                                message: function () {
                                                    return msg;
                                                }
                                            }
                                        });
                                    }

                                Usersfactory.register(user, function(data) {
                                    $scope.alerts.push({ type: 'success', msg: 'New User has been successfuly Created!' });
                                    $scope.isSaving = true;
                                        //$scope.reset(); 
                                        $scope.user = angular.copy(oriUser);
                                        $scope.form.$setPristine(true);
                                        $scope.isSaving = false;
                                        msgmodal("New User has been successfully created");
                                        REGDISCEN();

                                        $scope.file = undefined;
                                        angular.element('#blah').attr('src','/img/default_profile_pic.jpg');
                                        $scope.processing = false;
                                });
                            });
                    }           
            }
        } 

        }   
    };

    var REGDISCEN = function() { 
        Usersfactory.availableREGDISCENlist(userid, function(data) {	
        	if(data.regions != '' ) { 
        		$scope.availableregion = data.regions;
        		$scope.hideregion = false;
        	} else {
        		$scope.hideregion = true;
        	}

        	if(data.districts != '' ) {
        		$scope.availabledistrict = data.districts;
        		$scope.hidedistrict = false;
        	} else {
        		$scope.hidedistrict = true;
        	}

            if(data.centers != '' ) {
                $scope.availablecenter = data.centers;
                $scope.hidecenter = false;
            } else {
                $scope.hidecenter = true;
            }
        });
    }
    REGDISCEN();

	//DATE PICKER
	$scope.today = function () {
    $scope.dt = new Date();
    };

    $scope.today();
    $scope.clear = function () {
    $scope.dt = null;
       
    };

    $scope.toggleMin = function () {
        $scope.minDate = $scope.minDate ? null : new Date();
    };

    $scope.toggleMin();

    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = true;
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        class: 'datepicker'
    };

    $scope.initDate = new Date('2016-15-20');
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];

})