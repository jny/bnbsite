'use strict';

/* Controllers */

app.controller('detailCtrl', function($scope, $state ,$q, $http, Config, $stateParams, $modal, uiGmapGoogleMapApi, detailsFactory, Upload){

      var datalat = '';
      var datalon = '';
   detailsFactory.loadlocation($stateParams.centerid,function(data){
    if(data == ''){

      datalat = '39.8422862102958';
      datalon = '-104.95239295312501';
      $scope.searchbox = { template:'searchbox.tpl.html', events:events};
      $scope.map = {center: {latitude: datalat, longitude: datalon }, zoom: 12 };
      $scope.options = {scrollwheel: true};
      $scope.coordsUpdates = 0;
      $scope.dynamicMoveCtr = 0;
      $scope.marker = {
        id: 0,
        coords: {
          latitude: datalat,
          longitude: datalon
        },

      options: { draggable: true },

         events: {
            dragend: function (marker, eventName, args) {
               var lat = marker.getPosition().lat();
               var lon = marker.getPosition().lng();
               $scope.lats = lat;
               $scope.lons = lon;

                  var location = {
                     'centerid': $stateParams.centerid,
                     'lat': lat,
                     'lon': lon
                  }

                  detailsFactory.saveLocation(location,function(data){
                     // console.log(data);
                  })

               $scope.marker.options = {
                draggable: true,
                labelAnchor: "100 0",
                labelClass: "marker-labels"
               };
            }
         }
      }




    }else{


      datalat = data.lat;
      datalon = data.lon;
      $scope.searchbox = { template:'searchbox.tpl.html', events:events};
      $scope.map = {center: {latitude: datalat, longitude: datalon }, zoom: 12 };
      $scope.options = {scrollwheel: true};
      $scope.coordsUpdates = 0;
      $scope.dynamicMoveCtr = 0;
      $scope.marker = {
        id: 0,
        coords: {
          latitude: datalat,
          longitude: datalon
        },

      options: { draggable: true },

         events: {
            dragend: function (marker, eventName, args) {
               var lat = marker.getPosition().lat();
               var lon = marker.getPosition().lng();
               $scope.lats = lat;
               $scope.lons = lon;

                  var location = {
                     'centerid': $stateParams.centerid,
                     'lat': lat,
                     'lon': lon
                  }

                  detailsFactory.saveLocation(location,function(data){
                     // console.log(data);
                  })

               $scope.marker.options = {
                draggable: true,
                labelAnchor: "100 0",
                labelClass: "marker-labels"
               };
            }
         }
      }


      } //end of else
   })


   


   var events = {
      places_changed: function (searchBox) {

         var places = searchBox.getPlaces()

         if (places.length == 0) {
          return;
         }

         for (var i = 0, place; place = places[i]; i++) {

            $scope.map = {center: {latitude: place.geometry.location.lat(), longitude: place.geometry.location.lng() }, zoom: 10 };
            $scope.marker = {
               id: 0,
               coords: {
                  latitude: place.geometry.location.lat(),
                  longitude: place.geometry.location.lng()
               },
               options: { draggable: true },
               events: {
                  dragend: function (marker, eventName, args) {
                     var lat = marker.getPosition().lat();
                     var lon = marker.getPosition().lng();
                     
                     $scope.lats = lat;
                     $scope.lons = lon;

                     var location = {
                        'centerid': $stateParams.centerid,
                        'lat': lat,
                        'lon': lon
                     }

                     detailsFactory.saveLocation(location,function(data){
                        // console.log(data);
                     })

                     $scope.marker.options = {
                        draggable: true,
                        labelAnchor: "100 0",
                        labelClass: "marker-labels"
                     };
                  }
               }
            }

         }
      }
   }

   $scope.searchbox = { template:'searchbox.tpl.html', events:events};
      $scope.map = {center: {latitude: datalat, longitude: datalon }, zoom: 15 };
      $scope.options = {scrollwheel: true};
      $scope.coordsUpdates = 0;
      $scope.dynamicMoveCtr = 0;
      $scope.marker = {
        id: 0,
        coords: {
          latitude: datalat,
          longitude: datalon
        },

      options: { draggable: true },

         events: {
            dragend: function (marker, eventName, args) {
               var lat = marker.getPosition().lat();
               var lon = marker.getPosition().lng();
               $scope.lats = lat;
               $scope.lons = lon;

               datalat = data.lat;
               datalon = data.lon;

                  var location = {
                     'centerid': $stateParams.centerid,
                     'lat': lat,
                     'lon': lon
                  }

                  detailsFactory.saveLocation(location,function(data){
                     // console.log(data);
                  })

               $scope.marker.options = {
                draggable: true,
                labelAnchor: "100 0",
                labelClass: "marker-labels"
               };
            }
         }
      }

      $scope.timelist = [
         {time:'01:00'}, {time:'01:05'}, {time:'01:10'}, {time:'01:15'}, {time:'01:20'}, {time:'01:25'},
         {time:'01:30'}, {time:'01:35'}, {time:'01:40'}, {time:'01:45'}, {time:'01:50'}, {time:'01:55'},
         {time:'02:00'}, {time:'02:05'}, {time:'02:10'}, {time:'02:15'}, {time:'02:20'}, {time:'02:25'},
         {time:'02:30'}, {time:'02:35'}, {time:'02:40'}, {time:'02:45'}, {time:'02:50'}, {time:'02:55'},
         {time:'03:00'}, {time:'03:05'}, {time:'03:10'}, {time:'03:15'}, {time:'03:20'}, {time:'03:25'},
         {time:'03:30'}, {time:'03:35'}, {time:'03:40'}, {time:'03:45'}, {time:'03:50'}, {time:'03:55'},
         {time:'04:00'}, {time:'04:05'}, {time:'04:10'}, {time:'04:15'}, {time:'04:20'}, {time:'04:25'},
         {time:'04:30'}, {time:'04:35'}, {time:'04:40'}, {time:'04:45'}, {time:'04:50'}, {time:'04:55'},
         {time:'05:00'}, {time:'05:05'}, {time:'05:10'}, {time:'05:15'}, {time:'05:20'}, {time:'05:25'},
         {time:'05:30'}, {time:'05:35'}, {time:'05:40'}, {time:'05:45'}, {time:'05:50'}, {time:'05:55'},
         {time:'06:00'}, {time:'06:05'}, {time:'06:10'}, {time:'06:15'}, {time:'06:20'}, {time:'06:25'},
         {time:'06:30'}, {time:'06:35'}, {time:'06:40'}, {time:'06:45'}, {time:'06:50'}, {time:'06:55'},
         {time:'07:00'}, {time:'07:05'}, {time:'07:10'}, {time:'07:15'}, {time:'07:20'}, {time:'07:25'},
         {time:'07:30'}, {time:'07:35'}, {time:'07:40'}, {time:'07:45'}, {time:'07:50'}, {time:'07:55'},
         {time:'08:00'}, {time:'08:05'}, {time:'08:10'}, {time:'08:15'}, {time:'08:20'}, {time:'08:25'},
         {time:'08:30'}, {time:'08:35'}, {time:'08:40'}, {time:'08:45'}, {time:'08:50'}, {time:'08:55'},
         {time:'09:00'}, {time:'09:05'}, {time:'09:10'}, {time:'09:15'}, {time:'09:20'}, {time:'09:25'},
         {time:'09:30'}, {time:'09:35'}, {time:'09:40'}, {time:'09:45'}, {time:'09:50'}, {time:'09:55'},
         {time:'10:00'}, {time:'10:05'}, {time:'10:10'}, {time:'10:15'}, {time:'10:20'}, {time:'10:25'},
         {time:'10:30'}, {time:'10:35'}, {time:'10:40'}, {time:'10:45'}, {time:'10:50'}, {time:'10:55'},
         {time:'11:00'}, {time:'11:05'}, {time:'11:10'}, {time:'11:15'}, {time:'11:20'}, {time:'11:25'},
         {time:'11:30'}, {time:'11:35'}, {time:'11:40'}, {time:'11:45'}, {time:'11:50'}, {time:'11:55'},
         {time:'12:00'}, {time:'12:05'}, {time:'12:10'}, {time:'12:15'}, {time:'12:20'}, {time:'12:25'},
         {time:'12:30'}, {time:'12:35'}, {time:'12:40'}, {time:'12:45'}, {time:'12:50'}, {time:'12:55'},
      ]

   $scope.hoursalerts = [];

   $scope.hourscloseAlert = function (index) {
    $scope.hoursalerts.splice(index, 1);
   };

   $scope.saveOpenhours = function(openhours){
      openhours['centerid'] = $stateParams.centerid;
      openhours['centerwew'] = $stateParams.centerid;
      console.log(openhours['centerwew'])
      detailsFactory.saveOpenhours(openhours,function(data){
         $scope.hoursalerts[0]= {type: data.type, msg: data.msg};
      })
   }

   

   detailsFactory.loadhours($stateParams.centerid,function(data){
      $scope.openhours = data.hours;
   })

   $scope.socialalerts = [];

   $scope.socialcloseAlert = function (index) {
    $scope.socialalerts.splice(index, 1);
   };

   $scope.savesociallink = function(link){
      $scope.upload(link,$scope.file);
   }

   $scope.upload = function (link,files) 
    {

        var filename
        var filecount = 0;
        if (files && files.length) 
        {

            $scope.imageloader=true;
            $scope.imagecontent=false;

            for (var i = 0; i < files.length; i++) 
            {
                var file = files[i];

                    if (file.size >= 2000000)
                    {
                        $scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
                        filecount = filecount + 1;
                        
                        if(filecount == files.length)
                            {
                                $scope.imageloader=false;
                                $scope.imagecontent=true;
                            }
                    }
                    else
                    {

                        var promises;
                            
                            promises = Upload.upload({
                                
                                url: Config.amazonlink, //S3 upload url including bucket name
                                method: 'POST',
                                transformRequest: function (data, headersGetter) {
                                //Headers change here
                                var headers = headersGetter();
                                delete headers['Authorization'];
                                return data;
                                },
                                fields : {
                                  key: 'uploads/socialimages/' + file.name, // the key to store the file on S3, could be file name or customized
                                  AWSAccessKeyId: Config.AWSAccessKeyId,
                                  acl: 'private', // sets the access to the uploaded file in the bucket: private or public 
                                  policy: Config.policy, // base64-encoded json policy (see article below)
                                  signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                                  "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                                },
                                file: file
                            })
                                promises.then(function(data){
                                    filecount = filecount + 1;
                                    filename = data.config.file.name;
                                    var fileout = {
                                       'centerid':$stateParams.centerid,
                                       'title': link.title,
                                       'linkurl': link.linkurl,
                                       'status': link.status,
                                       'imgfilename' : filename
                                    };
                                    $http({
                                        url: Config.ApiURL + "/center/social/savesocial",
                                        method: "POST",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        data: $.param(fileout)
                                    }).success(function (data, status, headers, config) {
                                       $scope.socialalerts[0]= {type: data.type, msg: data.msg};
                                       loaddbsocialliks();
                                       $scope.socialtablelist = true;
                                       $scope.addsociallist = false
                                       $scope.editsociallist = false;
                                    }).error(function (data, status, headers, config) {
                                          
                                    });
   
                                });
                    }           
            }
        }
        
          
    } //end of upload

   

   $scope.deletelink = function(linkid) {
        var modalInstance = $modal.open({
            templateUrl: 'deleteSocialLink.html',
            controller: deleteSocialLinkCTRL,
            resolve: {
                linkid: function() {
                    return linkid
                }
            }
        });
    }

    var loaddbsocialliks = function(){

      detailsFactory.loadsociallinks($stateParams.centerid,function(data){
          if(data != 'null'){
            $scope.sociallinksdata = data;
          }
          else{

             $scope.sociallinksdata = {};
          }
          
      })

    }

    loaddbsocialliks();


    var successloadalert = function(){

            $scope.socialalerts[0]= {type: 'success', msg: 'Social Link successfully Deleted!'};
            
    }

    var errorloadalert = function(){

            $scope.socialalerts[0]= {type: 'danger', msg: 'Something went wrong Social Link not Deleted!'};
    }


       
    var deleteSocialLinkCTRL = function($scope, $modalInstance, linkid) {
        $scope.ok = function() {
            $http({
                url: Config.ApiURL + "/center/social/deletesocial/" + linkid,
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
            }).success(function(data, status, headers, config) {

                loaddbsocialliks();
                $modalInstance.close();
                successloadalert();

            }).error(function(data, status, headers, config) {
                loaddbsocialliks();
                $modalInstance.close();
                errorloadalert();
            });

        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };


    }


    $scope.setstatus = function (status,linkid,keyword,centerslugs) {
       
            if(status == 1)
            {
                $http({
                        url: Config.ApiURL + "/center/social/updatesocialstatus/0/" + linkid + '/' + keyword,
                        method: "POST",
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    }).success(function (data, status, headers, config) {
                      
                        $scope.currentstatusshow = centerslugs;
                        var i = 2;
                        setInterval(function(){
                            i--;

                            if(i == 0)
                            {
                                 loaddbsocialliks();
                                 $scope.currentstatusshow = 0;
                            }
                        },1000)
                        
                    }).error(function (data, status, headers, config) {
                        loaddbsocialliks();
                        
                    });
            }
            else
            {
                 $http({
                        url: Config.ApiURL + "/center/social/updatesocialstatus/1/" + linkid + '/' + keyword,
                        method: "POST",
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    }).success(function (data, status, headers, config) {
                        

                        $scope.currentstatusshow = centerslugs;
                        var i = 2;
                        setInterval(function(){
                            i--;
                            if(i == 0)
                            {
                                 loaddbsocialliks();
                                $scope.currentstatusshow = 0;
                            }


                        },1000)
                    }).error(function (data, status, headers, config) {
                        loaddbsocialliks();
                    });
            }
       
    }

    $scope.addsociallink = function(){
      $scope.addsociallist = true
      $scope.editsociallist = false;
      $scope.socialtablelist = false;
    }

    $scope.canceladdsociallist = function(){
      $scope.socialtablelist = true;
       $scope.addsociallist = false
        $scope.editsociallist = false;
    }

    $scope.editlink = function(linkid){

      $scope.editsociallist = true;
      $scope.addsociallist = false
      $scope.socialtablelist = false;

      detailsFactory.loadlinksbyid(linkid,function(data){
        if(data != 'null'){
         $scope.linkdata = data;
        }
        else{
          $scope.linkdata = {};
        }
      })

      
    }

    $scope.updatesociallinks = function(linkdata){


      $scope.upload2(linkdata,$scope.files);

    }


    $scope.upload2 = function (linkdata,files) 
    {
      
        var filename
        var filecount = 0;
        if (files && files.length) 
        {

            $scope.imageloader=true;
            $scope.imagecontent=false;

            for (var i = 0; i < files.length; i++) 
            {
                var file = files[i];

                    if (file.size >= 2000000)
                    {
                        $scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
                        filecount = filecount + 1;
                        
                        if(filecount == files.length)
                            {
                                $scope.imageloader=false;
                                $scope.imagecontent=true;
                            }
                    }
                    else
                    {

                        var promises;
                            
                            promises = Upload.upload({
                                
                                url: Config.amazonlink, //S3 upload url including bucket name
                                method: 'POST',
                                transformRequest: function (data, headersGetter) {
                                //Headers change here
                                var headers = headersGetter();
                                delete headers['Authorization'];
                                return data;
                                },
                                fields : {
                                  key: 'uploads/socialimages/' + file.name, // the key to store the file on S3, could be file name or customized
                                  AWSAccessKeyId: Config.AWSAccessKeyId,
                                  acl: 'private', // sets the access to the uploaded file in the bucket: private or public 
                                  policy: Config.policy, // base64-encoded json policy (see article below)
                                  signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                                  "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                                },
                                file: file
                            })
                                promises.then(function(data){

                                    filecount = filecount + 1;
                                    filename = data.config.file.name;
                                    linkdata['imgfilename'] =  filename;
                                   
                                    detailsFactory.updateSociallinks(linkdata,function(data){
                                       $scope.socialalerts[0]= {type: data.type, msg: data.msg};
                                    })

                                    $scope.socialtablelist = true;
                                    $scope.addsociallist = false
                                    $scope.editsociallist = false;
                                          
                              
                                });
                    }           
            }
        }
        
          
    } //end of upload

    var loadphone = function(){
      detailsFactory.loadphonenumber($stateParams.centerid,function(data){
        
         $scope.phone = data;
   
      })
    }
    
    loadphone();

    $scope.phonealerts = [];

   $scope.phonecloseAlert = function (index) {
    $scope.phonealerts.splice(index, 1);
   };

    $scope.updatephone = function(phone){
      phone['centerid'] = $stateParams.centerid;
     
      detailsFactory.savephonenumber(phone,function(data){
     
        $scope.phonealerts[0]= {type: data.type, msg: data.msg};
   
      })

    }

    var loademailbycenter = function(){

      detailsFactory.getemail($stateParams.centerid,function(data){

       $scope.email = data;
   
      })
    }

    loademailbycenter();

    $scope.emailalerts = [];

    $scope.emailcloseAlert = function (index) {
      $scope.emailalerts.splice(index, 1);
    };

    $scope.updateemail = function(email){
      email['centerid'] = $stateParams.centerid;

      detailsFactory.saveemail(email,function(data){
         $scope.emailalerts[0]= {type: data.type, msg: data.msg};
         loademailbycenter();
      })
    }

    detailsFactory.centerdetails($stateParams.centerid,function(data){
      $scope.centerdesc = data.details;
    })

    $scope.savedescription = function(centerdesc) {
      var details = { 'centerdesc' : centerdesc, 'centerid' : $stateParams.centerid };
      detailsFactory.savedescription(details, function(data) {
        $scope.descalert = true;
      })
    }
    $scope.desccloseAlert = function (index) {
        $scope.descalert = false;
    };

  //disabled input URL onpageload
  $scope.updateFbLink = true;
  $scope.updateTwLink = true;
  $scope.updateGoLink = true;
  $scope.updateYeLink = true;

  // 4 fixed social links
  var sociallinks = function() {
    detailsFactory.loadsociallinks($stateParams.centerid, function(data) {

      // angular.forEach(data, function(value, key) {
      //   if(value.title == )
      // })
       if(data.facebook === false) {$scope.facebooklink = ''; $scope.fblinkstatus = 0;}
       else {$scope.facebooklink = data.facebook.linkurl; $scope.fblinkstatus = data.facebook.status}

       if(data.twitter === false) {$scope.twitterlink = ''; $scope.twlinkstatus = 0;}
       else {$scope.twitterlink = data.twitter.linkurl; $scope.twlinkstatus = data.twitter.status}

       if(data.google === false) {$scope.googlelink = ''; $scope.golinkstatus = 0;}
       else {$scope.googlelink = data.google.linkurl; $scope.golinkstatus = data.google.status}

       if(data.yelp === false) {$scope.yelplink = ''; $scope.yelinkstatus = 0;}
       else {$scope.yelplink = data.yelp.linkurl; $scope.yelinkstatus = data.yelp.status}
    });
  }
  sociallinks();

  // if the edit button is click
  // the save and cancel button appears
  //the textbox will also enabled
  $scope.btnEditLink = function(scl) {
    if(scl === 'facebook') {
      $scope.updateFbLink = false;
      $scope.btnSaveFbLink = true;
    } else if (scl === 'twitter') {
      $scope.updateTwLink = false;
      $scope.btnSaveTwLink = true;
    } else if (scl === 'google') {
      $scope.updateGoLink = false;
      $scope.btnSaveGoLink = true;
    } else if (scl === 'yelp') {
      $scope.updateYeLink = false;
      $scope.btnSaveYeLink = true;
    }

  }

  //if the cancel button is click || successful save
  //the edit button will appear and the save button will disapper
  //the textbox will also disabled
  var socialtoggleCtrl = function(scl) {
    if(scl === 'facebook') {
      $scope.updateFbLink = true;
      $scope.btnSaveFbLink = false;
    } else if (scl === 'twitter') {
      $scope.updateTwLink = true;
      $scope.btnSaveTwLink = false;
    } else if (scl === 'google') {
      $scope.updateGoLink = true;
      $scope.btnSaveGoLink = false;
    } else if (scl === 'yelp') {
      $scope.updateYeLink = true;
      $scope.btnSaveYeLink = false;
    }
  }

  $scope.btnCancel = function(scl) {
    sociallinks(); //displays the default value of links from the database
    socialtoggleCtrl(scl);
  }

  $scope.btnUpdateLink = function(link, scl) {
    if(link === undefined) { // just to make sure the link is valid
      $scope.socialAlert[0] = {type:'danger', msg:'Please check the '+ scl + ' url'};
    } else { //link is validated and ready to save
      var asset = {link:link, type:scl, centerid:$stateParams.centerid};
      detailsFactory.updatesociallink(asset, function (data) {
        $scope.sociallinkalert = true;
        $scope.socialAlert[0] = {type:data.status, msg:data.msg};
        sociallinks();
        socialtoggleCtrl(scl);
      })
    }
  }

  $scope.updatesocialurlstatus = function(status, scl) {
    var asset = {status:status, type:scl, centerid:$stateParams.centerid};
      detailsFactory.updatesocialurlstatus(asset, function (data) {
        $scope.sociallinkalert = true;
        $scope.socialAlert[0] = {type:data.status, msg:data.msg};
        if(data.status === 'danger') {
          sociallinks();
        }
      })
  }
  //status alert of social links
  $scope.socialAlert = [];
  $scope.closesociallinkalert = function(index) {
    $scope.socialAlert.splice(index, 1); 
  }

  detailsFactory.getspnldata($stateParams.centerid, function(data){
    console.log(data);
    $scope.spnlstatus = data.spnl;
  });

  $scope.spnlchangestatus = function(spnlstatus){
    detailsFactory.setspnldata($stateParams.centerid, spnlstatus, function(data){
      console.log(data);
      $scope.spnlstatus = spnlstatus;
    });
  }
  
})