'use strict';

/* Controllers */

app.controller('Editcenter', function($scope, $state, Upload ,$q, $http, Config, CreateCenterFactory, EditCenterFactory, $location, anchorSmoothScroll, $timeout, $stateParams,$modal){
  
    $scope.alerts = [];

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    $scope.center = {
        status:1,
        centertype:1  
    };

    var oricenter = angular.copy($scope.center);

    $scope.saveCenter = function(center)
    {
        if (center.openingdate == undefined) { center.openingdate = new Date(center.openingdate); } 
        else {  center.openingdate = new Date(center.openingdate); }

        EditCenterFactory.savecenter(center,function(data){
            
            $scope.alerts[0]= {type: data.type, msg: data.success};
            // clear fields
            //scroll top
            $location.hash('gototop');
            anchorSmoothScroll.scrollTo('gototop');
            editcenter();
        });
    }

    $scope.oncentertitle = function (Text)
    {

        if(Text == null){
            $scope.center.centerslugs = '';
        }
        else {
            // console.log('wew');
            var text1 = Text.replace(/[^\w ]+/g,'');
            $scope.center.centerslugs = angular.lowercase(text1.replace(/ +/g,'-'));
        }
        
    }

    var tempdata = [];
    var editcenter = function () {
        EditCenterFactory.loadcenter($stateParams.centerid,function(data){
            $scope.center = data.center;

            tempdata.push(data.center);
            EditCenterFactory.loadcity(data.center.centerstate,function(data){
                $scope.centercity = data;
            });

            EditCenterFactory.loadzip(data.center.centercity,function(data){
                
                $scope.getcenterzip = data;
                // $scope.center.centerzip =data[0].zip;
            });

        });
    }
    editcenter();

    EditCenterFactory.loadcentermanager(function(data3){
         $scope.centermanager = data3;
         // $scope.center.centerstate = data[0];

    });


    EditCenterFactory.loadstate(function(data){
         $scope.centerstate = data;
         // $scope.center.centerstate = data[0];

    });


    CreateCenterFactory.listregion(function (data){
        if(data != '') {
            $scope.regions = data;
            $scope.hideregion = false;
        } else { $scope.hideregion = true; }

    });


    $scope.statechange = function(statecode)
    {

        EditCenterFactory.loadcity(statecode,function(data){
            $scope.centercity = data;
            $scope.center.centercity =data[0].city;
            
            EditCenterFactory.loadzip(data[0].city,function(data){
                $scope.getcenterzip = data;
                $scope.center.centerzip =data[0].zip;
            });
        });
    }

    $scope.citychange = function(cityname)
    {
             EditCenterFactory.loadzip(cityname,function(data){
                $scope.getcenterzip = data;
                $scope.center.centerzip =data[0].zip;
            });
    }

    $scope.percent = function(price){
        var getpercent = .25 * price;
        $scope.center.dis1monthprice = price - getpercent;
    }

    $scope.percent2 = function(price){
        var getpercent = .25 * price;
        $scope.center.dis3monthprice = price - getpercent;
    }

    $scope.authorizehelp = function() {
        var modalInstance = $modal.open({
            templateUrl: 'authorizeHelp.html',
            controller: authorizeHelpCTRL
        });
    }
       
    var authorizeHelpCTRL = function($scope, $modalInstance) {
        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };
    };

    $scope.paypalhelp = function() {
        var modalInstance = $modal.open({
            templateUrl: 'paypalHelp.html',
            controller: paypalhelpCTRL
        });
    }
       
    var paypalhelpCTRL = function($scope, $modalInstance) {
        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };
    };

    //DATE PICKER
    $scope.today = function () {
    $scope.dt = new Date();
    };

    $scope.today();
    $scope.clear = function () {
    $scope.dt = null;
       
    };

    $scope.toggleMin = function () {
        $scope.minDate = $scope.minDate ? null : new Date();
    };

    $scope.toggleMin();

    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = true;
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        class: 'datepicker'
    };

    $scope.initDate = new Date('2016-15-20');
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
})