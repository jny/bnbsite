'use strict';

/* Controllers */

app.controller('beneplacepricingCtrl', function ($scope, $state, $q,$stateParams, Config, beneplacepricingFactory){

	beneplacepricingFactory.loadprice(function(data){
		$scope.price = data;
	})

	$scope.introalerts = [];

    $scope.closeintroAlert = function (index) {
        $scope.introalerts.splice(index, 1);
    };

	$scope.saveintro = function(price){
		beneplacepricingFactory.saveintro(price, function(data){
			$scope.introalerts[0]= {type: data.type, msg: data.msg};
		})
	}

	$scope.groupalerts = [];

    $scope.closegroupAlert = function (index) {
        $scope.groupalerts.splice(index, 1);
    };

	$scope.savegroup = function(price){
		beneplacepricingFactory.saveintro(price, function(data){
			$scope.groupalerts[0]= {type: data.type, msg: data.msg};
		})
	}

})