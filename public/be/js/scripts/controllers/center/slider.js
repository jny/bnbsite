'use strict';

app.controller('sliderCtrl', function ($scope, $http, $stateParams, Config, Upload, $modal ,centerViewFactory) {
 $scope.alerts = [];

 $scope.saveordering = function(img, ord) {
    var thispost = { imageid : img, ordering : ord };
    centerViewFactory.saveordering(thispost, function(data) {
        loadcenterimages();
    })
 }

 $scope.closeAlert = function (index, type) {
    if(type == 'default') {
        $scope.alerts.splice(index, 1);
    } else if (type == 'image') {
        $scope.imagealerts.splice(index, 1);
    }
    
};

$scope.imageloader = false;
$scope.imagecontent = true;

	$scope.che = function() {
		
	}

	var loadcenterimages = function() {
		centerViewFactory.loadCenterImages($stateParams.centerid, function(data) {
			$scope.centerimages = data.centerimages;
            var img = [];
            var ord = [];
            angular.forEach(data.centerimages, function(value, key) {
                ord.push(value.imageorder);
                img.push(value.id);
            })
            $scope.ord = ord;
             $scope.img= img;
		});
	}
	loadcenterimages();

	$scope.$watch('files', function () {
        $scope.upload($scope.files);
    });


        $scope.delete = function(id){
            var modalInstance = $modal.open({
                templateUrl: 'delete.html',
                controller: deleteCTRL,
                resolve: {
                    imgid: function() {
                        return id
                    }
                }
            });

        }

        var successloadalert = function(){ $scope.alerts.splice(0, 1); $scope.alerts.push({ type: 'success', msg: 'Image successfully Deleted!' });}
        var errorloadalert = function(){$scope.alerts.push({ type: 'danger', msg: 'Something went wrong Image not Deleted!' });}

        var deleteCTRL = function($scope, $modalInstance, imgid) {

        $scope.message="Are you sure do you want to delete this Photo?";
            $scope.ok = function() {
                $http({
                    url: Config.ApiURL+"/centerslider/delete/"+ imgid,
                    method: "get",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                }).success(function(data, status, headers, config) {
                    successloadalert();
                    loadcenterimages();
                    $modalInstance.close();
                }).error(function(data, status, headers, config) {
                    loadcenterimages();
                    $modalInstance.close();
                    errorloadalert();
                });
            };

            $scope.cancel = function() {
                $modalInstance.dismiss('cancel');
            };
        }




    $scope.upload = function (files) 
    {
        var filename
        var filecount = 0;
        if (files && files.length) 
        {
            $scope.imageloader=true;
            $scope.imagecontent=false;

            for (var i = 0; i < files.length; i++) 
            {
                $scope.imagealerts =[];
                var file = files[i];

                    if (file.size >= 6000000) //6MB
                    {
                        $scope.imagealerts.push({type: 'danger', msg: 'File ' + file.name + ' is too big. (Maximum of 6MB)'});
                        filecount = filecount + 1;
                        
                        if(filecount == files.length)
                            {
                                $scope.imageloader=false;
                                $scope.imagecontent=true;
                            }
                    }
                    else
                    {
                        var promises;
                            
                            promises = Upload.upload({
                                
                                url: Config.amazonlink, //S3 upload url including bucket name
                                method: 'POST',
                                transformRequest: function (data, headersGetter) {
                                //Headers change here
                                var headers = headersGetter();
                                delete headers['Authorization'];
                                return data;
                                },
                                fields : {
                                  key: 'uploads/center/'+ $stateParams.centerid + "/" + file.name, // the key to store the file on S3, could be file name or customized
                                  AWSAccessKeyId: Config.AWSAccessKeyId,
                                  acl: 'private', // sets the access to the uploaded file in the bucket: private or public 
                                  policy: Config.policy, // base64-encoded json policy (see article below)
                                  signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                                  "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                                },
                                file: file
                            })
                                promises.then(function(data){

                                    filecount = filecount + 1;
                                    filename = data.config.file.name;
                                    var fileout = {
                                        'imgfilename' : filename,
                                        'centerid' : $stateParams.centerid
                                    };

                                    centerViewFactory.saveimagesforslider(fileout, function(data) {
                                    	loadcenterimages();
                                    	$scope.imageloader=false;
                    					$scope.imagecontent=true;
                                    });
   
                                });
                    }           
            }
        }      
    };

})