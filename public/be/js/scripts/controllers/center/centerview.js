'use strict';

/* Controllers */

app.controller('Centerview', function ($scope, $state, $q,$stateParams, Config, centerViewFactory){
	var globaluserid = '';
	$scope.userid = function(userid) {
		globaluserid = userid;
		if($stateParams.uifrom != 'none'){
			if($stateParams.uifrom == 'introsession'){
				$state.go('centerview.list', {userid: globaluserid });
			}
			else if($stateParams.uifrom == 'groupsession'){
				$state.go('centerview.priv', {userid: globaluserid });
			}
			else if($stateParams.uifrom == 'beneplace'){
				$state.go('centerview.bene', {userid: globaluserid });
			}
		}
		else{
			$state.go('centerview.slider');
		}
	};

	
	centerViewFactory.loadcenter($stateParams.centerid,function(data){
		$scope.centerTitle = data.centertitle;
	})

 	

})