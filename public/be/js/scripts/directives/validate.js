
'use strict';
app.directive('uiModule', ['', function() {
  console.log('sample directive');
  return{
    restrict: 'EA',
    controller: function($scope){
    }
  };
/* Directives */
// All the directives rely on jQuery.
}]);

app.directive('ensureUnique', ['$http', function($http, Config) {
  return {
    require: 'ngModel',
    link: function(scope, ele, attrs, c) {
      scope.$watch(attrs.ngModel, function() {
      	console.log("directive");
        $http({
          method: 'POST',
          url: Config.ApiURL + '/validation/author/uniquename',
          data: {'field': c.$viewValue} //c.$viewValue gets the value of the field
        }).success(function(data, status, headers, cfg) {
          c.$setValidity('unique', data.isUnique);
          console.log(data)
        }).error(function(data, status, headers, cfg) {
          // c.$setValidity('unique', false);
          console.log('there is an error in userid validation');
        });
      });
    }
  }
}]);
