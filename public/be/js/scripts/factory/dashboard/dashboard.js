app.factory('dashboardFactory', function($http, $q, Config){
    return {
        counttotalpost: function(callback){
            $http({
                url: Config.ApiURL +"/be/dashboard/counttotalpost",
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        countvisit: function(callback){
            $http({
                url: Config.ApiURL +"/be/dashboard/countvisit",
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        }
       

    }  
})