app.factory('ViewStoryFactory', function($http, $q, Config){
	return {
		getInformation: function(storyid, callback) {
			$http({
				url: Config.ApiURL + "/story/information/" + storyid,
				method: "GET",
				headers: {'Content-Type' : 'application/x-www-form-urlencoded'}
			}).success(function (data, status, headers, config){
				callback(data);
			})
		},
		saveReviewedStory: function(story, callback) {
			$http({
				url: Config.ApiURL + "/story/savereviewedstory",
				method: "POST",
				headers: {'Content-Type' : 'application/x-www-form-urlencoded'},
				data: $.param(story)
			}).success(function (data, status, headers, config){
				callback(data);
			})
		},
		addmetatag: function(metaprop, callback) {
			$http({
				url: Config.ApiURL + "/story/addmetatags",
				method: "POST",
				headers: {'Content-Type' : 'application/x-www-form-urlencoded'},
				data: $.param(metaprop)
			}).success(function (data, status, headers, config){
				callback(data);
			})
		},
		metatags: function(storyid, callback) {
			$http({
				url: Config.ApiURL + "/story/metatags/"+storyid,
				method: "GET",
			}).success(function (data, status, headers, config){
				callback(data);
			})
		},
		updatemetatags: function(storyid, metatags, callback) {
			var metaprop = { 'storyid':storyid, 'metatags': metatags.toString() };
			$http({
				url: Config.ApiURL + "/story/updatemetatags",
				method: "POST",
				headers: {'Content-Type' : 'application/x-www-form-urlencoded'},
				data: $.param(metaprop)
			}).success(function (data, status, headers, config){
				callback(data);
			})
		},
		workshoptitles: function(storyid, callback) {
			$http({
				url: Config.ApiURL + "/story/workshoptitles/"+storyid,
				method: "GET"
			}).success(function (data, status, headers, config){
				callback(data);
			})
		},
		addrelated: function(storyid, title, callback) {
			var related = { 'storyid':storyid, 'title':title };
			$http({
				url: Config.ApiURL + "/story/addrelated",
				method: "POST",
				headers: {'Content-Type' : 'application/x-www-form-urlencoded'},
				data: $.param(related)
			}).success(function (data, status, headers, config){
				callback(data);
			})
		},
		removerelated: function(storyid, related, callback) {
			var related = { 'storyid':storyid, 'related': related.toString() };
			$http({
				url: Config.ApiURL + "/story/removerelated",
				method: "POST",
				headers: {'Content-Type' : 'application/x-www-form-urlencoded'},
				data: $.param(related)
			}).success(function (data, status, headers, config){
				callback(data);
			})
		},	
	}
});
