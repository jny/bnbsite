app.factory('allgroupclasssessionFactory', function($http, $q, Config){
    return {
        loadclasslist: function(num, off, keyword , callback){
            $http({
                url: Config.ApiURL +"/be/allgroupclasssession/allgroupclasssession/" + num + '/' + off + '/' + keyword,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        loadgroupsession: function(sessionid, userid, callback){
            $http({
                url: Config.ApiURL +"/be/getstarted/loadgroupsession/"+ sessionid +"/"+userid,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        }
       

    }  
})