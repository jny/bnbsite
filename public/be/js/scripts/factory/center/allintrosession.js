app.factory('allintrosessionFactory', function($http, $q, Config){
    return {
        loadclasslist: function(num, off, keyword , callback){
            $http({
                url: Config.ApiURL +"/be/allintrosession/allintrosession/" + num + '/' + off + '/' + keyword,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        loadsession: function(sessionid, userid, callback){
            $http({
                url: Config.ApiURL +"/be/getstarted/loadsession/"+ sessionid+ "/"+ userid,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        }
       

    }  
})