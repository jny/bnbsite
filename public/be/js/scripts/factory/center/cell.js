app.factory('cellFactory', function($http, $q, Config){
    return {
        savecontact: function(contact , callback){
            $http({
                url: Config.ApiURL +"/be/managercontract/savecontact",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(contact)
            }).success(function (data, status, headers, config) {
                callback(data);
                console.log(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        loadcontact: function(centerid , callback){
            $http({
                url: Config.ApiURL +"/be/managercontract/loadcontact/"+ centerid,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        testcontact: function(contact , callback){
            $http({
                url: Config.ApiURL +"/be/managercontract/testsmsmessage",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(contact)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },


    }  
})