app.factory('EditCenterFactory', function($http, $q, Config){
    return {

        data: {},
        loadstate: function(callback){
            $http({
                url: Config.ApiURL + "/center/statelist",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                data = data;
                callback(data);
            }).error(function (data, status, headers, config) {
                data = data;
                callback(data);
            });
         },


         loadcity: function(statecode,callback){
            $http({
                url: Config.ApiURL + "/center/citylist/" + statecode,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                data = data;
                callback(data);
            }).error(function (data, status, headers, config) {
                data = data;
                callback(data);
            });
         },

         loadzip: function(cityname,callback){
            $http({
                url: Config.ApiURL + "/center/ziplist/" + cityname,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                data = data;
                callback(data);
            }).error(function (data, status, headers, config) {
                data = data;
                callback(data);
            });
         },

         loadregionmanager: function(callback){
            $http({
                url: Config.ApiURL + "/center/regionmanagerlist",
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                data = data;
                callback(data);
            }).error(function (data, status, headers, config) {
                data = data;
                callback(data);
            });
         },

         // loaddistrictmanager: function(callback){
         //    $http({
         //        url: Config.ApiURL + "/center/districtmanagerlist",
         //        method: "GET",
         //        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
         //    }).success(function (data, status, headers, config) {
         //        data = data;
         //        callback(data);
         //    }).error(function (data, status, headers, config) {
         //        data = data;
         //        callback(data);
         //    });
         // },
         

         loadcentermanager: function(callback){
            $http({
                url: Config.ApiURL + "/center/centermanagerlist",
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                data = data;
                callback(data);
            }).error(function (data, status, headers, config) {
                data = data;
                callback(data);
            });
         },

         savecenter: function(center,callback){

            $http({
                url: Config.ApiURL + "/center/saveeditedcenter",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(center)
            }).success(function (data, status, headers, config) {
                data = data;
                callback(data);
            }).error(function (data, status, headers, config) {
                callback({success: "Something went wrong please check your feilds!", type: 'danger'});
            });
         },

         loadcenter: function(centerid,callback){
            $http({
                url: Config.ApiURL + "/center/loadcenter/"+ centerid,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                data = data;
                callback(data);
            }).error(function (data, status, headers, config) {
                data = data;
                callback(data);
            });
         },


    }



   
})