app.factory('introsessionFactory', function($http, $q, Config){
    return {
        loadintrolist: function(num, off, keyword, centerid , callback){
            $http({
                url: Config.ApiURL +"/be/getstarted/introsessionlist/" + num + '/' + off + '/' + keyword + '/' + centerid,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        loadsession: function(sessionid, userid, callback){
            $http({
                url: Config.ApiURL +"/be/getstarted/loadsession/"+ sessionid + "/" + userid,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        }
       

    }  
})