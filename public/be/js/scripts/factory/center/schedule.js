app.factory('scheduleFactory', function($http, $q, Config){
    return {

         loadschedule: function(centerid,callback){
            $http({
                url: Config.ApiURL + "/center/schedule/loadschedule/"+ centerid,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                data = data;
                callback(data);
            }).error(function (data, status, headers, config) {
                data = data;
                callback(data);
            });
         },
        
         saveschedule:function(session,callback){
            $http({
                url: Config.ApiURL + "/center/schedule/saveschedule",
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(session)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });

         },
         loadschedulebyid:function(sessionid,callback){
            $http({
                url: Config.ApiURL + "/center/schedule/loadschedulebyid/"+sessionid,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });

         },
         updateschedule:function(session,callback){
            $http({
                url: Config.ApiURL + "/center/schedule/updateschedule",
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(session)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });

         },
         deleteschedule:function(sessionid,callback){
            $http({
                url: Config.ApiURL + "/center/schedule/deleteschedule/"+sessionid,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });

         },

    }  
})