app.factory('pricingFactory', function($http, $q, Config){
    return {

        loadfee: function(centerid,callback){
            $http({
                url: Config.ApiURL + "/center/pricing/loadfee/" + centerid,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
         },
        savefee: function(allfee,callback){
            $http({
                url: Config.ApiURL + "/center/pricing/savefee",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(allfee)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        statusregfee: function(centerid,status,callback){
            $http({
                url: Config.ApiURL + "/center/pricing/statusregfee/" + centerid + '/' + status,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(status)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        statusregclass: function(centerid,status,callback){
            $http({
                url: Config.ApiURL + "/center/pricing/statusregclass/" + centerid + '/' + status,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        statussessionfee: function(centerid,status,callback){
            $http({
                url: Config.ApiURL + "/center/pricing/statussessionfee/" + centerid + '/' + status,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        savemembership: function(membership,callback){
            $http({
                url: Config.ApiURL + "/center/pricing/savemembership",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(membership)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        loadmembership: function(centerid,callback){
            $http({
                url: Config.ApiURL + "/center/pricing/loadmembership/" + centerid,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
         },
         saveprivatesession: function (asset, callback) {
            $http({
                url: Config.ApiURL + "/center/pricing/saveprivatesession",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(asset)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
         },
         loadPrivateSession: function(centerid, callback) {
            $http({
                url: Config.ApiURL + "/center/pricing/loadprivatesession/"+centerid,
                method: "GET"
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
         }

    }  
})