app.factory('Createnews', function($http, $q, Config){
  	return {
    	loadcategory: function(callback){
    	 	$http({
    	 		url: Config.ApiURL + "/news/listcategory",
    	 		method: "GET",
    	 		headers: {
    	 			'Content-Type': 'application/x-www-form-urlencoded'
    	 		}
    	 	}).success(function (data, status, headers, config) {
    	 		callback(data);
    	 	}).error(function (data, status, headers, config) {
    	 		callback(data);
    	 	});
    	 },
         loadcenter: function(callback){
            $http({
                url: Config.ApiURL + "/news/listcenter",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
         },
          sample: function(num, off, keyword , callback){
            $http({
                        url: Config.ApiURL +"/news/managenews/" + num + '/' + off + '/' + keyword,
                        method: "GET",
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    }).success(function (data, status, headers, config) {
                       data = data;
                       callback(data);
                       pagetotalitem = data.total_items;
                       currentPage = data.index;
                       
                    }).error(function (data, status, headers, config) {
                       callback(data);
                    });
         },
         workshoptitles: function(newsid, callback) {
            $http({
                url: Config.ApiURL + "/mainnews/workshoptitles/"+newsid,
                method: "GET"
            }).success(function (data, status, headers, config){
                callback(data);
                console.log(data);
            })
        },
        addrelated: function(newsid, title, callback) {
            var related = { 'newsid':newsid, 'title':title };
            $http({
                url: Config.ApiURL + "/mainnews/addrelated",
                method: "POST",
                headers: {'Content-Type' : 'application/x-www-form-urlencoded'},
                data: $.param(related)
            }).success(function (data, status, headers, config){
                callback(data);
            })
        },
        removerelated: function(newsid, related, callback) {
            var related = { 'newsid':newsid, 'related': related.toString() };
            $http({
                url: Config.ApiURL + "/mainnews/removerelated",
                method: "POST",
                headers: {'Content-Type' : 'application/x-www-form-urlencoded'},
                data: $.param(related)
            }).success(function (data, status, headers, config){
                callback(data);
            })
        },
        centernewsworkshoprelated: function(newsid, callback) {
            $http({
                url: Config.ApiURL + "/centernews/workshoprelated/"+newsid,
                method: "GET"
            }).success(function (data, status, headers, config){
                callback(data);
                console.log(data);
            })
        },
        centernewsaddrelated: function(newsid, title, callback) {
            var related = { 'newsid':newsid, 'title':title };
            $http({
                url: Config.ApiURL + "/centernews/addrelated",
                method: "POST",
                headers: {'Content-Type' : 'application/x-www-form-urlencoded'},
                data: $.param(related)
            }).success(function (data, status, headers, config){
                callback(data);
            })
        },
        centernewsremoverelated: function(newsid, related, callback) {
            var related = { 'newsid':newsid, 'related': related.toString() };
            $http({
                url: Config.ApiURL + "/centernews/removerelated",
                method: "POST",
                headers: {'Content-Type' : 'application/x-www-form-urlencoded'},
                data: $.param(related)
            }).success(function (data, status, headers, config){
                callback(data);
            })
        },      

    }
   
})