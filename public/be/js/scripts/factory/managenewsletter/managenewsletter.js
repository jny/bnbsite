app.factory('Managenewsletter', function($http, $q, Config){
  	return {
        data: {},
    	loadlist:  function(num, off, keyword, callback){
    	 	$http({
    	 		url: Config.ApiURL + "/managenewsletter/list/"+ num + '/' + off + '/' + keyword,
    	 		method: "GET",
    	 		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    	 	}).success(function (data, status, headers, config) {
    	 		data = data;
    	 		callback(data);
                pagetotalitem = data.total_items;
                currentPage = data.index;
    	 	}).error(function (data, status, headers, config) {
    	 		data = data;
    	 		callback(data);
    	 	});
    	 },
         create: function(newsletter,callback) {
            $http({
                url: Config.ApiURL + "/newsletter/add",
                method: "POST",
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                data: $.param(newsletter)
            }).success(function (data, status, headers, config) {
                data = data;
                callback(data);
            }).error(function (data, status, headers, config) {
                data = data;
                callback(data);
            });
        },
        checktitle: function(title, callback) {
            $http({
                url: Config.ApiURL + "/newsletter/checktitle/"+title,
                method: "GET",
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).success(function (data, status, headers, config) {
                data = data;
                callback(data);
            }).error(function (data, status, headers, config) {
                data = data;
                callback(data);
            });
        },
        deletenews: function(id,callback){
         $http({
                url: Config.ApiURL + "/newsletter/deletenewsletter/"+id,
                method: "GET",
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).success(function (data, status, headers, config) {
                data = data;
                callback(data);
            }).error(function (data, status, headers, config) {
                data = data;
                callback(data);
            });
        },
        deletenewspdf: function(id,callback){
         $http({
                url: Config.ApiURL + "/newsletter/deletenewspdf/"+id,
                method: "GET",
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).success(function (data, status, headers, config) {
                data = data;
                callback(data);
            }).error(function (data, status, headers, config) {
                data = data;
                callback(data);
            });
        },
        editnewsletter: function(id, callback) {
            $http({
                url: Config.ApiURL + "/newsletter/edit/"+id,
                method: "GET",
            }).success(function (data, status, headers, config) {
                data = data;
                callback(data);
            }).error(function (data, status, headers, config) {
                data = data;
                callback(data);
            });
        },
        editnewsletterpdf: function(id, callback) {
            $http({
                url: Config.ApiURL + "/newsletter/editpdf/"+id,
                method: "GET",
            }).success(function (data, status, headers, config) {
                data = data;
                callback(data);
            }).error(function (data, status, headers, config) {
                data = data;
                callback(data);
            });
        },
         updatenewsletter: function(newsletter,callback) {
            $http({
            url: Config.ApiURL+"/newsletter/updatenewsletter",
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data:$.param(newsletter)
            }).success(function (data, status, headers, config) {
             console.log(data);
             callback(data);
            }).error(function (data, status, headers, config) {
             console.log(data);
             callback(data);
            });
         },
         updatenewsletterpdf: function(newsletter,callback) {
            $http({
            url: Config.ApiURL+"/newsletter/updatenewsletterpdf",
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data:$.param(newsletter)
            }).success(function (data, status, headers, config) {
             console.log(data);
             callback(data);
            }).error(function (data, status, headers, config) {
             console.log(data);
             callback(data);
            });
         }
    }
   
})