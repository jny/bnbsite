app.factory('managenotificationFactory', function($http, $q, Config){
    return {
         listnotification: function(userid, offset, filter, callback){
            $http({
                url: Config.ApiURL + "/be/notification/listnotification/" + userid + "/" + offset + "/" + filter,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
         },
         changestatus: function(notificationid, userid, callback){
            $http({
                url: Config.ApiURL + "/be/notification/changestatus/" + notificationid + "/" + userid,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
         },
         loadnotification: function(num, off, keyword, userid, callback){
            $http({
                url: Config.ApiURL +"/be/notification/loadnotification/" + num + '/' + off + '/' + keyword + '/' + userid,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function (data, status, headers, config) {
                callback(data);
             }).error(function (data, status, headers, config) {
                 callback(data);
             });
         },

         deletenotification: function(notificationid, callback){
            $http({
                url: Config.ApiURL +"/be/notification/deletenotification/" + notificationid,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function (data, status, headers, config) {
                callback(data);
             }).error(function (data, status, headers, config) {
                 callback(data);
             });
         },


    }
   
})