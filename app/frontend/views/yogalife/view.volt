<?php
header('Content-Type: charset=utf-8');
if($logoimage->value1 == 1){
  header('Location: /../../maintenance/');
}
?><?php echo $this->getContent()?>
<?php
	if($var404 == null) {
		echo "<center><label class='not-found'>Error 404: Page not found!</label></center>";
	}
	else {

?>

<div id="crumbs_cont">
	<ul id="crumbs">
		<li class="crumbsli"><a class="crumbs red_text" href="/">Home</a></li>
		<li class="crumbsli"><a class="crumbs red_text" href="<?php echo $articles; ?>">B&B Buzz</a></li>
		<?php 
			$getginfo = $newscategory;
			foreach ($getginfo as $key => $value) {
		?>
		<li>
		<a class="crumbscategory" href="<?php echo $articles; ?>/category/<?php echo $getginfo[$key]->categoryslugs;  ?>/1"><?php echo $getginfo[$key]->categoryname . " &nbsp; &#8226;";  ?></a>
		</li>
		<?php } ?>  
	</ul>
</div>

<div class="row">
    <div class="col s12">
      	<div class="newsindexcontent">

	        <div class="newsindexnewsdata">

	        	<h2 class="view_title"><?php echo $title; ?></h2>
	        	<span class="view_date">Released on <?php $date = date_create($date); echo date_format($date, "F jS, Y"); ?></span>
	        	<div class="sns_wrap-view">
	        		<ul class="ul-social-view">
	        			<li>
	        				<div class="fb-like fb-view" data-href="<?php echo  $this->config->application->BaseURL;?><?php echo $articles; ?>/view/<?php echo $newsslugs;  ?>" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>
	        			</li>
	        			<li>
	        				<div class="fb-share-button fb-view" data-href="<?php echo  $this->config->application->BaseURL;?><?php echo $articles; ?>/view/<?php echo $newsslugs;  ?>" data-layout="button_count"></div>
	        			</li>
	        			<li>
	        				<a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>
	        			</li>
	        			<li>
	        				<!-- Place this tag where you want the +1 button to render. -->
	        				<div class="Gplus"><div class="g-plusone"></div></div>
	        			</li>
	        			<li>
	        				<a href="//www.pinterest.com/pin/create/button/" data-pin-do="buttonBookmark"  data-pin-color="white"><img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_white_20.png" /></a>
	        			</li>
					</ul>
	        	</div>

	            <div class="view_content">
	            	<?php
	            	foreach ($latestnews as $data) {
	            		$scorelugs[]=$data->newsslugs;
	            		$banger[]=$data->banner;
	            	}
	            	?>
	              <!-- <img src="<?php  $this->config->application->amazonlink .'/uploads/newsimage/'.$banner; ?>" class="full_width"> -->
	              <?php echo $body; ?>

	              {% if author %}
	              <span class="float_right">&mdash; {{author}}</span>
	              {% endif %}
	              
	              <br>
	              <hr class="hr-custom">
	              <h6 class="h6-tiny">Category:  
				    <a href="<?php echo $articles; ?>/category/<?php echo $categoryslugs; ?>/1" class="a-custom-default">
			          <?php echo $categoryname; ?>
					</a>
				  </h6>

				  <div class="view_slide">

				  	<?php if($scorelugs[0]!=$newsslugs && current($banger)==$banger[0]){ 
				  		$key= array_search($newsslugs,$scorelugs);
				  	?>

				  	<a href="<?php echo  $this->config->application->BaseURL.$articles."/view/".$scorelugs[$key-1];?>" id="quickList_aPrev" title="prev" class="slide_btn btn_prev">prev</a>

				  	<?php } ?>

				  	<?php
				  	if($newsslugs != end($scorelugs)) {
				  	// if(end($scorelugs) != $newsslugs && end($banger)) {
				  		$key= array_search($newsslugs,$scorelugs);
				  	?>

				  	<a href="<?php echo  $this->config->application->BaseURL.$articles."/view/".$scorelugs[$key+1]; ?>" id="quickList_aNext" title="next" class="slide_btn btn_next">next</a>

				  	<?php } ?>

				  	<ul class="slide">
				  		<li>

				  			<?php if($scorelugs[0]!=$newsslugs && current($banger)==$banger[0]){ 
				  				$keyz= array_search($newsslugs,$scorelugs);
				  				?>

				  				<a href="<?php echo  $this->config->application->BaseURL.$articles."/view/".$scorelugs[$keyz-1];?>" id="quickList_aImgPrev" class="left">
				  					<img id="quickList_ImgPrev" src="<?php echo  $this->config->application->amazonlink."/uploads/newsimage/".$banger[$keyz-1]; ?>" style="height:43px;width:65px;border-width:0px;margin-left: 40px;">
				  					<p><?php echo substr(ucwords(str_replace("-"," ",$scorelugs[$keyz-1])),0,40)."..."; ?></p>
				  				</a>

				  				<?php } ?>

				  			<?php 

				  				if($scorelugs[0]!=$newsslugs && current($banger)!=$banger[0]){ 
				  				$keyz= array_search($newsslugs,$scorelugs);
				  				?>

				  				<a href="<?php echo  $this->config->application->BaseURL.$articles."/view/".@$scorelugs[$keyz-1];?>" id="quickList_aImgPrev" class="left">
				  					<img id="quickList_ImgPrev" src="<?php echo  $this->config->application->amazonlink."/uploads/newsimage/".@$banger[$keyz-1]; ?>" style="height:43px;width:65px;border-width:0px;margin-left: 40px;">
				  					<p><?php echo substr(ucwords(str_replace("-"," ",@$scorelugs[$keyz-1])),0,40)."..."; ?></p>
				  				</a>

				  				<?php } ?>

				  			<?php
				  				// if(end($scorelugs) != $newsslugs && end($banger)) {
				  				if($newsslugs != end($scorelugs)) {
				  				$key= array_search($newsslugs,$scorelugs);
				  				?>		

				  				<a href="<?php echo  $this->config->application->BaseURL.$articles."/view/".$scorelugs[$key+1];?>" id="quickList_aImgNext" class="right">
				  				<img id="quickList_ImgNext" src="<?php echo  $this->config->application->amazonlink."/uploads/newsimage/".$banger[$key+1]; ?>" style="height:43px;width:65px;border-width:0px;margin-left: 60px;">
				  					<p><?php echo substr(ucwords(str_replace("-"," ",$scorelugs[$key+1])),0,40)."..."; ?></p>
				  				</a>

				  				<?php } ?>
				  		</li>
				  	</ul>

				  </div>

	            </div> <!-- End of View content DIV-->
	            <!-- DISCUSSION PLUGIN -->
	            <div class="view_discussion">
	            	<div id="disqus_thread"></div>
					<script type="text/javascript">
					    /* * * CONFIGURATION VARIABLES * * */
					    var disqus_shortname = 'bodynbrain';
					    
					    /* * * DON'T EDIT BELOW THIS LINE * * */
					    (function() {
					        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
					        dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
					        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
					    })();
					</script>
					<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
	            </div> <!-- END OF DISCUSSION PLUGIN -->
	        </div>

	        <div class="newsindexsidebar">
	          <a href="/getstarted/starterspackage"><img src="/img/frontend/banner_getstarted_small.jpg" style="width:100%; height:auto"></a>

			   	<div class="row" ng-controller="yogalifeviewCtrl">

			   	<div class="col s6 mostpop bg_whitish padding_s" style="font-size:120%; text-align:center">
			   		<b class=" cursor_pointer" ng-click="tab_mostpop()">MOST POPULAR</b>
			   	</div>
			   	<div class="col s6 latest dev_text padding_s" style="font-size:120%; text-align:center">
			   		<b class="cursor_pointer " ng-click="tab_latest()">LATEST</b>
			   	</div>
			
			   	<div class="col s12 mostpopular bg_whitish" ng-show="mostpopular">
			   		<ul class="sidebar_list" style="padding:0">
			    		<li><br></li>
			    		<!-- LOOPING MOST VIEWED NEWS -->
			    		<?php 
			    			foreach($sidebarmostviewednews as $news) {
			    			?>
			    				<li>
			    					<a href="<?php echo $articles; ?>/view/<?php echo $news->newsslugs;?>" class="a_list">
						    		<div class="li_list">
						    			<div class="thumbnail">
											<img src="<?php echo  $this->config->application->amazonlink.'/uploads/newsimage/'.$news->banner;?>" class="img_thumbnail">
						    			</div>
						    			<div class="span">
						    				<?php echo substr($news->description, 0, 50) . "..";?>
						    			</div>
						    		</div>
					    			</a>
					    		</li>
			    			<?php
			    			}
			    		?>
			    	</ul>
			   	</div>
			   	<br>
			   	<div class="col s12 latest bg_whitish" ng-show="latest">
			   		<ul class="sidebar_list">
			    		<li><br></li>
			    		<!-- LOOPING LATEST NEWS -->
			    		<?php 
			    			foreach($sidebarlatestnews as $news) {
			    			?>
			    				<li><a href="<?php echo $articles; ?>/view/<?php echo $news->newsslugs;?>" class="a_list">
						    		<div class="li_list">
						    			<div class="thumbnail">
										  <img src="<?php echo  $this->config->application->amazonlink.'/uploads/newsimage/'.$news->banner;?>" class="img_thumbnail">
						    			</div>
						    			<div class="span">
						    				<?php echo substr($news->description, 0, 50) . ".."; ?>
						    			</div>
						    		</div>
					    			</a>
					    		</li>
			    			<?php
			    			}
			    		?>
			    	</ul>
			   	</div>
			    </div>
			    <br>
			   <div class="row sidebar_newsletter padding_m">
			      <div class="row">
			      	<div class="col s10">
			      		<h5>e-Newsletter</h5>
			      	</div>
			      	<form 	action="//bodynbrain.us3.list-manage.com/subscribe/post?u=cfc22757143336e0cfcf90eef&amp;id=eef0458b00" 
			      			method="post" 
			      			id="mc-embedded-subscribe-form" 
			      			name="mc-embedded-subscribe-form" 
			      			class="validate" 
			      			target="_blank" 
			      			novalidate >
				        <div class="input-field col s12">
				          <input 	type="email" 
				          			value="" 
				          			name="EMAIL" 
				          			class="required email validate custom-txtbox" 
				          			id="mce-EMAIL" 
				          			placeholder="E-mail Address">
				        </div>
				        <div class="col s6">
				        	<input 	type="submit" 
				        			value="Send" 
				        			name="subscribe" 
				        			id="mc-embedded-subscribe" 
				        			class="btn-custom btn-block">
				    	</div>
			    	</form>      
          
			      </div>                            
			  </div>
			  <br>
			  <div class="row success_stories padding_m">
			  	<div class="col s12">
			  		<h4 class="success_stories_title">Success Stories</h4>
			  		<ul class="sidebar_list">
			    		<li><br></li>
			    <?php 	foreach($view->stories as $story)  { ?>
			    <?php $story_slugs = preg_replace("![^a-z0-9]+!i", "-", strtolower($story->subject)); ?>
			    		<li><a href="/successstories/view/<?php echo $story->ssid; ?>/<?php echo $story_slugs; ?>" class="a_list">
				    			<div class="li_list">
				    				<div class="thumbnail">
				    					<!-- <img src="<?php   $this->config->application->amazonlink.'/uploads/testimonialimages/'.$story->photo;?>" class="img_thumbnail_stories"> -->
				    					<div class="thumbnail_ss" style="background:url(<?php echo  $this->config->application->amazonlink.'/uploads/testimonialimages/'.$story->photo;?>);">
	          							</div>
				    				</div>
				    				<div class="span">
				    				  <?php echo $story->subject; ?>
				    				  <br>
				    				  <label><?php echo $story->author; ?></label>
				    				</div>
				    			</div>
			    			</a>
			    		</li>
			    <?php 	} ?>
			    	</ul>
			  	</div>
			  </div>

	        </div> <!-- END of newsindexsidebar -->

      	</div>
    </div>
</div>

<?php 
	} // END OF VAR404 ELSE
?>


