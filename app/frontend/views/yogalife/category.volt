<?php echo $this->getContent()?>
<?php
if($logoimage->value1 == 1){
  header('Location: /../../maintenance/');
}
?>
<div id="crumbs_cont">
	<ul id="crumbs">
		<li class="crumbsli"><a class="crumbs" href="#">Home</a></li>
		<li class="crumbsli"><a class="crumbs" href="<?php echo $articles; ?>">B&B Buzz</a></li>
		<?php 
			$getginfo = $newscategory;
			foreach ($getginfo as $key => $value) {
		?>
		<li>
		<a class="crumbscategory" href="<?php echo $articles; ?>/category/<?php echo $getginfo[$key]->categoryslugs;  ?>/1"><?php echo $getginfo[$key]->categoryname . " &nbsp; &#8226;";  ?></a>
		</li>
		<?php } ?>  
	</ul>
</div>

<div class="divseparetor"></div>

<div class="row">
    <div class="col s12">
      	<div class="newsindexcontent">

	        <div class="newsindexnewsdata">
	          
	          	<h4><?php echo $showcategoryname; ?></h4>
	        	<ul class="category_list">
	            
	            <?php 
					$getginfo = $newsbycategory;
					if(count($getginfo) == 0)
					{
						echo '<span class="warning_message">No Available News</span>';
					}
					else
					{
						foreach ($getginfo as $key => $value) {
					?>
		                <li>
	                        <a href="<?php echo $articles; ?>/view/<?php echo $getginfo[$key]->newsslugs;  ?>">
	                            <span class="titlelist"><?php echo $getginfo[$key]->title;  ?></span>
	                            <img alt="" src="<?php echo  $this->config->application->amazonlink."/uploads/newsimage/".$getginfo[$key]->banner; ?>" width="135" height="90" class="img">
	                            <span class="text"><?php echo $getginfo[$key]->description;  ?></span>
	                        </a>
	                      	<br>
	                       <div class="sns_wrap">
	                       	  <table class="table-social">
	                       	  	<tr>
	                       	  	  <td>
	                       	  	  	<span class="margin_top_10px inbl">
	                       				<div class="fb-like" data-href="<?php echo  $this->config->application->BaseURL;?><?php echo $articles; ?>/view/<?php echo $getginfo[$key]->newsslugs;  ?>" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>
	                       			</span>
	                       		  </td>
	                       		  <td>
	                       		  	<span class="margin_top_10px inbl">
	                       		  		<div class="fb-share-button" data-href="<?php echo  $this->config->application->BaseURL;?><?php echo $articles; ?>/view/<?php echo $getginfo[$key]->newsslugs;  ?>" data-layout="button_count"></div>
	                       		  	</span>
	                       		  </td>
	                       		  <td>
	                       		  	<!-- Place this tag where you want the +1 button to render. -->
									<div class="Gplus margin_top_20px"><div class="g-plusone"></div></div>
	                       		  </td>
	                       		  <td>
	                       		  	<div class="margin_top_20px">
	                       		  		<a href="//www.pinterest.com/pin/create/button/" data-pin-do="buttonBookmark"  data-pin-color="white"><img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_white_20.png" /></a>
	                       		  	</div>
	                       		  </td>
	                       		</tr>
							  </table>
	                       </div>

	                    </li><br><br>
	                <?php }
	                	// PAGINATION START
			            echo '<div class="myPagination">';
			            echo '<span first_page_line></span><a href="'.$articles.'/category/'.$showcategoryslugs.'/1" class="first_page"></a>&nbsp';
			            $pageLastNumberKey = $totalpage - 4;
			            if($page >= $pageLastNumberKey) {
						 	if($page > 1)
								echo '<a href="'.$articles.'/category/'.$showcategoryslugs.'/'.($page - 1).'" class="back_pagination"></a>&nbsp';
								for($i=max(1, $pageLastNumberKey); $i<=max(1, min($totalpage,$page+4)); $i++) 
							{
							 if($i == $page)
							  echo '<span class="active_page pagination_pages">'.$i.'</span>';
							 else
							  echo '<a href="'.$articles.'/category/'.$showcategoryslugs.'/'.$i.'" class="other_pages pagination_pages">'.$i.'</a>&nbsp';
							}
						}
						else {	
							if($page > 1)	
								echo '<a href="'.$articles.'/category/'.$showcategoryslugs.'/'.($page - 1).'" class="back_pagination"></a>&nbsp';
								for($i=max(1, $page); $i<=max(1, min($totalpage,$page+4)); $i++)
							{
							 if($i == $page)
							  echo '<span class="active_page pagination_pages">'.$i.'</span>';
							 else
							  echo '<a href="'.$articles.'/category/'.$showcategoryslugs.'/'.$i.'" class="other_pages pagination_pages">'.$i.'</a>&nbsp';
							}
						}
						if ($page < $totalpage)
						 	echo '<a href="'.$articles.'/category/'.$showcategoryslugs.'/'.($page + 1).'" class="next_pagination"></a>&nbsp;';
							echo '<a href="'.$articles.'/category/'.$showcategoryslugs.'/'.$totalpage.'" class="last_page"></a><span last_page_line></span>';
							echo"</div>";
						// PAGINATION ENDS
					} ?> 
                    
	            </ul>


	        </div>

	        <div class="newsindexsidebar">
	          <a href="/getstarted/starterspackage"><img src="/img/frontend/banner_getstarted_small.jpg" style="width:100%;height:auto;"></a>

			      <div class="row" ng-controller="yogalifeviewCtrl">
			      	<div class="col s6 mostpop bg_whitish padding_s" style="font-size:120%; text-align:center">
			      		<b class=" cursor_pointer" ng-click="tab_mostpop()">MOST POPULAR</b>
			      	</div>
			      	<div class="col s6 latest dev_text padding_s" style="font-size:120%; text-align:center">
			      		<b class="cursor_pointer " ng-click="tab_latest()">LATEST</b>
			      	</div>

			      	<div class="col s12 mostpopular bg_whitish" ng-show="mostpopular">
			      		<ul class="sidebar_list" style="padding:0">
			      			<li><br></li>
			      			<!-- LOOPING MOST VIEWED NEWS -->
			      			<?php 
			      			foreach($sidebarmostviewednews as $news) {
			      				?>
			      				<li>
			      					<a href="<?php echo $articles; ?>/view/<?php echo $news->newsslugs;?>" class="a_list">
			      						<div class="li_list">
			      							<div class="thumbnail">
			      								<img src="<?php echo  $this->config->application->amazonlink.'/uploads/newsimage/'.$news->banner;?>" class="img_thumbnail">
			      							</div>
			      							<div class="span">
			      								<?php echo substr($news->description, 0, 50) . "..";?>
			      							</div>
			      						</div>
			      					</a>
			      				</li>
			      				<?php
			      			}
			      			?>
			      		</ul>
			      	</div>
			        <br>
			        <div class="col s12 latest bg_whitish" ng-show="latest">
			        	<ul class="sidebar_list">
			        		<li><br></li>
			        		<!-- LOOPING LATEST NEWS -->
			        		<?php 
			        		foreach($sidebarlatestnews as $news) {
			        			?>
			        			<li><a href="<?php echo $articles; ?>/view/<?php echo $news->newsslugs;?>" class="a_list">
			        				<div class="li_list">
			        					<div class="thumbnail">
			        						<img src="<?php echo  $this->config->application->amazonlink.'/uploads/newsimage/'.$news->banner;?>" class="img_thumbnail">
			        					</div>
			        					<div class="span">
			        						<?php echo substr($news->description, 0, 50) . ".."; ?>
			        					</div>
			        				</div>
			        			</a>
			        		</li>
			        		<?php
			        	}
			        	?>
			        	</ul>
			      	</div>
			      </div>
		        	<br>

	          	<div class="row padding_m br_lt_gr">
	          		<div class="col s10">
	          			<h5>e-Newsletter</h5>
	          		</div>
	          		<form action="//bodynbrain.us3.list-manage.com/subscribe/post?u=cfc22757143336e0cfcf90eef&amp;id=eef0458b00" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
	          			<div class="input-field col s6">
	          				<input type="text" value="" name="FNAME" class="input_enews_name2 placeholder left_newsletter_txtbox" placeholder="First Name" id="mce-FNAME" autocomplete="off" style=" float:left;">
	          			</div>
	          			<div class="input-field col s6">
	          				<input type="text" value="" name="LNAME" class="input_enews_name2 placeholder right_newsletter_txtbox" placeholder="Last Name" id="mce-LNAME" autocomplete="off">
	          			</div>
	          			<div class="input-field col s12 no-margin">
	          				<input type="email" value="" name="EMAIL" class="required email validate custom-txtbox" id="mce-EMAIL" placeholder="E-mail Address">
	          			</div>
	          			<div class="col s6">
	          				<input type="submit" value="Send" name="subscribe" id="mc-embedded-subscribe" class="btn-custom btn-block">
	          			</div>
	          		</form>      

	          	</div>
	          	<div class="row success_stories padding_m" ng-controller="yogalifecategoryCtrl" style="margin-top:10px !important">
	          		<div class="col s12">
	          			<h4 class="success_stories_title">Success Stories</h4>
	          			<ul class="sidebar_list">
	          				<li><br></li>
	          				<li ng-repeat="story in story5">
	          					<a href="/successstories/view/{[{story.id}]}" class="a_list">
	          					<div class="li_list">
	          						<div class="thumbnail">
	          							<!-- <img src="<?php   $this->config->application->amazonlink.'/uploads/testimonialimages/{[{story.photo}]}';?>" class="img_thumbnail"> -->
	          							<div class="thumbnail_ss" style="background:url(<?php echo  $this->config->application->amazonlink.'/uploads/testimonialimages/{[{story.photo}]}';?>);">
	          							</div>
	          						</div>
	          						<div class="span">
	          							{[{story.subject}]}
	          							<br>
	          							<label>{[{story.author}]}</label>
	          						</div>
	          					</div>
	          				</a>
	          			</li>
	          		</ul>
	          	</div>
	          </div>

	        </div>

      	</div>
    </div>
</div>
