<?php
if($logoimage->value1 == 1){
  header('Location: /../../maintenance/');
}
?>
<div class="container-fluid padding_top_md" ng-controller="successCtrl">
  <div class="row" id="center-row">
    <div class="col s12">
      <section id="special_page_sidecontent">
        <div id="special_page_sidebarmenu">
          <div class="sidebar-head">
            <a href="/<?php echo $center->centerslugs; ?>" class="white_text">
              <?php echo $center->centertitle; ?>
            </a>
          </div>
          <ul class="sidebarmenu-list">
            <li>
              <a href="/<?php echo $center->centerslugs; ?>/info">
              <label class="dot"> &#x25AA; </label>Center Info</a>
            </li>
            <li>
              <a href="/<?php echo $center->centerslugs; ?>/blog/1">
                <label class="dot"> &#x25AA; </label>
                <span class="bluish_text center_active">News</span>
              </a>
            </li>
            <li><a href="/<?php echo $center->centerslugs; ?>/class-schedule"><label class="dot"> &#x25AA; </label>Class Schedule</a></li>
            <li><a href="/<?php echo $center->centerslugs; ?>/success-stories/1"><label class="dot"> &#x25AA; </label>Success Stories</a></li>
            <li><a href="/<?php echo $center->centerslugs; ?>/calendar/1"><label class="dot"> &#x25AA; </label>Calendar</a></li>
            <li><a href="/<?php echo $center->centerslugs; ?>/pricing"><label class="dot"> &#x25AA; </label>Pricing</a></li>
          </ul>
        </div>

        <a href="/successstories/write" class="a_share_your_experience">
          <img src="/img/frontend/share.gif"> <br>
          Share <br> Your Experience
        </a>

        <div id="newsletter">
          <div class="frmtitle">e-Newsletter</div>
          <form>
            <div ><input type="text" class="text" placeholder="E-mail Address"></div>
            <input type="button" class="send" value="Send" />
          </form>
          <div class="clearboth"></div>
        </div>
      </section>

      <section id="middle_body">
        <h5 class="main_title">Our Center News</h5>



        <div class="view_wrap">
          <!-- view header -->
          <p class="view_title"><?php echo $title; ?></p>
          <p class="view_date no-margin">Released on <?php
              $newdate = date_create($date);
              $newdate = date_format($newdate, "F jS Y");

           echo $newdate; ?>
         </p>
          <div class="sns_wrap margin_top-20px">
            <div class="fb-like fb-view" data-href="<?php echo  $this->config->application->BaseURL ."/".$center->centerslugs;?>/newspost/<?php echo $newsslugs;  ?>" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>
          </div>
          <?php if($banner != ''){ ?>
           <img src="<?php echo $this->config->application->amazonlink .'/uploads/newsimage/'.$banner; ?>" class="full_width">
          <?php } ?>


          <!-- view content -->
          <div class="view_content">

            <p style="line-height:22px;font-size:15px;color:#333;padding-top:30px">
              <?php echo $body; ?>

              {% if author %}
                <span class="float_right">&mdash; {{author}}</span>
              {% endif %}
            </p>
          </div>
        </div>


         <!-- DISCUSSION PLUGIN -->
              <div class="view_discussion">
                <div id="disqus_thread"></div>
          <script type="text/javascript">
              /* * * CONFIGURATION VARIABLES * * */
              var disqus_shortname = 'bodynbrain';

              /* * * DON'T EDIT BELOW THIS LINE * * */
              (function() {
                  var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
                  dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                  (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
              })();
          </script>
          <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
              </div> <!-- END OF DISCUSSION PLUGIN -->
      </section>

      <div class="clearboth"></div>
      <aside id="sidebar_right_center">
        <div id="sidebar_half_center">
          <a href="/getstarted/starterspackage"><img src="/img/frontend/banner_getstarted_small.jpg" class="full_width" /></a>
          <div class="bg_yellowish padding_m">
            <span class="size_18">Success Stories</span>
          <?php foreach($success_stories as $success) {
                $story_slugs = preg_replace("![^a-z0-9]+!i", "-", strtolower($success->subject));
          ?>
                  <a href="/<?php echo $center->centerslugs .'/story/'. $story_slugs; ?>">
                  <div class="bg_centered" style="background:url(<?php echo $this->config->application->amazonlink . '/uploads/testimonialimages/' . $success->photo; ?>); height:100px"></div>
                  <span class="teal_text bl padding_top_S"><?php echo $success->subject; ?></span>
                  <label class="margin_bottom_s bl">
                    <?php echo $success->author .', '.$center->centertitle . ' center, ' . $center->centerstate; ?>
                  </label>
                  <div class="word_break text_85 auto_text"><?php echo $success->metadesc; ?></div>
                  <br>
                  </a>
          <?php } ?>
          </div>
        </div>
        <div class="popularpost_center">
            <div class="frmtitle marg_lft pop_feat" >Popular Features</div>
          <?php foreach($popular_features as $data) { ?>
              <div class="post">
                <a href="<?php echo $articles; ?>/view/<?php echo $data->newsslugs;?>">
                <img class="post_pic img_thumbnail" src="<?php echo $this->config->application->amazonlink.'/uploads/newsimage/'.$data->banner; ?>" />
              </a>
              <div class="small-text">
                <a href="<?php echo $articles; ?>/view/<?php echo $data->newsslugs;?>">
                  <?php  echo substr($data->description, 0, 50) . ".."; ?>
                </a>
              </div>
              <div class="clearboth"></div>
              </div>
          <?php } ?>
            <div class="clearboth"> </div>
          </div>
      </aside>

    </div> <!-- end of col s12 -->
  </div> <!-- end of row -->
</div> <!-- end of container -->
