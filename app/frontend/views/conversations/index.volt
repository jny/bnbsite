<?php
if($logoimage->value1 == 1){
  header('Location: /../../maintenance/');
}
?><div id="crumbs_cont">
  <ul id="crumbs">
    <li class="crumbsli"><a class="crumbs" href="/bnbsite">Home</a></li>
    <li class="crumbsli"><a class="crumbs" href="#">Conversations</a></li> 
  </ul>
</div>
<div class="community_top">
	<h6 class="title">JOIN THE CONVERSATION!</h6>
	<p class="welcome">Welcome to our Community Conversations, an integral part of  DahnYoga.com. Here you can post and read messages on interesting topics related to your Body &amp; Brain yoga experience. Plus,  you’ll get to know other people who share your interests and experiences. You can find instructions and guidelines for posting in Community Conversations here. We look forward to hearing from you!</p>
	<!--<p class="term">New or have a question regarding our posting policies? Read our <a href="/terms-of-use">Terms of Use</a> for more information.</p>-->
</div>
<div class="row" style="margin-top:25px !important">
	
	<div class="col s12 m6 padright">
		<a href="/conversations/my-experiece"><img src="/img/frontend/community_list01.jpg" width="100%" height="auto"></a>
	</div>

	<div class="col s12 m6 padleft">
		<a href="/conversations/community"><img src="/img/frontend/community_list02.jpg" width="100%" height="auto"></a>
	</div>

</div>
<div id="tagboard-embed" style="margin-top:0px;"></div>
<script>    var tagboardOptions = { tagboard: "dahnyoga/179093" };</script>
<script src="https://tagboard.com/public/js/embed.js"></script>