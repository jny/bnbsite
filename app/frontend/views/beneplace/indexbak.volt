<?php echo $this->getContent()?>
<?php
if($logoimage->value1 == 1){
  header('Location: /../../maintenance/');
}
?>
<div class="introffer" id="non-printable">
			<h2>TRIAL MEMBERSHIP</h2>
</div>
<div ng-controller="BeneplaceCtrl" >
	<div >
		<div class="introffer" ng-if="firststep">
			<!-- <h2>TRIAL MEMBERSHIP</h2> -->
			<ul class="offer_menu">
				<li class="omenu02 on sel1"><a href="" backtosecondstep>find location</a></li>
				<li class="omenu01 sel2">choose program</li> 
				<li class="omenu03 sel3">schedule session &amp; class</li>
				<li class="omenu04 sel4">confirm your appointment</li>
			</ul>

			<p class="offer_t">Find a location near you.</p>
			<div class="offer_ipbox" id="gototop">
				<form name="forsearch" ng-submit="searchcenter(search)">
					<div class="box_line">
						<span class="th">Search by Zip code</span><input name="txtZipcode" ng-model="search.zip" type="text" id="txtZipcode" class="input_text" onkeypress="if (event.keyCode == 13) {__doPostBack('btnFind',''); return false;}"><span>(Ex. 85252)</span>
					</div>
					<div class="box_line">
						<span class="th">Search by City &amp; State</span> <input name="txtCity" ng-model="search.city" type="text" id="txtCity" class="input_text" onkeypress="if (event.keyCode == 13) {__doPostBack('btnFind',''); return false;}"><span>(Ex. Phoenix, AZ)</span>
					</div>
					<div class="box_line">
						<span class="th">Search By State</span><select ng-model="search.state" name="ddlState" id="ddlState" onkeypress="if (event.keyCode == 13) {__doPostBack('btnFind',''); return false;}">
						<option value="">Select State</option>
						<option value="AZ">Arizona</option>
						<option value="CA">California</option>
						<option value="CO">Colorado</option>
						<option value="DC">District of Columbia</option>
						<option value="FL">Florida</option>
						<option value="GA">Georgia</option>
						<option value="HI">Hawaii</option>
						<option value="IL">Illinois</option>
						<option value="MA">Massachusetts</option>
						<option value="MD">Maryland</option>
						<option value="MN">Minnesota</option>
						<option value="NC">North Carolina</option>
						<option value="NJ">New Jersey</option>
						<option value="NM">New Mexico</option>
						<option value="NV">Nevada</option>
						<option value="NY">New York</option>
						<option value="OR">Oregon</option>
						<option value="TX">Texas</option>
						<option value="VA">Virginia</option>
						<option value="WA">Washington</option>
						<option value="WI">Wisconsin</option>
					</select><span>(Select State)</span>
				</div>
				<div class="box_line">
					<div id="divErrorMsr" class="processMsg" style="display:none;">
						<p>* Please provide a valid 5 character zip code.</p>
					</div>
					<div class="processMsg">
					</div>
					<input type="hidden" ng-model="lat">
					<input type="hidden" ng-model="lon">
					<span class="th">&nbsp;</span><a ng-click="searchcenter(search)" style="cursor:pointer;">FIND A CENTER</a>
				</div>
			</form>
		</div>
		<div ng-show="result" class="f_result">
			<p class="gtxt">Please select the center that’s most convenient for you:</p>
			<table cellpadding="0" cellspacing="0" border="0">
				<colgroup>
				<col style="width:120px">
				<col style="width:65px">
				<col style="width:670px">
				<col style="width:250px">
			</colgroup>
			<tbody><tr>
				<th><center>#</center></th>
				<th colspan="2">CENTER NAME AND ADDRESS</th>
				<th class="nob"><center>DISTANCE</center></th>
			</tr>
			<tr ng-repeat="list in locationlist">
				<td><center>{[{$index + 1}]}</center></td>
				<td class="nob"><center><input type="radio" id="radio1" ng-click="centerradiopick(list.centerid,list.centertitle,list.centeraddress,list.centerzip,list.phonenumber,list.lat,list.lon,list.email,list.paypalid,list.authorizeid,list.authorizekey,list.orig1monthprice,list.dis1monthprice,list.orig3monthprice,list.dis3monthprice)"  name="centername"></center></td>
				<td class="addif">
					<span class="nm"><a href="" ng-click="centerpick(list.centerid,list.centertitle,list.centeraddress,list.centerzip,list.phonenumber,list.lat,list.lon,list.email,list.paypalid,list.authorizeid,list.authorizekey,list.orig1monthprice,list.dis1monthprice,list.orig3monthprice,list.dis3monthprice)">{[{list.centertitle}]}</a></span><br>
					{[{list.centeraddress}]}1<br>
					Phone : {[{list.centertelnumber}]}
				</td>
				<td><center>{[{list.distance}]} <span ng-if="list.distance">Miles</span></center></td>
			</tr>
		</tbody></table>
		<br>
		<a class="fblue"  style="cursor:pointer;" ng-model="sel" ng-value="sel=optionSelect" ng-click="gotoschedule()" ng-show="btnpickcenter">CHOOSE THIS CENTER</a>
		<a class="wblue">US BODY &amp; BRAIN CENTER LOCATIONS</a>
		<div class="mem_map">
			<div id="map_canvas">
				<ui-gmap-google-map center="map.center" zoom="map.zoom" draggable="true" options="options" bounds="map.bounds">
				<ui-gmap-markers models="randomMarkers" coords="'self'"  icon="'icon'">
				<ui-gmap-windows show="show">
				<div ng-non-bindable>{[{title}]}</div>
			</ui-gmap-windows>
		</ui-gmap-markers>
	</ui-gmap-google-map>
</div>

</div>


</div>

</div>
</div>




<div>
	<div class="introffer" ng-if="secondstep">
		<!-- <h2>TRIAL MEMBERSHIP</h2> -->
		<ul class="offer_menu">
			<li class="omenu02 sel1"><a href="" ng-click="backtofirststep()">find location</a></li>
			<li class="omenu01 on sel2">choose program</li> 
			<li class="omenu03 sel3">schedule session &amp; class</li>
			<li class="omenu04 sel4">confirm your appointment</li>
		</ul>


		<p class="offer_t">Get a 25% discount when you try.</p>



		<div class="monthwrap" id="gototop">
			<div class="month01">
				<p class="first_box"><a href="" ng-click="selectsession('1')">1 MONTH</a></p>
				<p class="second_box">${[{dis1monthprice}]}<br><span class="stxt">(<strike>${[{orig1monthprice}]} VALUE</strike>)</span></p>
				<a class="btn_signmup" href="" ng-click="selectsession('1')">Sign Me Up!</a>
			</div>
			<div class="oor">OR</div>
			<div class="month03">
				<p class="first_box"><a href="" ng-click="selectsession('3')">3 MONTH</a></p>
				<p class="second_box">${[{dis3monthprice}]}<br><span class="stxt yred">(<strike>${[{orig3monthprice}]} VALUE</strike>)</span></p>
				<a class="btn_signmup" href="" ng-click="selectsession('3')">Sign Me Up!</a>
			</div>
		</div>
		<p class="guidetxt">
			* Valid for new membership only.<br>
			* Prices may vary by location.<br>
			* Registration fee will be waived.
		</p>
		<div class="offercall">
			STILL HAVE QUESTIONS? CALL US AT:
			<a>1-877-477-YOGA</a>
		</div>
		<p class="guidetxt02">Experience the BodynBrain difference by trying our group classes, or meeting one on one with an instructor.<br>
			Either way. you'll receive our personalized service and signature energy balancing techniques. These deals <br>
			are valid only for new members.</p>

		</div>
	</div>





	<div >
	<form name="scheduleform" ng-show="thirdstep" ng-submit="paymentsubmit()">
		<div class="introffer">
			<!-- <h2>TRIAL MEMBERSHIP</h2> -->
			<ul class="offer_menu">
				<li class="omenu02 sel1"><a href="" ng-click="backtofirststep()">find location</a></li>
				<li class="omenu01 sel2"><a href="" ng-click="backtosecondstep()">choose program</a></li> 
				<li class="omenu03 on sel3">schedule session &amp; class</li>
				<li class="omenu04 sel4">confirm your appointment</li>
			</ul>

			<p class="offer_t"> You’ve chosen a 1-month membership.</p>

			<div class="offer_ipbox2" id="gototop">
				<div class="box_line">
					<span class="th02">Name</span>
					<input name="txtName" type="text" id="txtName" ng-model='fullname' required ng-change="putname(fullname)">
				</div>
				<div class="box_line">
					<span class="th02">Email Address</span><input name="txtEmail" type="email" id="txtEmail" ng-model='email' required ng-change="putemail(email)">
				</div>
				<div class="box_line">
					<span class="th02">Phone Number</span>
					<input name="txtPhone1" type="text" id="txtPhone1" style="width: 114px" maxlength="3" ng-model="phone1" ng-change="putphone1(phone1)" onkeypress="return isNumberKey(event)" required>
					<input name="txtPhone2" type="text" id="txtPhone2" style="width: 114px" maxlength="3" ng-model="phone2" ng-change="putphone2(phone2)"  onkeypress="return isNumberKey(event)" required>
					<input name="txtPhone3" type="text" id="txtPhone3" style="width: 114px" maxlength="4" ng-model="phone3" ng-change="putphone3(phone3)"  onkeypress="return isNumberKey(event)" required>
				</div>
				<div class="box_line" style="display:none;">
					<span class="th02">Preferred Location</span>
					<select name="ddlState" onchange="javascript:setTimeout('__doPostBack(\'ddlState\',\'\')', 0)" id="ddlState" disabled="disabled" style="width:200px;">
						<option value="422">Aiea, HI</option>
						<option value="58">Albuquerque, NM</option>
						<option selected="selected" value="11">Alexandria, VA</option>
						<option value="456">Anaheim Hills, CA</option>
						<option value="29">Arlington, MA</option>
						<option value="467">Aspen Hill, MD</option>
						<option value="471">Babylon Village, NY</option>
						<option value="27">Bay Ridge, NY</option>
						<option value="120">Beaverton, OR</option>
						<option value="176">Belt Line, TX</option>
						<option value="450">Beltsville, MD</option>
						<option value="497">Bethany, OR</option>
						<option value="14">Bethesda, MD</option>
						<option value="477">Bloomingdale, IL</option>
						<option value="40">Brea, CA</option>
						<option value="28">Bronx, NY</option>
						<option value="125">Brookline, MA</option>
						<option value="158">Brooklyn Heights, NY</option>
						<option value="33">Buckhead, GA</option>
						<option value="468">Burbank, CA</option>
						<option value="60">Burke, VA</option>
						<option value="173">Cambridge, MA</option>
						<option value="483">Cerritos, CA</option>
						<option value="145">CGI, NJ</option>
						<option value="51">Champion, TX</option>
						<option value="444">Chatsworth, CA</option>
						<option value="168">Cottonwood, NM</option>
						<option value="110">Copperfield, TX</option>
						<option value="388">Denver, CO</option>
						<option value="434">Downtown Albuquerque, NM</option>
						<option value="429">Duluth, GA</option>
						<option value="386">Durango, NV</option>
						<option value="75">East Meadow, NY</option>
						<option value="473">Everett, WA</option>
						<option value="76">Flushing, NY</option>
						<option value="77">Forest Hills, NY</option>
						<option value="111">Franklin Square, NY</option>
						<option value="42">Fremont, CA</option>
						<option value="17">Gaithersburg, MD</option>
						<option value="448">Garden Grove, CA</option>
						<option value="474">Glen Ellyn, IL</option>
						<option value="143">Glendale, CA</option>
						<option value="64">Glendale-AZ, AZ</option>
						<option value="470">Golden, CO</option>
						<option value="78">Great Neck, NY</option>
						<option value="57">Henderson, NV</option>
						<option value="156">Honolulu, HI</option>
						<option value="493">Humboldt Park, IL</option>
						<option value="436">Irvine, CA</option>
						<option value="435">Kaimuki, HI</option>
						<option value="100">Katy, TX</option>
						<option value="462">Kew Gardens Hills, NY</option>
						<option value="459">Kingwood, TX</option>
						<option value="113">Kipling, CO</option>
						<option value="147">Kirkland, WA</option>
						<option value="71">Libertyville, IL</option>
						<option value="439">Littleton, CO</option>
						<option value="417">Lynbrook, NY</option>
						<option value="424">Lynnwood, WA</option>
						<option value="465">Madison, NJ</option>
						<option value="492">Madison-WI, WI</option>
						<option value="455">Manhattan, NY</option>
						<option value="494">Manteca, CA</option>
						<option value="490">Maple Grove, MN</option>
						<option value="472">Miami, FL</option>
						<option value="148">Mill Creek, WA</option>
						<option value="476">Mineola, NY</option>
						<option value="191">Missouri City, TX</option>
						<option value="479">Monrovia, CA</option>
						<option value="385">Montecito, NV</option>
						<option value="478">Mt Prospect, IL</option>
						<option value="162">New City, NY</option>
						<option value="475">New Rochelle, NY</option>
						<option value="446">North Las Vegas, NV</option>
						<option value="400">Northbrook, IL</option>
						<option value="481">Oak Park, IL</option>
						<option value="137">Oceanside, CA</option>
						<option value="72">Orland Park, IL</option>
						<option value="389">Pasadena, CA</option>
						<option value="399">Peachtree Buckhead, GA</option>
						<option value="491">Pinecrest, FL</option>
						<option value="486">Rainbow, NV</option>
						<option value="488">Raleigh, NC</option>
						<option value="438">Ramsey, NJ</option>
						<option value="469">Ravenna Park, WA</option>
						<option value="394">Ridgefield, NJ</option>
						<option value="445">Rockville, MD</option>
						<option value="165">Rolling Hills, CA</option>
						<option value="87">Roswell, GA</option>
						<option value="44">San Mateo, CA</option>
						<option value="414">San Ramon, CA</option>
						<option value="94">Santa Clara, CA</option>
						<option value="91">Santa Fe, NM</option>
						<option value="3">Scottsdale, AZ</option>
						<option value="484">Skokie, IL</option>
						<option value="183">Smithtown, NY</option>
						<option value="496">Stony Point, NY</option>
						<option value="489">Sun City, AZ</option>
						<option value="485">Sunnyside, NY</option>
						<option value="80">Syosset, NY</option>
						<option value="150">Tacoma, WA</option>
						<option value="487">Tarzana, CA</option>
						<option value="23">Tempe, AZ</option>
						<option value="432">The Woodlands, TX</option>
						<option value="142">Torrance, CA</option>
						<option value="192">Union Square, NY</option>
						<option value="139">Valley, CA</option>
						<option value="122">Voss, TX</option>
						<option value="61">Washington DC, DC</option>
						<option value="495">Wayne, NJ</option>
						<option value="121">West Linn, OR</option>
						<option value="116">Westchester, NY</option>
						<option value="440">Westchester-CA, CA</option>
						<option value="115">Westminster, CO</option>
						<option value="70">Westmont, IL</option>
						<option value="107">Westpark, TX</option>
						<option value="443">Wilshire, CA</option>
						<option value="112">Wyckoff, NJ</option>

					</select>
				</div>
				<div class="box_line">

					<p class="iptxt">
						
						<span>{[{centertitle}]} Center</span>
						<br>
						<br>
						<span>{[{centeraddress}]}, {[{centerzip}]}</span>
						<br>
						<br>
						<span>{[{centerphone}]}</span>
						<span style="display: none;">
							Alexandria@bodynbrain.com
							VA
						</span>
					</p>
				</div>
			</div>














			<div class="offer_ipbox3">
				<div class="box_line" style="margin-bottom: 40px">
					<span class="th"></span><a>SCHEDULE YOUR FIRST CLASS</a>
				</div>
				<div class="box_line" id="gototop">
					<p class="desc_error benedateerror" ng-show="dateerror" style="color:red !important;">{[{dateerrormsg}]}</p>
					<span class="th">Date</span><!--글씨 쓸때는 grey클래스 뺌(색상 다름)-->
					<input name="txtDate" type="text" id="datepicker" class="" ng-model="scheduledate" ng-change="checkdate(scheduledate)" required>
					<span>(Ex: 06/30/2014)</span>


					<input type="hidden" name="TextBoxWatermarkExtenderDate_ClientState" id="TextBoxWatermarkExtenderDate_ClientState">
					<div style="display: none;">
						<input name="txtDateTime" type="text" id="txtDateTime" class="input_text">
						<input type="button" name="btnCalendar" value="" onclick="javascript:__doPostBack('btnCalendar','')" id="btnCalendar">
					</div>
				</div>
				<span id="spDateMsg" class="iptxt" style="color:Red; margin-left:115px; display:none;">Please choose a date after <span id="lblToday"></span> </span>
				<div class="box_line" id="gotooffer_sche">
					<span class="th" style="height: 50px;"></span>
					<p class="iptxt">
						Choose one of the classes you want to attend. Please arrive at<br>
						the studio 15 minutes early. We will contact you to confirm your<br>
						appointment.</p>
					</div>
				</div>

				<center><span title="Please Choose Schedule from table below" style="color:Red;font-size: 18px;" ng-show="classhourwarning">Please Choose Schedule from table below!</span></center>
				<br>
				<br>
				<a class="btn_blue">MAKE AN APPOINTMENT FOR ONE OF THESE CLASSES</a>
				<div class="offer_sche" >
					<!-- <table cellpadding="0" cellspacing="0" border="0">
						<colgroup>
						<col style="width: 125px">
						<col style="width: 125px">
						<col style="width: 125px">
						<col style="width: 125px">
						<col style="width: 125px">
						<col style="width: 125px">
						<col style="width: 125px">
					</colgroup>
					<tbody><tr>
						<th>
							<center>SUN</center>
						</th>
						<th>
							<center>MON</center>
						</th>
						<th>
							<center>TUE</center>
						</th>
						<th>
							<center>WED</center>
						</th>
						<th>
							<center>THU</center>
						</th>
						<th>
							<center>FRI</center>
						</th>
						<th  class="nob">
							<center>SAT</center>
						</th>
						
					</tr>

					<tr>
						<td>
							<div class="divtable" ng-if="today != 'Sunday'" ng-repeat="list in schedlelist | filter: 'Sunday'">
								{[{list.classtype}]}<br>
								{[{list.starttime}]} {[{list.starttimeformat}]}

							</div>
							<div class="divtable" ng-if="today == 'Sunday'" ng-repeat="list in schedlelist | filter: 'Sunday'">
								<input type="radio" id="{[{list.classid}]}" name="radios" ng-model="radiodata" value="{[{list.starttime}]} {[{list.starttimeformat}]}" ng-click="puttime(radiodata)" required>
								<label class="bluetext" for="{[{list.classid}]}">{[{list.classtype}]} <br>{[{list.starttime}]} {[{list.starttimeformat}]}</label>

								


							</div>
						</td>
						<td>
							<div class="divtable" ng-if="today != 'Monday'" ng-repeat="list in schedlelist | filter: 'Monday'">
								{[{list.classtype}]}<br>
								{[{list.starttime}]} {[{list.starttimeformat}]}

							</div>
							<div class="divtable" ng-if="today == 'Monday'" ng-repeat="list in schedlelist | filter: 'Monday'">
								<input type="radio" id="{[{list.classid}]}" name="radios" ng-model="radiodata" value="{[{list.starttime}]} {[{list.starttimeformat}]}" ng-click="puttime(radiodata)" required>
								<label class="bluetext" for="{[{list.classid}]}">{[{list.classtype}]} <br>{[{list.starttime}]} {[{list.starttimeformat}]}</label>
							</div>
						</td>
						<td>
							<div class="divtable" ng-if="today != 'Tuesday'" ng-repeat="list in schedlelist | filter: 'Tuesday'">
								{[{list.classtype}]}<br>
								{[{list.starttime}]} {[{list.starttimeformat}]}

							</div>
							<div class="divtable" ng-if="today == 'Tuesday'" ng-repeat="list in schedlelist | filter: 'Tuesday'">
								<input type="radio" id="{[{list.classid}]}" name="radios" ng-model="radiodata" value="{[{list.starttime}]} {[{list.starttimeformat}]}" ng-click="puttime(radiodata)" required>
								<label class="bluetext" for="{[{list.classid}]}">{[{list.classtype}]} <br>{[{list.starttime}]} {[{list.starttimeformat}]}</label>

							</div>
						</td>
						<td>
							<div class="divtable" ng-if="today != 'Wednesday'" ng-repeat="list in schedlelist | filter: 'Wednesday'">
								{[{list.classtype}]}<br>
								{[{list.starttime}]} {[{list.starttimeformat}]}

							</div>
							<div class="divtable" ng-if="today == 'Wednesday'" ng-repeat="list in schedlelist | filter: 'Wednesday'">
								<input type="radio" id="{[{list.classid}]}" name="radios" ng-model="radiodata" value="{[{list.starttime}]} {[{list.starttimeformat}]}" ng-click="puttime(radiodata)" required>
								<label class="bluetext" for="{[{list.classid}]}">{[{list.classtype}]} <br>{[{list.starttime}]} {[{list.starttimeformat}]}</label>

							</div>
						</td>
						<td>
							<div class="divtable" ng-if="today != 'Thursday'" ng-repeat="list in schedlelist | filter: 'Thursday'">
								{[{list.classtype}]}<br>
								{[{list.starttime}]} {[{list.starttimeformat}]}

							</div>
							<div class="divtable" ng-if="today == 'Thursday'" ng-repeat="list in schedlelist | filter: 'Thursday'">
								<input type="radio" id="{[{list.classid}]}" name="radios" ng-model="radiodata" value="{[{list.starttime}]} {[{list.starttimeformat}]}" ng-click="puttime(radiodata)" required>
								<label class="bluetext" for="{[{list.classid}]}">{[{list.classtype}]} <br>{[{list.starttime}]} {[{list.starttimeformat}]}</label>

							</div>
						</td>
						<td>
							<div class="divtable" ng-if="today != 'Friday'" ng-repeat="list in schedlelist | filter: 'Friday'">
								{[{list.classtype}]}<br>
								{[{list.starttime}]} {[{list.starttimeformat}]}

							</div>
							<div class="divtable" ng-if="today == 'Friday'" ng-repeat="list in schedlelist | filter: 'Friday'">
								<input type="radio" id="{[{list.classid}]}" name="radios" ng-model="radiodata" value="{[{list.starttime}]} {[{list.starttimeformat}]}" ng-click="puttime(radiodata)" required>
								<label class="bluetext" for="{[{list.classid}]}">{[{list.classtype}]} <br>{[{list.starttime}]} {[{list.starttimeformat}]}</label>

							</div>
						</td>
						<td>
							<div class="divtable" ng-if="today != 'Saturday'" ng-repeat="list in schedlelist | filter: 'Saturday'">
								{[{list.classtype}]}<br>
								{[{list.starttime}]} {[{list.starttimeformat}]}

							</div>
							<div class="divtable" ng-if="today == 'Saturday'" ng-repeat="list in schedlelist | filter: 'Saturday'">
								<input type="radio" id="{[{list.classid}]}" name="radios" ng-model="radiodata" value="{[{list.starttime}]} {[{list.starttimeformat}]}" ng-click="puttime(radiodata)" required>
								<label class="bluetext" for="{[{list.classid}]}">{[{list.classtype}]} <br>{[{list.starttime}]} {[{list.starttimeformat}]}</label>

							</div>
						</td>
					</tr>
				</tbody></table> -->


				<table cellpadding="0" cellspacing="0" border="0">
						<colgroup>
						<col style="width: 125px">
						<col style="width: 125px">
						<col style="width: 125px">
						<col style="width: 125px">
						<col style="width: 125px">
						<col style="width: 125px">
						<col style="width: 125px">
					</colgroup>
					<tbody><tr>
						<th>
							<center>TIME</center>
						</th>
						<th>
							<center>SUN</center>
						</th>
						<th>
							<center>MON</center>
						</th>
						<th>
							<center>TUE</center>
						</th>
						<th>
							<center>WED</center>
						</th>
						<th>
							<center>THU</center>
						</th>
						<th>
							<center>FRI</center>
						</th>
						<th  class="nob">
							<center>SAT</center>
						</th>
						
					</tr>

					<tr ng-repeat="list in schedlelist">
										<td>
											<div>
												{[{list.starttime}]} - <br>
												{[{list.endtime}]}
											</div>
										</td>
										<td>
											<div ng-if="today != 'Sunday'">
												<div class="divtable" ng-if="list.su != null && list.su != ''">
													{[{list.su}]}
												</div>
											</div>
											<div ng-if="today == 'Sunday'">
												<div class="divtable" ng-if="list.su != null && list.su != ''">
													<input type="radio" id="{[{list.classid}]}" name="radios" ng-model="radiodata" value="{[{list.starttime}]}" ng-click="puttime(radiodata)">
													<label class="bluetext" for="{[{list.classid}]}">{[{list.su}]}</label>
												</div>
											</div>
										</td>
										<td>
											<div ng-if="today != 'Monday'">
												<div class="divtable" ng-if="list.mo != null && list.mo != ''">
													{[{list.mo}]}
												</div>
											</div>
											<div ng-if="today == 'Monday'">
												<div class="divtable" ng-if="list.mo != null && list.mo != ''">
													<input type="radio" id="{[{list.classid}]}" name="radios" ng-model="radiodata" value="{[{list.starttime}]}" ng-click="puttime(radiodata)">
													<label class="bluetext" for="{[{list.classid}]}">{[{list.mo}]}</label>
												</div>
											</div>
										<td>
											<div ng-if="today != 'Tuesday'">
												<div class="divtable" ng-if="list.tu != null && list.tu != ''">
													{[{list.tu}]}
												</div>
											</div>
											<div ng-if="today == 'Tuesday'">
												<div class="divtable" ng-if="list.tu != null && list.tu != ''">
													<input type="radio" id="{[{list.classid}]}" name="radios" ng-model="radiodata" value="{[{list.starttime}]}" ng-click="puttime(radiodata)">
													<label class="bluetext" for="{[{list.classid}]}">{[{list.tu}]}</label>
												</div>
											</div>
										</td>
										<td>
											<div ng-if="today != 'Wednesday'">
												<div class="divtable" ng-if="list.we != null && list.we != ''">
													{[{list.we}]}
												</div>
											</div>
											<div ng-if="today == 'Wednesday'">
												<div class="divtable" ng-if="list.we != null && list.we != ''">
													<input type="radio" id="{[{list.classid}]}" name="radios" ng-model="radiodata" value="{[{list.starttime}]}" ng-click="puttime(radiodata)">
													<label class="bluetext" for="{[{list.classid}]}">{[{list.we}]}</label>
												</div>
											</div>
										</td>
										<td>
											<div ng-if="today != 'Thursday'">
												<div class="divtable" ng-if="list.th != null && list.th != ''">
													{[{list.th}]}
												</div>
											</div>
											<div ng-if="today == 'Thursday'">
												<div class="divtable" ng-if="list.th != null && list.th != ''">
													<input type="radio" id="{[{list.classid}]}" name="radios" ng-model="radiodata" value="{[{list.starttime}]}" ng-click="puttime(radiodata)">
													<label class="bluetext" for="{[{list.classid}]}">{[{list.th}]}</label>
												</div>
											</div>
										</td>
										<td>
											<div ng-if="today != 'Friday'">
												<div class="divtable" ng-if="list.fr != null && list.fr != ''">
													{[{list.fr}]}
												</div>
											</div>
											<div ng-if="today == 'Friday'">
												<div class="divtable" ng-if="list.fr != null && list.fr != ''">
													<input type="radio" id="{[{list.classid}]}" name="radios" ng-model="radiodata" value="{[{list.starttime}]}" ng-click="puttime(radiodata)">
													<label class="bluetext" for="{[{list.classid}]}">{[{list.fr}]}</label>
												</div>
											</div>
										</td>
										<td>
											<div ng-if="today != 'Saturday'">
												<div class="divtable" ng-if="list.sa != null && list.sa != ''">
													{[{list.sa}]}
												</div>
											</div>
											<div ng-if="today == 'Saturday'">
												<div class="divtable" ng-if="list.sa != null && list.sa != ''">
													<input type="radio" id="{[{list.classid}]}" name="radios" ng-model="radiodata" value="{[{list.starttime}]}" ng-click="puttime(radiodata)">
													<label class="bluetext" for="{[{list.classid}]}">{[{list.sa}]}</label>
												</div>
											</div>
										</td>
										
									</tr>
				</tbody></table>
			</div>
			<p class="guidetxt03">
				* Energy Movement classes are Advanced BodynBrain classes and not available as trial
				classes.<br>
				You’ll schedule the 1-on-1 session at the center.</p>
				<div class="cbox">
					<a class="btn_blue">IS THERE ANYTHING YOU'D LIKE US TO KNOW?</a>
					<textarea name="txtComment" id="txtComment" class="luknow" ng-model="comment" ng-change="putcomment(comment)"></textarea>
					<p class="iptxt">
						All of your personal information will be kept private: <a href="/privacy-policy" target="_blank">Privacy Policy</a></p>
					</div>
					<div class="offer_sche" style="margin-bottom: 90px">
						<table cellpadding="0" cellspacing="0" border="0">
							<colgroup>
							<col style="width: 275px">
							<col style="width: 275px">
							<col style="width: 275px">
							<col style="width: 275px">
						</colgroup>
						<tbody><tr>
							<th>
								ITEM
							</th>
							<th>
								PRICE
							</th>
							<th>
								QUANTITY
							</th>
							<th>
								TOTAL
							</th>
						</tr>
						<tr>
							<td>
								{[{sessionclass}]}
								membership
							</td>
							<td>
								${[{finalonlinerate}]}
							</td>
							<td>
								<input type="text" ng-init="itemquantity = 1" ng-model="itemquantity" id="txtQuantity" value="1" class="input_text" style="width:30px !important; text-align:center !important;  padding: 0 8px !important; height: 25px !important; color: #666 !important; border: 1px solid #ccc !important; border-bottom: 1px solid #ccc !important; box-shadow: 0 1px 0 0 #FFF !important;margin:auto!important;" onkeypress="return isNumberKey(event)" ng-change="calculateprice(itemquantity)" required maxlength="3">
							</td>
							<td>
								$<span id="lblTotalPrice" ng-if="totalprice">{[{totalprice}]}</span> <span id="lblTotalPrice" ng-if="!totalprice">0</span>
							</td>
						</tr>
					</tbody></table>
				</div>

				<div class="btn_ar" style="">
					<input type="submit" name="btnSubmit"  value="CHECKOUT!" id="btnSubmit" class="btn_cout">
					<div class="oor">
					</form>
						OR</div>
						<form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" name="frmPayPal1" class="btn_submit_paypal" ng-show="validpaypal">
							<input type="hidden" name="business" value="{[{paypalid}]}">
							<input type="hidden" name="cmd" value="_xclick">
							<input type="hidden" name="item_name" value="{[{sessionclass}]}">
							<input type="hidden" name="item_number" value="1">
							<input type="hidden" name="custom" value="{[{submitfullname}]}|{[{submitemail}]}|{[{submitcenterid}]}|{[{submitphonenumber}]}|{[{submittime}]}|{[{submitschedday}]}|{[{submitcomment}]}|{[{itemquantity}]}|{[{device}]}|{[{deviceos}]}|{[{devicebrowser}]}|{[{itemprice}]}|{[{sessionclass}]}|{[{sessiontype}]}|beneplace">
							<!-- <input type="hidden" name="credits" value="510"> -->
							<!-- <input type="hidden" name="userid" value="1"> -->
							<input type="hidden" name="amount" value="{[{itemprice}]}">
							<input type="hidden"name="quantity" value="{[{itemquantity}]}">
							<!-- <input type="hidden" name="cpp_header_image" value="http://www.phpgang.com/wp-content/uploads/gang.jpg"> -->
							<input type="hidden" name="no_shipping" value="1">
							<input type="hidden" name="currency_code" value="USD">
							<input type="hidden" name="handling" value="0">
							<!-- <input type="hidden" name="cancel_return" value="http://demo.phpgang.com/payment_with_paypal/cancel.php"> -->
							<!-- <input type="hidden" name="return" value="http://bnb.gotitgenius.com/getstarted/success"> -->
							<input type="submit" name="btnPaypalSubmit"  id="btnPaypalSubmit" class="btn_ppal" ng-show="validpaypal">
						</form> 
						<input type="submit" name="btnPaypalSubmit"  id="btnPaypalSubmit" class="btn_ppal" ng-show="invalidpaypal">

					</div>
				</div>
			</div>
 


	<form name="checkout" ng-submit="submitcheckout(card)" ng-show="paymentcheck">
	<div class="introffer">
		<!-- <h2>TRIAL MEMBERSHIP</h2> -->
		<ul class="offer_menu">
			<li class="omenu02 sel1"><a href="" ng-click="backtofirststep()">find location</a></li>
			<li class="omenu01 sel2"><a href="" ng-click="backtosecondstep()">choose program</a></li> 
			<li class="omenu03 on sel3">schedule session &amp; class</li>
			<li class="omenu04 sel4">confirm your appointment</li>
		</ul>

		<div class="payment_wrap" id="gototop">
			<!-- payment method -->
			<h6 class="sub_title2"><span>Payment</span> Method</h6>
			<div style="z-index:100; position:absolute; right:300px; top:600px; font-size:12px; line-height:2em;">
				<div id="ValidationSummary1" class="processMsg" style="color:Red;display:none;">

				</div>
				<span id="RequiredFieldValidator8" title="Card Number is required." style="color:Red;display:none;"></span>
				<span id="RequiredFieldValidator9" title="Holder's Name is required." style="color:Red;display:none;"></span>
				<span id="RequiredFieldValidator10" title="Full Name is required." style="color:Red;display:none;"></span>
				<span id="RequiredFieldValidator11" title="Address line 1 is required." style="color:Red;display:none;"></span>
				<span id="RequiredFieldValidator12" title="City is required." style="color:Red;display:none;"></span>
				<span id="RequiredFieldValidator13" title="Zip Code is required." style="color:Red;display:none;"></span>
				<span id="RequiredFieldValidator14" title="Phone number is required." style="color:Red;display:none;"></span>
				<span id="CustomValidator1" style="color:Red;display:none;"></span>
			</div>
			<div class="form_box">

				<div>

					<img src="/img/frontend/amex.png" width="49px" height="29px">
					<img src="/img/frontend/discover.png" width="49px" height="29px">
					<img src="/img/frontend/mastercard.png" width="49px" height="29px">
					<img src="/img/frontend/visa.png" width="49px" height="29px">

				</div>

				<!-- <div class="input_box">
					<label for="select_card">Select card type</label>
					<div class="select_wrap">
						<select name="ddlCardType" id="ddlCardType" ng-init="card.cardtype = 'Visa'" ng-model="card.cardtype" required>
							<option value="Visa">Visa</option>
							<option value="Master card">Master card</option>
							<option value="American express">American express</option>
							<option value="Discover">Discover</option>

						</select>
					</div>
				</div> -->

				<div class="input_box" >
					<div class="carderror">{[{cardmsg}]}</div>
				</div>

				<div class="input_box">
					<label for="input_cardnumber">Card number</label>
					<input name="txtCardNumber" type="text" id="txtCardNumber" class="input_text" ng-model="card.cardnumber" onkeypress="return isNumberKey(event)" required>
				</div>

				<div class="input_box">
					<label for="input_cardname">Firstname</label>
					<input name="txtHoldersName" type="text" id="txtHoldersName" class="input_text" value="" ng-model="card.firstname" required>

				</div>

				<div class="input_box">
					<label for="input_cardname">Lastname</label>
					<input name="txtHoldersName" type="text" id="txtHoldersName" class="input_text" value="" ng-model="card.lastname" required>
				</div>

				<div class="input_box" style="margin-bottom:0">
					<label for="select_month">Expiration date</label>
					<div class="select_wrap select_month">
						<select name="ddlMonth" id="ddlMonth" class="inputBox"  ng-init="card.expimonth = '01'" ng-model="card.expimonth" required>
							<option value="01">01</option>
							<option value="02">02</option>
							<option value="03">03</option>
							<option value="04">04</option>
							<option value="05">05</option>
							<option value="06">06</option>
							<option value="07">07</option>
							<option value="08">08</option>
							<option value="09">09</option>
							<option value="10">10</option>
							<option value="11">11</option>
							<option value="12">12</option>

						</select>
					</div>
					<div class="select_wrap select_year">
						<select name="ddlYear" id="ddlYear" class="inputBox" ng-init="card.expiyear = '15'" ng-model="card.expiyear" required>
							<option value="15">2015</option>
							<option value="16">2016</option>
							<option value="17">2017</option>
							<option value="18">2018</option>
							<option value="19">2019</option>
							<option value="20">2020</option>
							<option value="21">2021</option>
							<option value="22">2022</option>
							<option value="23">2023</option>
							<option value="24">2024</option>
							<option value="25">2025</option>
							<option value="26">2026</option>
							<option value="27">2027</option>
							<option value="28">2028</option>
							<option value="29">2029</option>

						</select>
					</div>
				</div>
			</div>
			<div class="auth_wrap">
				<p>Privacy Policy : <a href="#"><img alt="VERIFIED MERCHANT Authorize.Net" src="/img/frontend/img_authorizenet.gif"></a></p>
			</div>

			<h5 class="sub_title2"><span>Billing</span> Information</h5>
				<div class="form_box billing_wrap">
					<div class="input_box">
						<label for="input_fullname">Full Name</label>
						<input name="txtFullname" type="text" id="txtFullname" class="input_text" value="" ng-model="card.billfullname" required>
					</div>
					<div class="input_box">
						<label for="input_addr1">Address Line 1</label>
						<input name="txtAddr1" type="text" id="txtAddr1" class="input_text" ng-model="card.billadd1" required>
					</div>
					<div class="input_box">
						<label for="input_addr2">Address Line 2</label>
						<input name="txtAddr2" type="text" id="txtAddr2" class="input_text" ng-model="card.billadd2">
						<span class="desc">(optional)</span>
					</div>
					<div class="input_box">
						<label for="input_city">City</label>
						<input name="txtCity" type="text" id="txtCity" class="input_text" ng-model="card.billcity" required>
					</div>
					<div class="input_box">
						<label for="select_state">State</label>
						<div class="select_wrap select_state">
							<select name="ddlBillingState" id="ddlBillingState" class="inputBox" ng-init="card.billstate = 'CA'" ng-model="card.billstate" required>
								<option value="AK">AK</option>
								<option value="AL">AL</option>
								<option value="AR">AR</option>
								<option value="AZ">AZ</option>
								<option value="CA">CA</option>
								<option value="CO">CO</option>
								<option value="CT">CT</option>
								<option value="DC">DC</option>
								<option value="DE">DE</option>
								<option value="FL">FL</option>
								<option value="GA">GA</option>
								<option value="HI">HI</option>
								<option value="IA">IA</option>
								<option value="ID">ID</option>
								<option value="IL">IL</option>
								<option value="IN">IN</option>
								<option value="KS">KS</option>
								<option value="KY">KY</option>
								<option value="LA">LA</option>
								<option value="MA">MA</option>
								<option value="MD">MD</option>
								<option value="ME">ME</option>
								<option value="MI">MI</option>
								<option value="MN">MN</option>
								<option value="MO">MO</option>
								<option value="MS">MS</option>
								<option value="MT">MT</option>
								<option value="NC">NC</option>
								<option value="ND">ND</option>
								<option value="NE">NE</option>
								<option value="NH">NH</option>
								<option value="NJ">NJ</option>
								<option value="NM">NM</option>
								<option value="NV">NV</option>
								<option value="NY">NY</option>
								<option value="OH">OH</option>
								<option value="OK">OK</option>
								<option value="OR">OR</option>
								<option value="PA">PA</option>
								<option value="RI">RI</option>
								<option value="SC">SC</option>
								<option value="SD">SD</option>
								<option value="TN">TN</option>
								<option value="TX">TX</option>
								<option value="UT">UT</option>
								<option value="VA">VA</option>
								<option value="VI">VI</option>
								<option value="VT">VT</option>
								<option value="WA">WA</option>
								<option value="WI">WI</option>
								<option value="WY">WY</option>

							</select>
						</div>
					</div>
					<div class="input_box">
						<label for="input_zipcode">Zip Code</label>
						<input name="txtBillingZipCode" type="text" id="txtBillingZipCode" class="input_text" ng-model="card.billzip" onkeypress="return isNumberKey(event)" required>
					</div>
					<div class="input_box">
						<label for="input_phonenumber">Phone number</label>
						<input name="txtPhoneNumber" type="text" id="txtPhoneNumber" class="input_text" value="" onkeypress="return isNumberKey(event)" ng-model="card.billphone" required>
					</div> 
				</div> <!-- end of billing_wrap -->

			<!-- terms of use -->
			<h6 class="sub_title2"><span>Terms</span> of Use</h6>
			<div id="divUserDahnyoga" style="display:" class="form_box terms_wrap" tabindex="0">
				<p>Dahn Yoga &amp; Health Centers, Inc. ("Dahn Yoga ") operates the web site www.dahnyoga.com (“Site”) to provide online access to information about Dahn Yoga and the products, services, and opportunities we provide (the "Service"). By accessing and using this Site, you agree to each of the terms and conditions set forth herein ("Terms of Use"). Additional terms and conditions applicable to specific areas of this Site or to particular content or transactions are also posted in particular areas of the Site and, together with these Terms of Use, govern your use of those areas, content or transactions. These Terms of Use, together with applicable additional terms and conditions, are referred to as this "Agreement".</p>
				<br>
				<p>Dahn Yoga reserves the right to modify this Agreement at any time without giving you prior notice. Your use of the Site following any such modification constitutes your agreement to follow and be bound by the Agreement as modified. The last date these Terms of Use were revised is set forth below.</p>
				<br>
				<p>1. Products, Content and Specifications</p>
				<br>
				<p>All features, content, specifications, products and prices of products and services described or depicted on this Site, are subject to change at any time without notice. Certain weights, measures and similar descriptions are approximate and are provided for convenience purposes only. Dahn Yoga will make all reasonable efforts to accurately display the attributes of our products, including the applicable colors; however, the actual color you see will depend on your computer system, and we cannot guarantee that your computer will accurately display such colors. The inclusion of any products or services on this Site at a particular time does not imply or warrant that these products or services will be available at any time. It is your responsibility to ascertain and obey all applicable local, state, federal and international laws (including minimum age requirements) in regard to the possession, use and sale of any item purchased from this Site. By placing an order, you represent that the products ordered will be used only in a lawful manner. All videocassettes, DVDs and similar products sold are for private, home use (where no admission fee is charged), non-public performance and may not be duplicated.</p>
			</div><br>
			<div id="divUserBody" style="display:none;" class="form_box terms_wrap" tabindex="0">
				<p>Body &amp; Brain Yoga &amp; Health Centers, Inc. ("Dahn Yoga ") operates the web site www.dahnyoga.com (“Site”) to provide online access to information about Dahn Yoga and the products, services, and opportunities we provide (the "Service"). By accessing and using this Site, you agree to each of the terms and conditions set forth herein ("Terms of Use"). Additional terms and conditions applicable to specific areas of this Site or to particular content or transactions are also posted in particular areas of the Site and, together with these Terms of Use, govern your use of those areas, content or transactions. These Terms of Use, together with applicable additional terms and conditions, are referred to as this "Agreement".<br>&nbsp;<br>Dahn Yoga reserves the right to modify this Agreement at any time without giving you prior notice. Your use of the Site following any such modification constitutes your agreement to follow and be bound by the Agreement as modified. The last date these Terms of Use were revised is set forth below.<br>1. Products, Content and Specifications</p>
			</div>
			<div class="terms_check" id="gototerm">
				<div class="check_wrap">
					<input id="chbTerms" type="checkbox" ng-init="acceptterm = false" ng-model="acceptterm" name="chbTerms"><label for="chbTerms"> I accept the Terms of Use</label>
						<span id="CustomValidatorTerms" style="color:Red;" ng-show="checkthis">(Please Accept Terms of Use!)</span>

				</div>
			</div>

				<!-- order summary -->
				<h5 class="sub_title2"><span>Order</span> summary</h5>
				<div class="form_box summary_wrap">
					<p class="summary_title">{[{sumsession}]}</p>
					<ul>
						<li><span class="lb">Name: </span>{[{sumname}]}</li>
						<li><span class="lb">preferred Location:</span>{[{sumcentername}]} {[{sumlocation}]},{[{sumzip}]}</li>
						<li><span class="lb">Expected Date &amp; Time: </span>{[{sumdate}]} {[{sumtime}]}</li>
						<li><span class="lb">Amount to be charged to your card: </span>${[{sumtotal}]}</li>
					</ul>
				</div>

				<div class="btn_wrap">
					<input type="submit" name="btnSubmit" value="SUBMIT" id="btnSubmit" class="fblue" style="margin-bottom:10px">
					<p class="btn_desc">The confirmation page may take several seconds to appear.</p>
				</div>
			</div>

		</div>

	<!-- </div> -->
	</form>



	
	<div class="introffer" ng-if="finalstep">
		<!-- <h2>TRIAL MEMBERSHIP</h2> -->
		<ul class="offer_menu" id="non-printable">
			<li class="omenu02 sel1"><a href="" ng-click="backtofirststep()">find location</a></li>
			<li class="omenu01 sel2">choose program</li> 
			<li class="omenu03 sel3">schedule session &amp; class</li>
			<li class="omenu04 on sel4">confirm your appointment</li>
		</ul>

		<div class="monthwrap" id="gototop">
			<div  id="non-printable">
				<p class="graytxt minwidth">Congratulations! You have made and appointment to try Dahn Yoga. Get Ready for less stress and more energy!<br>We will contact you soon to confirm your appointment.<br>Please be sure to arrive 15 minutes before class to meet with an instructor.<br> Dahn yoga recommends loose fitting clothing.</p>
				<br>
				<p class="graytxt minwidth">Please print this page for your records.<br>If you have any questions,please contact us at:<br>Phone: <span class="blacktext">{[{centerphone}]}</span> or Email: <a href="mailto:{[{centeremail}]}">{[{centeremail}]}</a><br>Your confirmation number is 111214502541<br>Starter Package: <span class="blacktext">{[{sessionclass}]}</span></p>
				<br>
				<p class="graytxt minwidth">We look forward seeing you!</p>
				<br>
				<br>
				<div class="printwrap">
					<button type="submit" onclick="myFunction()" name="btnSubmit" value="Submit" id="btnSubmit" class="bene_print"><p class="beneprinticospan">print</p></button>

				</div> <!-- end btn wrap -->
			</div>
			<div class="printdetails" id="printable">

				<div class="dahnyogalogo"><img id="dahnyogalogo" src="/img/bnblogo.gif"></div>

				<p class="bigred">Your Appointment</p>

				<p class="graytxt">Starter Package: <span class="blacktext">{[{sessionclass}]}</span></p>

				<p class="graytxt">Name: <span class="blacktext">{[{fullname}]}</span></p>

				<p class="graytxt">When: <span class="blacktext">{[{scheddate | date: 'fullDate'}]}, {[{schedhour}]} </span><em>(Your appointment is subject to availability.)</em></p>

				<p class="graytxt">Where: <span class="bluetext">{[{centertitle}]}</span><br>
					<em>{[{centeraddress}]}, {[{centerzip}]}</em><br>
					<em>Phone:{[{centerphone}]}</em>
				</p>

				<p class="graytxt">Confirmation Number: <span class="blacktext">{[{confirmnumber}]}</span></p>

				<p class="graytxt">Payment: <span class="blacktext">Paid ${[{totalprice}]} {[{paymenttype}]}</span></p>

				<div class="result_map">
					<div id="map_canvas">
						<ui-gmap-google-map center="map.center" zoom="map.zoom" draggable="true" options="options" bounds="map.bounds">
						<ui-gmap-markers models="randomMarkers" coords="'self'"  icon="'icon'">
						<ui-gmap-windows show="show">
						<div ng-non-bindable>{[{title}]}</div>
						</ui-gmap-windows>
						</ui-gmap-markers>
						</ui-gmap-google-map>
					</div>
				</div>
			</div>
		</div>
	</div>











</div>

<script language="javascript">

	function myFunction() {
		window.print();
	}

  function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;
    return true;
  }
</script>



<style>
/*.chrono {
	position:fixed; top:0; left:0;
	height:100%; width:100%;
	background:rgba(0,0,0,.7);
	z-index:99999999 !important;
}

.writeModal {
	display:block !important;
	position:fixed; top:100px !important;
	max-width:400px !important;
}

.hidden {
	display:none;
	visibility:hidden;
}*/

.loaders,
.loaders:before,
.loaders:after {
  background: #ffffff;
  -webkit-animation: load1 1s infinite ease-in-out;
  animation: load1 1s infinite ease-in-out;
  width: 1em;
  height: 4em;
}
.loaders:before,
.loaders:after {
  position: absolute;
  top: 0;
  content: '';
}
.loaders:before {
  left: -1.5em;
  -webkit-animation-delay: -0.32s;
  animation-delay: -0.32s;
}
.loaders {
  text-indent: -9999em;
  margin: 250px auto;
  position: relative;
  font-size: 11px;
  -webkit-transform: translateZ(0);
  -ms-transform: translateZ(0);
  transform: translateZ(0);
  -webkit-animation-delay: -0.16s;
  animation-delay: -0.16s;
}
.loaders:after {
  left: 1.5em;
}
@-webkit-keyframes load1 {
  0%,
  80%,
  100% {
    box-shadow: 0 0 #ffffff;
    height: 4em;
  }
  40% {
    box-shadow: 0 -2em #ffffff;
    height: 5em;
  }
}
@keyframes load1 {
  0%,
  80%,
  100% {
    box-shadow: 0 0 #ffffff;
    height: 4em;
  }
  40% {
    box-shadow: 0 -2em #ffffff;
    height: 5em;
  }
}

.txtloading{
	width: 100%;
	overflow: hidden;
	position:fixed;
	margin: 10px auto;
	top:350px;
}
.loadingtext{
	width: 200px;
	margin: 10px auto;
	top:350px;
}

</style>

<div class="chrono hidden"  id="non-printable">
	<div class="loaders">Loading...</div>
	<div class="txtloading">
		<div class="white_text loadingtext"><center>Loading Please wait...</center></div>
	</div>
</div>