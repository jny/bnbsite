<?php
if($logoimage->value1 == 1){
  header('Location: /../../maintenance/');
}
?><?php 
  $data_db = $postinfo;
  header('Content-Type: text/xml');
 ?>
<rss version="2.0">
<channel>
 <title>RSS Title</title>
 <description>RSS Feed</description>
 <link><?php echo $this->config->application->BaseURL ?>/feed/index</link>
 <lastBuildDate><?php echo date('r'); ?> </lastBuildDate>


 <?php
 foreach($data_db as $key => $value){

  ?>
  <item>
    <title><![CDATA[<?php echo $data_db[$key]->title ?>]]></title>
    <pubDate>
      <?php $datePublished = date_create($data_db[$key]->datecreated);
            echo date_format($datePublished, 'M, d y');
      ?>
    </pubDate>
    <link><?php echo $this->config->application->BaseURL ?>/yogalife/view/<?php echo $data_db[$key]->newsslugs ?></link>

    <description><![CDATA[<?php  echo substr(strip_tags($data_db[$key]->body),0,400).'...'; ?>]]></description>

  </item>

  <?php
  }

  ?>
</channel>
</rss>