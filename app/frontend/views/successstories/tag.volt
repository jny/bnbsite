<?php echo $this->getContent()?>
<?php
if($logoimage->value1 == 1){
  header('Location: /../../maintenance/');
}
?>
<div class="navi" style="margin: 0px 0px 30px; padding: 2px 0px 0px; border-width: 0px 0px 1px; border-bottom-style: solid; border-bottom-color: rgb(230, 230, 230); font-family: 'Droid Sans', sans-serif, Arial; font-size: 12px; font-stretch: inherit; line-height: 30px; vertical-align: baseline; height: 30px; color: rgb(227, 24, 54);">
    <a href="/" style="margin: 0px; padding: 0px; border: 0px; font-family: inherit; font-size: inherit; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; vertical-align: baseline; text-decoration: none; color: rgb(227, 24, 54);">
      Home
    </a>

    <span class="current" style="margin: 0px; padding: 0px; border: 0px; font-family: inherit; font-size: inherit; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; vertical-align: baseline; color: rgb(102, 102, 102);">
      Success Stories
    </span>
</div>

<div class="container-fluid padding_top_md" ng-controller="successCtrl">
  <div class="row">
    <div class="col s12">
      <section id="special_page_sidecontent">
        <div id="special_page_sidebarmenu">
          <div class="sidebar-head">B&B Yoga helped my...</div>
          <ul class="sidebarmenu-list">
            <li><a href="/success-stories/stress/1"><label class="dot"> &#x25AA; </label>stress</a></li>
            <li><a href="/success-stories/peace/1"><label class="dot"> &#x25AA; </label>peace</a></li>
            <li><a href="/success-stories/pain/1"><label class="dot"> &#x25AA; </label>pain</a></li>
            <li><a href="/success-stories/flexibility/1"><label class="dot"> &#x25AA; </label>flexibility</a></li>
            <li><a href="/success-stories/meditation/1"><label class="dot"> &#x25AA; </label>meditation</a></li>
            <li><a href="/success-stories/strength/1"><label class="dot"> &#x25AA; </label>strength</a></li>
            <li><a href="/success-stories/anxiety/1"><label class="dot"> &#x25AA; </label>anxiety</a></li>
            <li><a href="/success-stories/focus/1"><label class="dot"> &#x25AA; </label>focus</a></li>
            <li><a href="/success-stories/back-pain/1"><label class="dot"> &#x25AA; </label>back pain</a></li>
            <li><a href="/success-stories/joint-pain/1"><label class="dot"> &#x25AA; </label>joint pain</a></li>
          </ul>
        </div>

        <a href="/successstories/write" class="a_share_your_experience">
          <img src="/img/frontend/share.gif"> <br>
          Share <br> Your Experience
        </a>

        <div id="newsletter">
          <div class="frmtitle">e-Newsletter</div>
          <form action="//bodynbrain.us3.list-manage.com/subscribe/post?u=cfc22757143336e0cfcf90eef&amp;id=eef0458b00" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
            <input type="email" value="" name="EMAIL" class="required email text" id="mce-EMAIL" placeholder="E-mail Address">
            <input type="submit" value="" name="subscribe" id="mc-embedded-subscribe" class="send">
          </form>
          <div class="clearboth"></div>
        </div>
      </section>

      <section id="stories_slider_wrapper">
        <h3 class="h3_title margin_top_s">Success Stories</h3>
        The benefits of Body & Brain yoga are various. With regular and diligent practice, you can improve your condition greatly.
        Many Body & Brain yoga practitioners are saying that they got lots of physical and mental benefits.
        <hr style="border:none; background:#fddcdc; height:2px">
        <?php if($spotlights != null) { ?>
        <h5 class="red2_text margin_left_s font_bolder">Spotlight</h5>
        <div class="success_slider_wrapper">
          <div class="success_slider">
            <img src="/img/frontend/btn_left.png" class="success_slider_arrows float_left" ng-click="back()">
            <img src="/img/frontend/btn_right.png" class="success_slider_arrows float_right" ng-click="next()">
              <div class="hide_overflow">
                <div class="two_story_container" ng-style="myStyle">
          <?php $widget = 1; ?>
          <?php foreach($spotlights as $spotlight) { ?>
          <?php $story_slugs = preg_replace("![^a-z0-9]+!i", "-", strtolower($spotlight->subject)); ?>
                  <div class="widget_half widget<?php echo $widget;?>">
       
                      <div class="flex-container">
                        <div class="flex-left">
                          <div class="circleimages" style="float:left;">
                            <div class="circle bg_centered" 
                                 style="background:url('<?php

                                     $varheaders = get_headers($this->config->application->amazonlink . '/uploads/testimonialimages/'.$spotlight->photo);
                                     if($varheaders[0] == 'HTTP/1.1 200 OK'){
                                      echo $this->config->application->amazonlink . '/uploads/testimonialimages/'.$spotlight->photo;
                                    }
                                    else{
                                      echo '/img/blue-rose.jpg';
                                    }

                                    ?>')">
                            </div>
                          </div>  
                        </div>
                        <div class="flex-right">
                          <div class="padding_left_s">
                            <h6 class="h6-text"><?php echo $spotlight->subject; ?></h6>
                            <?php $story_slugs = preg_replace("![^a-z0-9]+!i", "-", strtolower($spotlight->subject)); ?>
                            <a href="/successstories/view/<?php echo $spotlight->ssid; ?>/<?php echo $story_slugs;?>" class="read-more">Read More &#10148;</a>
                            <label class="lbl-stories-author"><?php echo $spotlight->author; ?></label>
                          </div>
                        </div>
                      </div>
     
                  </div>

          <?php $widget++; ?> <input type="hidden" ng-init="spot=<?php echo $widget;?>" ng-value="<?php echo $widget; ?>">
          <?php } ?>

              </div>
            </div>  
          </div>
        </div>
        <?php } else { echo "<span class=\"warning_message\">No Available Spotlight Story</span>"; } ?>
      </section>

      <section id="middle_body" class="successstories-middle_body">
      <?php if($stories != null ) { ?>
       There are a total of <span class="bluish_text"><?php echo $totalstories; ?></span> Success Stories. <br><br>
        <h3 class="h3title">Latest Stories</h3>
        <hr class="hr_thin">
<?php   foreach($stories as $story) { ?>
<?php $story_slugs = preg_replace("![^a-z0-9]+!i", "-", strtolower($story->subject)); ?>
          <div>
            <a href="/successstories/view/<?php echo $story->ssid; ?>/<?php echo $story_slugs; ?>" class="dev_text">
              <span class="no-margin teal_text story_subject bl" style="font-size:138%;">
                <?php echo $story->subject;?>
              </span>
              <label class="margin_bottom_s font_latest_label" style="display:inline-block">
                <?php echo $story->author . ", " . $story->centertitle . ", " . $story->state ?> </label> <br>
              <div class="li_list_success">
                <div class="thumbnail_md" style="background:url(<?php

                                     $varheaders = get_headers($this->config->application->amazonlink . '/uploads/testimonialimages/'.$story->photo);
                                     if($varheaders[0] == 'HTTP/1.1 200 OK'){
                                      echo $this->config->application->amazonlink . '/uploads/testimonialimages/'.$story->photo;
                                    }
                                    else{
                                      echo '/img/blue-rose.jpg';
                                    }

                                    ?>);">
                </div>
                <div class="padding_sides_2 success_details">
                  <?php echo $story->metadesc; ?> <br>
                  <?php $story_slugs = preg_replace("![^a-z0-9]+!i", "-", strtolower($story->subject)); ?>
                  <span class="bluish_text">Read full story &#x0203A;</span> 
                </div>
              </div>
            </a>
            <hr class="hr_thin margin_vert_sm">
          </div>
<?php   } ?>
        
        <?php
              echo '<div class="myPagination">';
              echo '<span first_page_line></span><a href="/success-stories/'.$tag.'/1'.'" class="first_page"></a>&nbsp';
              $pageLastNumberKey = $totalpage - 4;
        if($page >= $pageLastNumberKey) {
          if($page > 1)
            echo '<a href="/success-stories/'.$tag.'/'.($page - 1).'" class="back_pagination"></a>&nbsp';
            for($i=max(1, $pageLastNumberKey); $i<=max(1, min($totalpage,$page+4)); $i++) 
          {
           if($i == $page)
            echo '<span class="active_page pagination_pages">'.$i.'</span>';
           else
            echo '<a href="/success-stories/'.$tag.'/'.$i.'" class="other_pages pagination_pages">'.$i.'</a>&nbsp';
          }
        }
        else {  
          if($page > 1) 
            echo '<a href="/success-stories/'.$tag.'/'.($page - 1).'" class="back_pagination"></a>&nbsp';
            for($i=max(1, $page); $i<=max(1, min($totalpage,$page+4)); $i++)
          {
           if($i == $page)
            echo '<span class="active_page pagination_pages">'.$i.'</span>';
           else
            echo '<a href="/success-stories/'.$tag.'/'.$i.'" class="other_pages pagination_pages">'.$i.'</a>&nbsp';
          }
        }
        if ($page < $totalpage)
          echo '<a href="/success-stories/'.$tag.'/'.($page + 1).'" class="next_pagination"></a>&nbsp;';
          echo '<a href="/success-stories/'.$tag.'/'.$totalpage.'" class="last_page"></a><span last_page_line></span>';
          echo"</div>";
        ?>
      <?php } else { echo "<span class='warning_message'>No Available Story to show</span>"; } ?>
      </section>

      <div class="clearboth"></div>
      <aside id="sidebar-right">
        <div class="sidebar-container">
          <div class="col-4">
              <div id="newsletter2">
                <div class="frmtitle">e-Newsletter</div>
                <form>
                  <div><input type="text" class="text" placeholder="E-mail Address" ></div>
                  <input type="button" class="send" value="" />
                </form>
                <div class="clearboth"></div>
              </div>
            <a href="/getstarted/starterspackage" class="add"><img id="starter-package" src="/img/frontend/banner_getstarted_small.jpg" class="full_width" /></a>
          </div>
            <div class="col-4">
              <div class="popularpost">
                <div class="frmtitle marg_lft pop_feat" >Popular Features</div>
              <?php foreach($popular_features as $data) { ?>
                  <div class="post">
                    <a href="<?php echo $articles; ?>/view/<?php echo $data->newsslugs;?>">
                    <img class="post_pic img_thumbnail" src="<?php echo $this->config->application->amazonlink.'/uploads/newsimage/'.$data->banner; ?>" />
                  </a>
                  <div class="small-text">
                    <a href="<?php echo $articles; ?>/view/<?php echo $data->newsslugs;?>">
                      <?php  echo substr($data->description, 0, 50) . ".."; ?>
                    </a>
                  </div>
                  <div class="clearboth"></div>
                  </div>
              <?php } ?>
                <div class="clearboth"> </div>
              </div>
            </div>
          </div>
      </aside>
    </div> <!-- end of col s12 -->
  </div> <!-- end of row -->
</div> <!-- end of container -->
<style>
/*.li_list_success .success_details {
   display:inline-block!important;
   max-width:75%!important;
   vertical-align:top!important;
}*/

</style>

