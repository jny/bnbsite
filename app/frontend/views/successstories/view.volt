<?php 

echo $this->getContent();

if($logoimage->value1 == 1){
  header('Location: /../../maintenance/');
}

  //check if picture is on AMAZON or NOT
    function url_exists($url) {
      $hdrs = @get_headers($url);
      // echo @$hdrs[1]."\n";
      return is_array($hdrs) ? preg_match('/^HTTP\\/\\d+\\.\\d+\\s+2\\d\\d\\s+.*$/',$hdrs[0]) : false;
    }

?>
<div id="crumbs_cont">
  <ul id="crumbs">
    <li class="crumbsli"><a class="crumbs red_text" href="/">Home</a></li>
    <li class="crumbsli"><a class="red_text" href="/success-stories/1" style="font-size:12px">Success Stories</a></li>
  </ul>
</div>

<div class="container-fluid padding_top_md" ng-controller="viewstoryCtrl">
  <div class="row">
    <div class="col s12">
      <section id="special_page_sidecontent">
        <div id="special_page_sidebarmenu">
          <div class="sidebar-head">B&B Yoga helped my...</div>
          <ul class="sidebarmenu-list">
            <li><a href="/success-stories/stress/1"><label class="dot"> &#x25AA; </label>stress</a></li>
            <li><a href="/success-stories/peace/1"><label class="dot"> &#x25AA; </label>peace</a></li>
            <li><a href="/success-stories/pain/1"><label class="dot"> &#x25AA; </label>pain</a></li>
            <li><a href="/success-stories/flexibility/1"><label class="dot"> &#x25AA; </label>flexibility</a></li>
            <li><a href="/success-stories/meditation/1"><label class="dot"> &#x25AA; </label>meditation</a></li>
            <li><a href="/success-stories/strength/1"><label class="dot"> &#x25AA; </label>strength</a></li>
            <li><a href="/success-stories/anxiety/1"><label class="dot"> &#x25AA; </label>anxiety</a></li>
            <li><a href="/success-stories/focus/1"><label class="dot"> &#x25AA; </label>focus</a></li>
            <li><a href="/success-stories/back-pain/1"><label class="dot"> &#x25AA; </label>back pain</a></li>
            <li><a href="/success-stories/joint-pain/1"><label class="dot"> &#x25AA; </label>joint pain</a></li>
          </ul>
        </div>

        <a href="/successstories/write" class="a_share_your_experience">
          <img src="/img/frontend/share.gif"> <br>
          Share <br> Your Experience
        </a>

        <div id="newsletter">
          <div class="frmtitle">e-Newsletter</div>
          <form action="//bodynbrain.us3.list-manage.com/subscribe/post?u=cfc22757143336e0cfcf90eef&amp;id=eef0458b00" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
            <input type="email" value="" name="EMAIL" class="required email text" id="mce-EMAIL" placeholder="E-mail Address">
            <input type="submit" value="" name="subscribe" id="mc-embedded-subscribe" class="send">
          </form>
          <div class="clearboth"></div>
        </div>
      </section>

      <section id="stories_slider_wrapper">
        <h5 class="margin_top_s"><?php echo $viewstory->subject; ?></h5>
        <a href="/{[{story.centerslugs}]}" class="inbl margin_top-10px bluish_text text_underline"><?php echo $viewstory->author; ?>, <?php echo $viewstory->centertitle; ?> center, (<?php echo $viewstory->centerstate; ?>)</a>
      </section>

      <section id="middle_body">
      	<div class="bl" ng-if="story">

          <span class="bg_cover bg_no_repeat margin_right_5px float_left" style="display:inline-block; width:200px; height:240px; background:#000 url(<?php

           $varheaders = get_headers($this->config->application->amazonlink . '/uploads/testimonialimages/'.$viewstory->photo);
          // var_dump($varheaders) ;
          if($varheaders[0] == 'HTTP/1.1 200 OK'){
            echo $this->config->application->amazonlink . '/uploads/testimonialimages/'.$viewstory->photo;
          }
          else{
            echo '/img/blue-rose.jpg';
            // echo $this->config->application->amazonlink . '/uploads/testimonialimages/{[{story.photo}]}';
          }

           ?>)" align="left">&nbsp;
          </span>

      	  <span><?php echo $viewstory->details; ?></span>
      

          <br><br>
    	  </div>
        <div class="bl clear" ng-if="relatedstory != ''">
          <br>
          <hr class="hr_thin margin_vert_sm">
          <span class="red_text size_20">Stories with similar experiences:</span>
          <ul>
          
            <li ng-repeat="related in relatedstory">
              <a href="/successstories/view/{[{related.ssid}]}/{[{related.subject | replaceslugs}]}" class="bluish_text font_90">
                <label class="dot"> &#x25AA; </label> 
                <span class="text_underline">{[{related.subject}]}</span>
              </a>
            </li>
          </ul>
        </div>

      	<div ng-if="story == false">
  			<span class="warning_message">Error! Story not found.</span>
      	</div>

      </section>

      <div class="clearboth"></div>
      <aside id="sidebar_right">
        <div class="sidebar-container">
          <div class="col-4">
            <div id="newsletter2">
              <div class="frmtitle">e-Newsletter</div>
              <form>
                <div><input type="text" class="text" placeholder="E-mail Address" ></div>
                <input type="button" class="send" value="" />
              </form>
              <div class="clearboth"></div>
            </div>
            <a href="/getstarted/starterspackage" class="add"><img id="starter-package" src="/img/frontend/banner_getstarted_small.jpg" class="full_width" /></a>
          </div>
          <div class="col-4">
            <div class="popularpost">
            <?php if($popular_features != null) { ?>
              <div class="frmtitle marg_lft pop_feat" >Popular Features</div>
              <?php foreach($popular_features as $news) { ?>
                <div class="post">
                  <a href="<?php echo $articles; ?>/view/<?php echo $news->newsslugs;?>">
                    <img src="<?php echo $this->config->application->amazonlink .'/uploads/newsimage/'. $news->banner;?>" class="post_pic img_thumbnail">
                  </a>
                
                  <div class="small-text">
                    <a href="<?php echo $articles; ?>/view/<?php echo $news->newsslugs;?>" title="<?php echo $news->description;?>">
                      <?php echo substr($news->description,0,50).".."; ?>
                    </a>
                  </div>
                  <div class="clearboth"></div>
                </div>
              <?php } //;foreach ?>
            <?php } //;if ?>
              <div class="clearboth"> </div>
            </div> <!-- //popularpost -->
          </div>
        </div>
      </aside>
    </div> <!-- end of col s12 -->
  </div> <!-- end of row -->
</div> <!-- end of container -->

