<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;


class IndexController extends ControllerBase
{
    public function indexAction()
    {
        $this->view->userip = $this->kh_getUserIP();
        // $this->angularLoader(array(
        //     'userfactory' => 'fe/scripts/factory/user.js'
        // ));
       
        /*var_dump( $this->config->application->ApiUR);*/

        $gotoroute = $this->config->application->ApiURL . '/fe/index/properties';
        $curl = curl_init($gotoroute);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->application->curlRest);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        curl_close($curl);
        $decode = json_decode($curl_response);
        $this->view->spotlights = $decode->spotlights;
        $this->view->locations = $decode;

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW); 
        $this->view->logoimage = $this->curl('settings/managesettings');
        $this->view->script_google = $this->curl('/settings/script');
    }
    public function route404Action() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW); 
        $this->view->logoimage = $this->curl('settings/managesettings');
        $this->view->script_google = $this->curl('/settings/script');
    }

    public function kh_getUserIP(){
        $client  = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote  = $_SERVER['REMOTE_ADDR'];

        if(filter_var($client, FILTER_VALIDATE_IP)){
            $ip = $client;
        }elseif(filter_var($forward, FILTER_VALIDATE_IP)){
            $ip = $forward;
        }else{
            $ip = $remote;
        }
        return $ip;
    }
}

