<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;

class PageController extends ControllerBase
{
    public function indexAction()
    {

    }
    public function viewAction($pageslugs)
    {
        $this->view->pageSlugs = $pageslugs;
        $this->view->leftsidebartoggle = true;
        $pageidglobal = 0;

        $service_url = $this->config->application->ApiURL.'/fe/page/view/' . $pageslugs;
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->application->curlRest);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);

        if(@$decoded->page == null){
            $this->pageAction($pageslugs);
            $this->view->pagelayout = 'center';
            $this->view->pick("center/page");
        }
        else {
            $this->view->pagelayout = 'page';
            $this->view->pageid = $decoded->page->pageid;
            $this->view->title = $decoded->page->title;
            $this->view->pageslugs = $decoded->page->pageslugs;
            $this->view->body = $decoded->page->body;
            $this->view->pagelayout = $decoded->page->pagelayout;
            $pageidglobal = $decoded->page->pageid;
            $this->view->fbcapc = $decoded->page->fbcapc;

            //SEO
            $this->view->metatitle = $decoded->page->metatitle;
            $this->view->metatags = $decoded->page->metatags;
            $this->view->metadesc = $decoded->page->metadesc;

            $this->view->leftsidebarname = "";
            $this->view->leftsidebaritem = $decoded->pageleftbar->item;
            if($decoded->pageleftbar->item == 1)
            {
                $this->view->leftsidebarname ='About';
                $pageslugs = array(
                            '/what-is',
                            '/what-is/how-different',
                            '/what-is/energy-principles',
                            '/what-is/ilchi-lee',
                            '/what-is/ceo-message',
                            '/what-is/yoga-class-guarantee',
                            '/what-is/research',
                            '/what-is/faq'
                            );
                $this->view->pageslugs = $pageslugs;
            }
            else if($decoded->pageleftbar->item == 2)
            {
                $this->view->leftsidebarname ='Classes';
                $pageslugs = array(
                            '/classes',
                            '/classes/video-classes',
                            '/classes/energy-martial-arts',
                            '/classes/energy-meditation',
                            '/classes/core-strengthening-revitalizing'
                            );
                $this->view->pageslugs = $pageslugs;
            }
            else if($decoded->pageleftbar->item == 3)
            {
                $this->view->leftsidebarname ='Workshops';
                $pageslugs = array(
                            '/workshops',
                            '/workshops/initial-awakening',
                            '/workshops/finding-true-self',
                            '/workshops/sedona-retreat',
                            '/workshops/solar-body-natural-healing-course',
                            '/workshops/dahnmudo',
                            '/workshops/brain-management-training',
                            '/workshops/dahn-master-course',
                            '/workshops/meditation-tours',
                            '/workshops/workshopschedules/all',
                            '/workshops/healthier-together'
                            );
                $this->view->pageslugs = $pageslugs;
            }
            else if($decoded->pageleftbar->item == 4)
            {
                $this->view->leftsidebarname ='Wellness at Work';
                $pageslugs = array(
                            '/wellness-at-work',
                            '/wellness-at-work/our-programs',
                            '/wellness-at-work/getting-started',
                            '/wellness-at-work/success-stories'
                            );
                $this->view->pageslugs = $pageslugs;
            }
            else if($decoded->pageleftbar->item == 5)
            {
                $this->view->leftsidebarname ='Responsibility';
                $pageslugs = array(
                            '/responsibility',
                            '/responsibility/community-classes',
                            '/responsibility/project-orange',
                            '/responsibility/book-donations',
                            '/responsibility/nevada-1st-project',
                            '/responsibility/nicaragua-project'
                            );
                $this->view->pageslugs = $pageslugs;
            }
            else if($decoded->pageleftbar->item == 6)
            {
                $this->view->leftsidebarname ='Franchise';
                $pageslugs = array(
                            '/franchise',
                            '/franchise/why-open-a-franchise',
                            '/franchise/franchising-options',
                            '/franchise/owners-success-stories',
                            '/franchise/get-started',
                            );
                $this->view->pageslugs = $pageslugs;
            }
            else if($decoded->pageleftbar->item == 7)
            {
                $this->view->leftsidebarname ='Miscellaneous';
                $pageslugs = array(
                            '/contactus',
                            '/terms-of-use',
                            '/privacy-policy',
                            '/site-map',
                            '/newsletter/enewsletter',
                            '/newsletter/pdfnewsletter'
                            );
                $this->view->pageslugs = $pageslugs;
            }
            else if($decoded->pageleftbar->item == 8)
            {
                $this->view->leftsidebarname ='Our Company';
                $pageslugs = array(
                            'about-us',
                            'our-instructors',
                            'our-history',
                            );
                $this->view->pageslugs = $pageslugs;
            }
             else if($decoded->pageleftbar->item == 9)
            {
                $this->view->leftsidebarname ='Our Company';
                $pageslugs = array(
                            '/personal-services',
                            '/personal-services/private-sessions',
                            '/personal-services/lifestyle-coaching',
                            '/personal-services/personal-assessment',
                            '/personal-services/customized-exercises'
                            );
                $this->view->pageslugs = $pageslugs;
            }
             else if($decoded->pageleftbar->item == 10)
            {
                $this->view->leftsidebarname ='Press';
            }
            else
            {
                $this->view->leftsidebartoggle = false;
            }

            $this->view->leftsidebar = $decoded->leftsidebar;

            $this->view->rightsidebar = $decoded->rightsidebar;

            $getginfo =  $decoded->rightsidebar;
            $this->view->rightsidebartoggle = true;
            if(count($decoded->rightsidebar) == 0) {
                $this->view->rightsidebartoggle = false;
            }

            $this->view->item1 = false;
            $this->view->item2 = false;
            $this->view->item3 = false;
            $this->view->item4 = false;
            $this->view->item5 = false;

            foreach ($getginfo as $key => $value) {
                if($getginfo[$key]->item == 1) {
                    $this->view->item1 = true;
                }
                else if($getginfo[$key]->item == 2) {
                    $this->view->item2 = true;
                }
                else if($getginfo[$key]->item == 3) {
                    $this->view->item3 = true;
                }
                else if($getginfo[$key]->item == 4) {
                    $this->view->item4 = true;
                }
                else if($getginfo[$key]->item == 5) {
                    $this->view->item5 = true;
                }
            } //foreach

            $this->view->popularnews = $decoded->popularnews;

            $this->view->logoimage = $this->curl('/settings/managesettings');
            $this->view->script_google = $this->curl('/settings/script');
        }
    }

    public function pageAction($centerslugs) {
            $service_url = $this->config->application->ApiURL.'/fe/center/page/' . $centerslugs;
            $curl = curl_init($service_url);
            curl_setopt($curl, CURLOPT_CAINFO, $this->config->application->curlRest);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $curl_response = curl_exec($curl);
            if ($curl_response === false) {
                $info = curl_getinfo($curl);
                curl_close($curl);
                die('error occured during curl exec. Additioanl info: ' . var_export($info));
            }
            curl_close($curl);
            $decoded = json_decode($curl_response);
            if($decoded->centerprop != null) {
                $this->view->centerpage = $decoded;
                // $this->view->timezone = $decoded->timezone;
                $this->view->schedules = $decoded->schedule;
                $this->view->regfee = $decoded->regfee;
                $this->view->regclass = $decoded->regclass;
                $this->view->reg1on1intro = $decoded->reg1on1intro;
                $this->view->centermembership = $decoded->centermembership;
                $this->view->classpackages = $decoded->classpackages;
                $this->view->privatesession = $decoded->privatesession;
                $this->view->center = $decoded->centerprop;
                $this->view->centerhours = $decoded->centerhours;
                $this->view->eventsdata = $decoded->eventslist;
                $this->view->getcenterslugs = $centerslugs;
                $this->view->email = $decoded->emaildata->email;
                $this->view->pagestatus = "active";
                $this->view->pagetitle = $decoded->centerprop->centertitle;
                $this->view->fbcapc = $decoded->centerprop->fbcapc;
                  //SEO
               $this->view->metatitle = $decoded->centerprop->centertitle . ", ".$decoded->centerprop->centerstate.": Body & Brain Yoga, Tai Chi, Meditation";
                $this->view->metatags = $decoded->centerprop->metatitle;
                $this->view->metadesc = $decoded->centerprop->metadesc;
            } else {
                $this->view->pagestatus = null;
            }
            $this->view->logoimage = $this->curl('/settings/managesettings');
        $this->view->script_google = $this->curl('/settings/script');
            $this->view->leftsidebarname = "";
    }
}
