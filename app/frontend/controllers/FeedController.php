<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;


class FeedController extends ControllerBase {

    public function indexAction() {

        $service_url_news = $this->config->application->ApiURL. '/news/rss/0';

        $curl = curl_init($service_url_news);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->application->curlRest);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curl_response = curl_exec($curl);

        if ($curl_response === false) 
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }

        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->postinfo = $decoded;
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->logoimage = $this->curl('/settings/managesettings');
        $this->view->script_google = $this->curl('/settings/script');
    } 
}

?>