<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;

class YogalifeController extends ControllerBase
{
    public function indexAction()
    {
        $this->view->leftsidebarname = "";
        $this->view->logoimage = $this->curl('/settings/managesettings');
        $this->view->script_google = $this->curl('/settings/script');
        //list Category

        $service_url_news = $this->config->application->ApiURL. '/fe/bnbbuzz/index';
        $curl = curl_init($service_url_news);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->application->curlRest);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false)
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->newscategory = $decoded->newscategory;

        $this->view->randomnewscategory = $decoded->randomenews;
        //SEO
        $this->view->metatitle = "Yoga & Meditation Tips, Tai Chi, Qigong | Body & Brain yoga";
        $this->view->metatags = "yogalife,Yoga,Meditation,Tips,Tai Chi, Qigong";
        $this->view->metadesc = "Learn what it takes to have a healthy mind, body and spirit with Yoga Republic. Topics include yoga, meditation, tai chi, and qigong.";


        //featured news
        // $service_url = $this->config->application->ApiURL. '/news/frontend/featurednews';
        // $curl = curl_init($service_url);
        // curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        // $curl_response = curl_exec($curl);
        // if ($curl_response === false) {
        //     $info = curl_getinfo($curl);
        //     curl_close($curl);
        //     die('error occured during curl exec. Additioanl info: ' . var_export($info));
        // }
        // curl_close($curl);
        // $decoded = json_decode($curl_response);
        $this->view->featurednews = $decoded->featurednews;

        //latest news
        // $service_url = $this->config->application->ApiURL. '/news/frontend/latest';

        // $curl = curl_init($service_url);
        // curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        // $curl_response = curl_exec($curl);
        // if ($curl_response === false) {
        //     $info = curl_getinfo($curl);
        //     curl_close($curl);
        //     die('error occured during curl exec. Additioanl info: ' . var_export($info));
        // }
        // curl_close($curl);
        // $decoded = json_decode($curl_response);
        $this->view->latestnews = $decoded->latestnews;

        $this->view->founderswisdom = $decoded->founderwisdom;

    }

    public function viewAction($newsslugs)
    {
        $this->view->leftsidebarname = "";
        $this->view->logoimage = $this->curl('/settings/managesettings');
        $this->view->script_google = $this->curl('/settings/script');

        $service_url_news = $this->config->application->ApiURL. '/fe/bnbbuzz/view/'.$newsslugs;
        $curl = curl_init($service_url_news);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->application->curlRest);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false)
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);

        $this->view->view = $decoded;

        if($decoded->fullnews != null) {
          $this->view->newscategory = $decoded->newscategory;
          $this->view->var404 = $decoded->fullnews;

            $this->view->newsid = $decoded->fullnews->newsid;
            $this->view->title = $decoded->fullnews->title;
            $this->view->newsslugs = $decoded->fullnews->newsslugs;
            $this->view->author = ucwords(strtolower($decoded->fullnews->author));
            $this->view->body = $decoded->fullnews->body;
            $this->view->banner = $decoded->fullnews->banner;
            $this->view->category = $decoded->fullnews->category;
            $this->view->date = $decoded->fullnews->date;
            $this->view->categoryname = $decoded->fullnews->categoryname;
            $this->view->categoryslugs = $decoded->fullnews->categoryslugs;
            //SEO
            $this->view->metatitle = $decoded->fullnews->title;
            $this->view->metatags = $decoded->fullnews->metatags;
            $this->view->metadesc = $decoded->fullnews->description;

            $this->view->latestnews = $decoded->latestnews;
            $this->view->sidebarmostviewednews = $decoded->mostviewed;
            $this->view->sidebarlatestnews = $decoded->sidebarlatestnews;
        } else {
          $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
          $this->view->pick("index/route404");
        }

        //VIEWS COUNTER
        $ch = curl_init($this->config->application->ApiURL. "/news/updateviews/". $newsslugs);
        curl_setopt($ch, CURLOPT_CAINFO, $this->config->application->curlRest);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        $response = curl_exec($ch);
        curl_close($ch);
        if(!$response) {
            return false;
        }
        // else{
        //
        // }
    }

    public function categoryAction($categoryslugs, $offset)
    {
        $this->view->logoimage = $this->curl('/settings/managesettings');
        $this->view->script_google = $this->curl('/settings/script');
        $this->view->leftsidebarname = "";

        $gotoroute = $this->config->application->ApiURL. '/fe/bnbbuzz/category/'.$categoryslugs.'/'.$offset;
        $curl = curl_init($gotoroute);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->application->curlRest);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false)
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);

        $this->view->showcategoryname = $decoded->newscategory->categoryname;
        $this->view->showcategoryslugs = $decoded->newscategory->categoryslugs;

        $this->view->newscategory = $decoded->categorylist;

        $this->view->newsbycategory = $decoded->listnewsbycategory;

        $this->view->sidebarmostviewednews = $decoded->mostviewed;

        $this->view->sidebarlatestnews = $decoded->sidebarlatestnews;

        $this->view->metatitle = $decoded->newscategory->categoryname;

        $itemperpage = 10;
        $this->view->page = $offset;
        $this->view->totalpage = ceil($decoded->totalpage / $itemperpage);
    }

    public function previewAction()
    {

        $this->view->leftsidebarname = "";
        $this->view->logoimage = $this->curl('/settings/managesettings');
        $this->view->script_google = '';
        // var_dump($_GET);

        // $mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
        // $dates = explode(" ", $_GET['date']);
        // $d = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];

        // $this->view->ptitle = $_GET['title'];
        // $this->view->pdate = $d;
        // $this->view->pcategoryname = $_GET['category']['categoryname'];
        // $this->view->pbanner = $_GET['banner'];
        // $this->view->pbody = $_GET['body'];
        // $this->view->pauthor = $_GET['author'];



        // $postdata = file_get_contents("http://bnbsite/bnb-buzz/preview");
        // $request = json_decode($postdata);
        // var_dump($request);
       // echo $news;


        $service_url_news = $this->config->application->ApiURL. '/fe/bnbbuzz/view/xd';
        $curl = curl_init($service_url_news);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->application->curlRest);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false)
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);

        $this->view->newscategory = $decoded->newscategory;

        $this->view->var404 = $decoded->fullnews;

        if($decoded->fullnews != null) {
            $this->view->newsid = $decoded->fullnews->newsid;
            $this->view->title = $decoded->fullnews->title;
            $this->view->newsslugs = $decoded->fullnews->newsslugs;
            $this->view->author = ucwords(strtolower($decoded->fullnews->author));
            $this->view->body = $decoded->fullnews->body;
            $this->view->banner = $decoded->fullnews->banner;
            $this->view->category = $decoded->fullnews->category;
            $this->view->date = $decoded->fullnews->date;
            $this->view->categoryname = $decoded->fullnews->categoryname;
            $this->view->categoryslugs = $decoded->fullnews->categoryslugs;
            //SEO
            $this->view->metatitle = $decoded->fullnews->metatitle;
            $this->view->metatags = $decoded->fullnews->metatags;
            $this->view->metadesc = $decoded->fullnews->description;
        }
        else{

        }

        $this->view->latestnews = $decoded->latestnews;

        $this->view->sidebarmostviewednews = $decoded->mostviewed;

        $this->view->sidebarlatestnews = $decoded->sidebarlatestnews;

        $this->view->view = $decoded;



    }

}
