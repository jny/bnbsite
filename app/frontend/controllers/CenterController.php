<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;

class CenterController extends ControllerBase {

	public function pageAction($centerslugs) {
    		$service_url = $this->config->application->ApiURL.'/fe/center/page/' . $centerslugs;
            $curl = curl_init($service_url);
            curl_setopt($curl, CURLOPT_CAINFO, $this->config->application->curlRest);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $curl_response = curl_exec($curl);
            if ($curl_response === false) {
                $info = curl_getinfo($curl);
                curl_close($curl);
                die('error occured during curl exec. Additioanl info: ' . var_export($info));
            }
            curl_close($curl);
            $decoded = json_decode($curl_response);
            if($decoded->centerprop != null) {
                $this->view->centerpage = $decoded;
                $this->view->schedules = $decoded->schedule;
                $this->view->regfee = $decoded->regfee;
                $this->view->regclass = $decoded->regclass;
                $this->view->reg1on1intro = $decoded->reg1on1intro;
                $this->view->centermembership = $decoded->centermembership;
                $this->view->classpackage = $decoded->classpackage;
                $this->view->center = $decoded->centerprop;
                $this->view->eventsdata = $decoded->eventslist;
                $this->view->getcenterslugs = $centerslugs;
                $this->view->email = $decoded->emaildata->email;
                $this->view->pagestatus = "active";
                $this->view->pagetitle = $decoded->centerprop->centertitle;
                $this->view->fbcapc = 'fuck';
                  //SEO
                $this->view->metatitle = $decoded->centerprop->centertitle . ", ".$decoded->centerprop->centerstate.": Body & Brain Yoga, Tai Chi, Meditation";
                $this->view->metatags = $decoded->centerprop->metatitle;
                $this->view->metadesc = $decoded->centerprop->metadesc;
            } else {
                $this->view->pagestatus = null;
            }
            $this->view->logoimage = $this->curl('/settings/managesettings');
            $this->view->script_google = $this->curl('/settings/script');
            $this->view->leftsidebarname = "";
		}

    public function newsAction(){
        $offset = $this->dispatcher->getParam("no");
        $centerslugs = $this->dispatcher->getParam("centerslugs");
        $this->view->pageSlugs = $centerslugs;
        $this->view->logoimage = $this->curl('/settings/managesettings');
        $this->view->script_google = $this->curl('/settings/script');

        //list news by Category
        $gotoroute = $this->config->application->ApiURL. '/news/frontend/listnewsbycenter/'. $centerslugs . '/' . $offset ;
        $curl = curl_init($gotoroute);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->application->curlRest);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curl_response = curl_exec($curl);

        if ($curl_response === false)
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->center = $decoded->centerprop;
        $this->view->newsbycenter = $decoded->newslist;
        $this->view->success_stories = $decoded->success;
        $this->view->popular_features = $decoded->mainnews;
        $itemperpage = 10;
        $this->view->page = $offset;
        $totalpage = ceil($decoded->totalnews / $itemperpage);
        $this->view->totalpage = $totalpage;
        $this->view->getcenterslugs = $centerslugs;
        $this->view->leftsidebarname = "";

        $this->view->metatitle = $decoded->centerprop->centertitle . " Body & Brain: News, Announcement";
        $this->view->metatags = "Body & Brain ".$decoded->centerprop->centertitle." Blog &";
        $this->view->metadesc = "Blog and News from the Body & Brain [".$decoded->centerprop->centertitle."] Center";
    }

    public function viewstoryAction() {
        $this->view->leftsidebarname = "";
        $centerslugs = $this->dispatcher->getParam("centerslugs");
        $storyslugs = $this->dispatcher->getParam("storyslugs");
        $ssid = $this->dispatcher->getParam("ssid");
        $service_url = $this->config->application->ApiURL.'/fe/center/successstories/'.$centerslugs.'/'.$storyslugs.'/'.$ssid;
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->application->curlRest);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);

        $this->view->story = $decoded->storyprop;
        $this->view->relatedstory = $decoded->relatedstory;
        $this->view->center = $decoded->centerprop;
        $this->view->success_stories = $decoded->success;
        $this->view->popular_features = $decoded->mainnews;
        $this->view->logoimage = $this->curl('/settings/managesettings');
        $this->view->script_google = $this->curl('/settings/script');
        // $slugs = preg_replace("![^a-z0-9]+!i", "-", $success->subject)

        $this->view->metatitle = $decoded->storyprop[0]->subject;
        $this->view->metadesc = $decoded->storyprop[0]->metadesc;
    }

    public function infoAction() {
        $centerslugs = $this->dispatcher->getParam("centerslugs");
        $service_url = $this->config->application->ApiURL.'/fe/center/centerinfo/'.$centerslugs;
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->application->curlRest);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->center = $decoded->centerprop;
        $this->view->timezone = $decoded->timezone;
        $this->view->centerphonenumber = $decoded->centerphonenumber->phonenumber;
        $this->view->centerhours = $decoded->centerhours;
        $this->view->popular_features = $decoded->mainnews;
        $this->view->getcenterslugs = $centerslugs;
        $this->view->leftsidebarname = "";
        $this->view->logoimage = $this->curl('/settings/managesettings');
        $this->view->script_google = $this->curl('/settings/script');
        $this->view->metatitle = $decoded->centerprop->centertitle . " Body & Brain: Address, Phone #, Open Hours";
    }

     public function newsviewAction() {
        $this->view->leftsidebarname = "";
         $centerslugs = $this->dispatcher->getParam("centerslugs");
         $newsslugs = $this->dispatcher->getParam("newsslugs");
         $this->view->logoimage = $this->curl('/settings/managesettings');
        $this->view->script_google = $this->curl('/settings/script');
        //VIEW FULL NEWS
        $service_url = $this->config->application->ApiURL. '/news/frontend/fullnewsbycenter/'. $centerslugs .'/'. $newsslugs;
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->application->curlRest);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        // var_dump($decoded);
        // die();
        $this->view->center = $decoded->centerprop;
        $this->view->success_stories = $decoded->success;
        $this->view->popular_features = $decoded->mainnews;
        // $this->view->var404 = $decoded;
        if($decoded->fullnews != false) {
            $this->view->newsid = $decoded->fullnews->newsid;
            $this->view->title = $decoded->fullnews->title;
            $this->view->newsslugs = $decoded->fullnews->newsslugs;
            $this->view->author = $decoded->fullnews->author;
            $this->view->body = $decoded->fullnews->body;
            $this->view->banner = $decoded->fullnews->banner;
            $this->view->date = $decoded->fullnews->date;
            $this->view->metatitle = $decoded->fullnews->centernewsmetatitle;
            $this->view->metatags = $decoded->fullnews->title;
            $this->view->metadesc = "Dahn Yoga [".$decoded->centerprop->centertitle."] Center blog post: [".$decoded->fullnews->title."]";
        }
    }


    public function classscheduleAction() {
        $centerslugs = $this->dispatcher->getParam("centerslugs");
        $this->view->logoimage = $this->curl('/settings/managesettings');
        $this->view->script_google = $this->curl('/settings/script');
        $service_url = $this->config->application->ApiURL.'/fe/center/classschedule/'.$centerslugs;
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->application->curlRest);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->center = $decoded->centerprop;
        $this->view->schedules = $decoded->schedule;
        $this->view->getcenterslugs = $centerslugs;
        $this->view->leftsidebarname = "";
        $this->view->success_stories = $decoded->success;
        $this->view->popular_features = $decoded->mainnews;
        $this->view->metatitle = $decoded->centerprop->centertitle . " Body & Brain: Class Schedule";
    }


    public function successstoriesAction(){
        $offset = $this->dispatcher->getParam("no");
        $this->view->logoimage = $this->curl('/settings/managesettings');
        $this->view->script_google = $this->curl('/settings/script');
        $centerslugs = $this->dispatcher->getParam("centerslugs");
        //list news by Category
        $gotoroute = $this->config->application->ApiURL. '/news/frontend/listsuccesssotriesbycenter/'. $centerslugs . '/' . $offset ;
        $curl = curl_init($gotoroute);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->application->curlRest);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curl_response = curl_exec($curl);

        if ($curl_response === false)
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->center = $decoded->centerprop;
        $this->view->storydata = $decoded->storylist;
            $image = array();
            foreach($decoded->storylist as $story) {
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,'https://bodynbrain.s3.amazonaws.com/uploads/testimonialimages/thumbnailTM_940.jpg');
                // don't download content
                curl_setopt($ch, CURLOPT_NOBODY, 1);
                curl_setopt($ch, CURLOPT_FAILONERROR, 1);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

                if(curl_exec($ch)!==FALSE) {
                    $image[] = 'exist';
                } else {
                    $image[] = 'haha';
                }
            }
        $this->view->image = $image;
        var_dump($image);
        $this->view->success_stories = $decoded->success;
        $this->view->popular_features = $decoded->mainnews;
        $itemperpage = 10;
        $this->view->page = $offset;
        $totalpage = ceil($decoded->totalstory / $itemperpage);
        $this->view->totalpage = $totalpage;
        $this->view->getcenterslugs = $centerslugs;
        $this->view->leftsidebarname = "";

        $this->view->metatitle = $decoded->centerprop->centertitle . " Body & Brain: Success Stories";
    }

    public function calendarAction(){
        $this->view->logoimage = $this->curl('/settings/managesettings');
        $this->view->script_google = $this->curl('/settings/script');
        $offset = $this->dispatcher->getParam("no");
        $centerslugs = $this->dispatcher->getParam("centerslugs");
        //list news by Category

        $gotoroute = $this->config->application->ApiURL. '/news/frontend/listeventsbycenter/'. $centerslugs . '/' . $offset ;
        $curl = curl_init($gotoroute);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->application->curlRest);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false)
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->center = $decoded->centerprop;
        $this->view->eventsdata = $decoded->eventslist;
        $this->view->popular_features = $decoded->mainnews;
        $itemperpage = 10;
        $this->view->page = $offset;
				$this->view->metatitle = $decoded->centerprop->centertitle . " Body & Brain: Events, Workshop Calendar";

        $decoded = ceil($decoded->totalevents / $itemperpage);
        $this->view->totalpage = $decoded;
        $this->view->getcenterslugs = $centerslugs;
        $this->view->leftsidebarname = "";
    }
    public function pricingAction() {
        $this->view->logoimage = $this->curl('/settings/managesettings');
        $this->view->script_google = $this->curl('/settings/script');
        $centerslugs = $this->dispatcher->getParam("centerslugs");
        $service_url = $this->config->application->ApiURL.'/fe/center/pricing/'.$centerslugs;
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->application->curlRest);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->center = $decoded->centerprop;
        $this->view->popular_features = $decoded->mainnews;
        $this->view->regfee = $decoded->regfee;
        $this->view->regclass = $decoded->regclass;
        $this->view->reg1on1intro = $decoded->reg1on1intro;
        $this->view->centermembership = $decoded->centermembership;
        $this->view->classpackages = $decoded->classpackages;
        $this->view->privatesession = $decoded->privatesession;
        $this->view->getcenterslugs = $centerslugs;
        $this->view->leftsidebarname = "";
        $this->view->metatitle = $decoded->centerprop->centertitle . " Body & Brain: Membership Pricing";
    }

    public function viewofferAction(){
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function newspreviewAction() {
        $this->view->leftsidebarname = "";
         $centerslugs = $this->dispatcher->getParam("centerslugs");
         $newsslugs = $this->dispatcher->getParam("newsslugs");
         $this->view->logoimage = $this->curl('/settings/managesettings');
        $this->view->script_google = $this->curl('/settings/script');
        //VIEW FULL NEWS
        $service_url = $this->config->application->ApiURL. '/news/frontend/fullnewsbycenter/sample/sample';
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->application->curlRest);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->center = $decoded->centerprop;
        $this->view->success_stories = $decoded->success;
        $this->view->popular_features = $decoded->mainnews;
        // $this->view->var404 = $decoded;
        if($decoded->fullnews != false) {
            $this->view->newsid = $decoded->fullnews->newsid;
            $this->view->title = $decoded->fullnews->title;
            $this->view->newsslugs = $decoded->fullnews->newsslugs;
            $this->view->author = $decoded->fullnews->author;
            $this->view->body = $decoded->fullnews->body;
            $this->view->banner = $decoded->fullnews->banner;
            $this->view->date = $decoded->fullnews->date;

            $this->view->metatitle = $decoded->fullnews->title;
            $this->view->metatags = $decoded->fullnews->title;
            $this->view->metadesc = "Dahn Yoga [".$decoded->centerprop->centertitle."] Center blog post: [".$decoded->fullnews->title."]";
        }
    }










}
