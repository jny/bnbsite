<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;


class ContactusController extends ControllerBase {

    public function indexAction() {
         $this->view->leftsidebarname = "";
         $this->view->logoimage = $this->curl('/settings/managesettings');
        $this->view->script_google = $this->curl('/settings/script');
        $this->view->metatitle = "Contact Us | Get in Touch | Body & Brain yoga";
        $this->view->metatags = "Contact Us";
        $this->view->metadesc = "If you have question, comments or suggestions, please contact us.  We will get back to you as soon as possible.";
    } 
}

?>