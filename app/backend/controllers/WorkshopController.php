<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;

class WorkshopController extends ControllerBase {
	public function indexAction() {
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
	}
	public function createworkshopAction() {
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
	}
	public function editvenueAction() {
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
	}
	public function editworkshopAction() {
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
	}
}