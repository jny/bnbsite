<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;

class ManagecontactsController extends ControllerBase {
	public function managecontactsAction() {
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
	}
}