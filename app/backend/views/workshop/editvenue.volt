<script type="text/ng-template" id="success.html">
  <div ng-include="'/be/tpl/success.html'"></div>
</script>

<!-- <div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3"></h1>
  <a id="top"></a>
</div>
 -->
<div class="container-fluid">
	<div class="row wrapper-md">
			<div class="panel panel-warning">
				<div class="panel-heading"><h4>Edit Workshop Venue</h4></div>
				<div class="panel-body">

<form name="editvenueform" class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="updatevenue(upvenue)">
	<div class="col-sm-6">
		<div class="form-group">
			<label class="col-md-3 control-label bolded"><b>Venue Name</b></label>
			<div class="col-md-9">
				<input type="text" name="venuename" class="form-control" placeholder="Venue Name" ng-model="upvenue.venuename" ng-change="checkvenuenameWException(upvenue.venuename)" required>
				<div class="popOver" ng-show="validvenue" >
					Venue name is already taken.
					<span class="pop-triangle"></span>
				</div>
				<span class="help-block m-b-none">Example:Sedona Mago Retreat, Honor's Haven</span>
			</div>
		</div>

		<div class="line line-lg"></div>

		<div class="form-group">
			<label class="col-md-3 control-label"><b>State</b></label>
			<div class="col-md-9">
				<div ui-module="select2">
				<select ui-select2 class="form-control"  ng-model="upvenue.SPR"  ng-change="statechange(upvenue.SPR)" required="required">
					<option ng-repeat="list in centerstate" ng-value="list.state_code">{[{list.state}]}</option>

				</select>
			</div>
			</div>
		</div>

		<div class="line line-lg"></div>

		<div class="form-group">
			<label class="col-md-3 control-label"><b>City</b></label>
			<div class="col-md-9">
				<div ui-module="select2">
					<select ui-select2 class="form-control" ng-model="upvenue.city" ng-change="citychange(upvenue.city)" required="required">
						<option ng-repeat="list in centercity" ng-value="list.city">{[{list.city}]}</option>
					</select>
				</div>
			</div>
		</div>

		<div class="line line-lg"></div>

		<div class="form-group">
			<label class="col-md-3 control-label"><b>Zip Code</b></label>
			<div class="col-md-9">
				<div ui-module="select2">
					<select ui-select2 class="form-control" ng-model="upvenue.zipcode"  required="required">
						<option ng-repeat="list in getcenterzip" ng-value="list.zip">{[{list.zip}]}</option>
					</select>
				</div>
			</div>
		</div>

		<div class="line line-lg"></div>

		<div class="form-group">
			<label class="col-md-3 control-label"><b>Country</b></label>
			<div class="col-md-9">
				<input type="text" class="form-control" ng-model="upvenue.country" required>
			</div>
		</div>

		<div class="line line-lg"></div>

		<div class="form-group">
			<label class="col-md-3 control-label"><b>Address 1</b></label>
			<div class="col-md-9">
				<input type="text" class="form-control" placeholder="Address 1" ng-model="upvenue.address1" required>
				<span class="help-block m-b-none">Street address, P.O. box, company name, c/o </span>
			</div>
		</div>

		<div class="line line-lg"></div>

		<div class="form-group">
			<label class="col-md-3 control-label"><b>Address 2</b></label>
			<div class="col-md-9">
				<input type="text" class="form-control" placeholder="Apartment, suite, unit, building, floor, etc." ng-model="upvenue.address2">
			</div>
		</div>

		
	</div>

	<div class="col-sm-6">
		<div class="form-group">
			<label class="col-md-3 control-label"><b>Website</b></label>
			<div class="col-md-9">
				<input type="url" class="form-control" ng-model="upvenue.website" placeholder="http://" required>
			</div>
		</div>

		<div class="line line-lg"></div>

		<div class="form-group">
			<label class="col-md-3 control-label"><b>Latitude</b></label>
			<div class="col-md-9">
				<input type="text" class="form-control" ng-model="upvenue.latitude" required>
			</div>
		</div>

		<div class="line line-lg"></div>

		<div class="form-group">
			<label class="col-md-3 control-label"><b>Longtitude</b></label>
			<div class="col-md-9">
				<input type="text" class="form-control" ng-model="upvenue.longtitude" required>
			</div>
		</div>

		<div class="line line-lg"></div>

		<div class="form-group">
			<label class="col-md-3 control-label"><b>Phone Number</b></label>
			<div class="col-md-9">
				<input type="text" class="form-control phone_us" placeholder="e.g. 928-284-5046" ng-model="upvenue.contact" minlength=14 maxlength=14 required>         
				<span class="help-block m-b-none"> (Admin should enter phone number including dashes, country code, area code, etc.)</span>
			</div>
		</div>

		<div class="line line-lg"></div>

		<div class="form-group">
			<label class="col-md-3 control-label">Authorizeid</label>
			<div class="col-md-9">
				<input type="text" class="form-control venue"  ng-model="upvenue.vauthorizeid" required>
			</div>
		</div>

		<div class="line line-lg"></div>

		<div class="form-group">
			<label class="col-md-3 control-label">Authorizekey</label>
			<div class="col-md-9">
				<input type="text" class="form-control venue" ng-model="upvenue.vauthorizekey" required>
			</div>
		</div>

		<div class="line line-lg"></div>

		<div class="form-group">
			<span class="pull-right">
				<a ui-sref="manageworkshop" class="btn btn-md btn-danger">Close</a>
				<button class="btn btn-md btn-success" ng-click="isCollapsed = isCollapsed" ng-disabled="venueform.$invalid || validvenue">Save</button>
			</span>
		</div>
	</div>
</form>

				</div>
			</div>
	</div>
</div>