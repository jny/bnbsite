<?php $role = $username['pi_userrole']; 
$userid = $username['bnb_userid'];
$fullname = $username['pi_fullname'];
if($role == 'Center Manager') { $centerid = $username['bnb_centerid']; }
?>
<!DOCTYPE html>
<html lang="en" data-ng-app="app" ng-cloak>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="user-scalable = yes" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta charset="utf-8">
  <!-- Title and other stuffs -->
  {{ get_title() }}
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">
  <!-- Stylesheets -->
  {{ stylesheet_link('be/css/bootstrap.css') }}
  {{ stylesheet_link('be/css/animate.css') }}
  {{ stylesheet_link('be/css/font-awesome.min.css') }}
  {{ stylesheet_link('be/css/simple-line-icons.css') }}
  {{ stylesheet_link('be/css/font.css') }}
  {{ stylesheet_link('be/css/app.css') }}
  {{ stylesheet_link('be/css/custom.css') }}
  {{ stylesheet_link('be/js/angularjs-toaster/toaster.css') }}
  {{ stylesheet_link('be/css/wobblebar.css') }}

  {{ stylesheet_link('vendors/angular-xeditable/dist/css/xeditable.css') }}
  {{ stylesheet_link('vendors/ngImgCrop/compile/unminified/ng-img-crop.css') }}
  {{ stylesheet_link('vendors/angular-xeditable/dist/css/xeditable.css') }}
  {{ stylesheet_link('be/js/jquery/chosen/chosen.css') }}
  
  <!-- HTML5 Support for IE --> 
  <!--[if lt IE 9]>
  <script src="be/js/html5shim.js"></script>
  <![endif]-->

  <link rel="shortcut icon" href="/img/frontend/favicon.gif" type='image/x-icon'/>
  <script>
    var userid = "<?php echo $username['bnb_userid']; ?>";
  </script>
</head>
<body ng-controller="AppCtrl">
<div>asdasd{[{sampledata}]}</div>
{{ content() }}
<toaster-container toaster-options="{'time-out': 6000, 'close-button':true, 'position-class': 'toast-bottom-left'}"></toaster-container>
<div class="app" id="app" ng-class="{'app-header-fixed':app.settings.headerFixed, 'app-aside-fixed':app.settings.asideFixed, 'app-aside-folded':app.settings.asideFolded}">
    <div class="app-header navbar">
      <!-- navbar header -->
      <div class="navbar-header bg-white">
        <button class="pull-right visible-xs dk" ui-toggle-class="show" data-target=".navbar-collapse">
          <i class="glyphicon glyphicon-cog"></i>
        </button>
        <button class="pull-right visible-xs" ui-toggle-class="off-screen" data-target=".app-aside" ui-scroll="app">
          <i class="glyphicon glyphicon-align-justify"></i>
        </button>
        <!-- brand -->
        <a href="#/" class="navbar-brand text-lt">
          <img src="/img/bnblogomenu.gif" alt=".">
          <span class="hidden-folded m-l-xs">{[{app.name}]}</span>
        </a>
        <!-- / brand -->
      </div>
      <!-- / navbar header -->

      <!-- navbar collapse -->
      <div class="collapse navbar-collapse box-shadow {[{app.settings.navbarCollapseColor}]}">
        <!-- buttons -->
        <div class="nav navbar-nav m-l-sm hidden-xs">
          <a href class="btn no-shadow navbar-btn" ng-click="app.settings.asideFolded = !app.settings.asideFolded">
            <i class="fa {[{app.settings.asideFolded ? 'fa-indent' : 'fa-dedent'}]} fa-fw"></i>
          </a>
          <a href class="btn no-shadow navbar-btn" ui-toggle-class="show" target="#aside-user">
            <i class="icon-user fa-fw"></i>
          </a>
        </div>
        <!-- / buttons -->

        <!-- link and dropdown -->

    <?php if ($role == 'Administrator') { ?>
        <ul class="nav navbar-nav hidden-sm">
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-fw fa-plus visible-xs-inline-block"></i>
              <span translate="header.navbar.new.NEW">New</span> <span class="caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
              <li>
                <a ui-sref="createcenter">
                  <span>Center</span>
                </a>
              </li>
              <li>
                <a ui-sref="createnews">
                  <span translate="header.navbar.new.NEWS">News</span>
                </a>
              </li>
              <li>
                <a ui-sref="createpage">
                  <span translate="header.navbar.new.PAGE">Page</span>
                </a>
              </li>
              <li>
                <a ui-sref="userscreate">
                  <span translate="header.navbar.new.USER">User</span>
                </a>
              </li>
            </ul>
          </li>
        </ul>
  <?php } ?>
        <!-- / link and dropdown -->

        <!-- search form -->
        <form class="navbar-form navbar-form-sm navbar-left shift" ui-shift="prependTo" target=".navbar-collapse" role="search" ng-controller="TypeaheadDemoCtrl">
          <div class="form-group">
            <div class="input-group">
              <input type="text" ng-model="selected" typeahead="state for state in states | filter:$viewValue | limitTo:8" class="form-control input-sm bg-light no-border rounded padder" placeholder="Search projects...">
              <span class="input-group-btn">
                <button type="submit" class="btn btn-sm bg-light rounded"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </div>
        </form>
        <!-- / search form -->

        <!-- nabar right -->
       
        <ul class="nav navbar-nav navbar-right" ng-controller="notificationCtrl" ng-init="userid('<?php echo $username["bnb_userid"];?>')">

          <li class="hidden-xs">
            <a ui-fullscreen></a>
          </li>
          <li class="dropdown">
            <a href class="dropdown-toggle" ng-click="removeinfi()">
              <i class="icon-bell fa-fw"></i>
              <span class="visible-xs-inline">Notifications</span>
              <span class="badge badge-sm up bg-danger pull-right-xs nifiicon bounce animated1 infinite" ng-show="nofiticationlistcount != 0" ng-bind="nofiticationlistcount"></span>
            </a>
            <!-- dropdown -->
            <div class="dropdown-menu w-xl animated fadeInUp">
              <div class="panel bg-white">
                <div class="panel-heading b-light bg-light">
                  <!-- <strong ng-if="nofiticationlistcount != 0" >You have <span>{[{nofiticationlistcount}]}</span> unread notifications</strong> -->
                  <strong>Notifications</strong>
                </div>
                <div id="infinitescrolldiv" class="list-group" ui-jq="slimScroll" ui-options="{height:'410px', size:'8px'}">
                  <div ng-repeat="list in nofiticationlist">
                    <a ng-if="list.notificationstatus == 0" href class="media list-group-item bg-light" ng-click="viewnotification(list.notificationid,list.itemid,list.type,list.centerid,list[0].uigo)">
                      <!-- <i class="fa fa-circle text-warning pull-right text-xs m-t-sm"></i> -->
                      <i class="fa fa-circle pull-right text-warning"></i>
                      <span class="pull-left thumb-sm" ng-if="list.type == 'introsession'">
                        <!-- <img src="/img/a0.jpg" alt="..." class="img-circle"> -->
                        <i class="icon-user-follow font30"></i>
                      </span>
                      <span class="media-body block m-b-none" ng-if="list.type == 'introsession'">
                        {[{list.name}]}<br>
                        Enrolled to 1-on-1 Intro Session<br>
                        <i class="fa fa-clock-o"></i> <small class="text-muted">{[{list[0].timeago}]}</small>
                      </span>
                      <!-- end of introsession -->

                      <span class="pull-left thumb-sm" ng-if="list.type == 'groupsession'">
                        <!-- <img src="/img/a0.jpg" alt="..." class="img-circle"> -->
                        <i class="icon-users font30"></i>
                      </span>
                      <span class="media-body block m-b-none" ng-if="list.type == 'groupsession'">
                        {[{list.name}]}<br>
                        Enrolled to 1 Group Class + 1-on-1 Intro Session<br>
                        <i class="fa fa-clock-o"></i> <small class="text-muted">{[{list[0].timeago}]}</small>
                      </span>
                      <!-- end of groupsession -->

                      <span class="pull-left thumb-sm" ng-if="list.type == 'beneplace'">
                        <!-- <img src="/img/a0.jpg" alt="..." class="img-circle"> -->
                        <i class="icon-paper-plane font30"></i>
                      </span>
                      <span class="media-body block m-b-none" ng-if="list.type == 'beneplace'">
                        {[{list.name}]}<br>
                        Enrolled to Beneplace<br>
                        <i class="fa fa-clock-o"></i> <small class="text-muted">{[{list[0].timeago}]}</small>
                      </span>
                      <!-- end of beneplace -->

                      <span class="pull-left thumb-sm" ng-if="list.type == 'message'">
                        <!-- <img src="/img/a0.jpg" alt="..." class="img-circle"> -->
                        <i class="icon-envelope font30"></i>
                      </span>
                      <span class="media-body block m-b-none" ng-if="list.type == 'message'">
                        {[{list.name}]}<br>
                        Sent you a new message<br>
                        <i class="fa fa-clock-o"></i> <small class="text-muted">{[{list[0].timeago}]}</small>
                      </span>
                      <!-- end of message -->

                      <span class="pull-left thumb-sm" ng-if="list.type == 'workshop'">
                        <!-- <img src="/img/a0.jpg" alt="..." class="img-circle"> -->
                        <i class="icon-users font30"></i>
                      </span>
                      <span class="media-body block m-b-none" ng-if="list.type == 'workshop'">
                        {[{list.name}]}<br>
                        Enrolled to Workshop<br>
                        <i class="fa fa-clock-o"></i> <small class="text-muted">{[{list[0].timeago}]}</small>
                      </span>
                      <!-- end of workshop -->

                      <span class="pull-left thumb-sm" ng-if="list.type == 'story'">
                        <!-- <img src="/img/a0.jpg" alt="..." class="img-circle"> -->
                        <i class="icon-bubbles font30"></i>
                      </span>
                      <span class="media-body block m-b-none" ng-if="list.type == 'story'">
                        {[{list.name}]}<br>
                        Sent you Testimonies<br>
                        <i class="fa fa-clock-o"></i> <small class="text-muted">{[{list[0].timeago}]}</small>
                      </span>
                      <!-- end of story -->
                    </a>

                    <a ng-if="list.notificationstatus == 1" href class="media list-group-item" ng-click="viewnotification(list.notificationid,list.itemid,list.type,list.centerid,list[0].uigo)">

                      <span class="pull-left thumb-sm" ng-if="list.type == 'introsession'">
                        <!-- <img src="/img/a0.jpg" alt="..." class="img-circle"> -->
                        <i class="icon-user-follow font30"></i>
                      </span>
                      <span class="media-body block m-b-none" ng-if="list.type == 'introsession'">
                        {[{list.name}]}<br>
                        Enrolled to 1-on-1 Intro Session<br>
                        <i class="fa fa-clock-o"></i> <small class="text-muted">{[{list[0].timeago}]}</small>
                      </span>
                      <!-- end of introsession -->

                      <span class="pull-left thumb-sm" ng-if="list.type == 'groupsession'">
                        <!-- <img src="/img/a0.jpg" alt="..." class="img-circle"> -->
                        <i class="icon-users font30"></i>
                      </span>
                      <span class="media-body block m-b-none" ng-if="list.type == 'groupsession'">
                        {[{list.name}]}<br>
                        Enrolled to 1 Group Class + 1-on-1 Intro Session<br>
                        <i class="fa fa-clock-o"></i> <small class="text-muted">{[{list[0].timeago}]}</small>
                      </span>
                      <!-- end of groupsession -->

                      <span class="pull-left thumb-sm" ng-if="list.type == 'beneplace'">
                        <!-- <img src="/img/a0.jpg" alt="..." class="img-circle"> -->
                        <i class="icon-paper-plane font30"></i>
                      </span>
                      <span class="media-body block m-b-none" ng-if="list.type == 'beneplace'">
                        {[{list.name}]}<br>
                        Enrolled to Beneplace<br>
                        <i class="fa fa-clock-o"></i> <small class="text-muted">{[{list[0].timeago}]}</small>
                      </span>
                      <!-- end of beneplace -->

                      <span class="pull-left thumb-sm" ng-if="list.type == 'message'">
                        <!-- <img src="/img/a0.jpg" alt="..." class="img-circle"> -->
                        <i class="icon-envelope font30"></i>
                      </span>
                      <span class="media-body block m-b-none" ng-if="list.type == 'message'">
                        {[{list.name}]}<br>
                        Sent you a new message<br>
                        <i class="fa fa-clock-o"></i> <small class="text-muted">{[{list[0].timeago}]}</small>
                      </span>
                      <!-- end of message -->

                      <span class="pull-left thumb-sm" ng-if="list.type == 'workshop'">
                        <!-- <img src="/img/a0.jpg" alt="..." class="img-circle"> -->
                        <i class="icon-users font30"></i>
                      </span>
                      <span class="media-body block m-b-none" ng-if="list.type == 'workshop'">
                        {[{list.name}]}<br>
                        Enrolled to Workshop<br>
                        <i class="fa fa-clock-o"></i> <small class="text-muted">{[{list[0].timeago}]}</small>
                      </span>
                      <!-- end of workshop -->

                      <span class="pull-left thumb-sm" ng-if="list.type == 'story'">
                        <!-- <img src="/img/a0.jpg" alt="..." class="img-circle"> -->
                        <i class="icon-bubbles font30"></i>
                      </span>
                      <span class="media-body block m-b-none" ng-if="list.type == 'story'">
                        {[{list.name}]}<br>
                        Sent you Testimonies<br>
                        <i class="fa fa-clock-o"></i> <small class="text-muted">{[{list[0].timeago}]}</small>
                      </span>
                      <!-- end of story -->
                    </a>
                  </div>
                  <a ng-click="incrementLimit()" href class="media bg-light" style="height:20px;padding-top:1px;overflow:hidden;">
                  <center>
                    <span style="padding:5px;" ng-show="notiloadertext">loadmore...</span>
                    <span class="ouro ouro2" style="padding-top:1px;overflow:hidden;" ng-show="notiloader"> <!--  -->
                      <span class="left"><span class="anim"></span></span>
                      <span class="right"><span class="anim"></span></span>
                    </span>
                  </center>
                  </a>
                </div>
                <div class="panel-footer text-sm">
                  <a href  class="pull-right"><i class="fa fa-cog"></i></a>
                  <a href ui-sref="managenotification" data-toggle="class:show animated fadeInRight">See all the notifications</a>
                </div>
              </div>
            </div>
            <!-- / dropdown -->
          </li>
          <li class="dropdown">
            <a href class="dropdown-toggle clear" data-toggle="dropdown">
              <span class="thumb-sm avatar m-t-n-sm m-b-n-sm m-l-sm">
                <img src="https://bodynbrain.s3.amazonaws.com/uploads/userimages/<?php echo $username['pi_profile_pic_name']; ?>" alt="...">
              </span>
              <span class="hidden-sm hidden-md"><?php echo $username['pi_username']; ?></span> <b class="caret"></b>
            </a>
            <!-- dropdown -->
            <ul class="dropdown-menu animated fadeInRight w">
              <li class="wrapper b-b m-b-sm bg-light m-t-n-xs">
                <div>
                  <p>300mb of 500mb used</p>
                </div>
                <progressbar value="60" class="progress-xs m-b-none bg-white"></progressbar>
              </li>
              <li>
                <a href>
                  <span class="badge bg-danger pull-right">30%</span>
                  <span>Settings</span>
                </a>
              </li>
              <li>
                <a ui-sref="editprofile">Profile</a>
              </li>
              <li>
                <a ui-sref="changepassword({userid: '<?php echo $username["bnb_userid"];?>'})">
                  <span class="label bg-info pull-right"></span>
                  Change Password
                </a>
              </li>
              <li>
                <a ui-sref="app.docs">
                  <span class="label bg-info pull-right">new</span>
                  Help
                </a>
              </li>
              <li class="divider"></li>
              <li>
                <a href="/bnbadmin/admin/logout">Logout <?php echo $role; ?></a>
              </li>
            </ul>
            <!-- / dropdown -->
          </li>

        </ul>
        <!-- / navbar right -->



      </div>
      <!-- / navbar collapse -->
      </div>

  <!-- menu -->
  <div class="app-aside hidden-xs {[{app.settings.asideColor}]}">
<div class="aside-wrap">
  <div class="navi-wrap">
    <!-- user -->
    <div class="clearfix hidden-xs text-center hide" id="aside-user">
      <div class="dropdown wrapper">
        <a ui-sref="editprofile">
          <span class="thumb-lg w-auto-folded avatar m-t-sm">
            <img src="https://bodynbrain.s3.amazonaws.com/uploads/userimages/<?php echo $username['pi_profile_pic_name']; ?>" alt="..." class="img-full">
          </span>
        </a>
        <a href class="dropdown-toggle hidden-folded">
          <span class="clear">
            <span class="block m-t-sm">
              <strong class="font-bold text-lt"><?php echo $fullname; ?></strong> 
              <b class="caret"></b>
            </span>
            <span class="text-muted text-xs block"><?php echo $role; ?></span>
          </span>
        </a>
        <!-- dropdown -->
        <ul class="dropdown-menu animated fadeInRight w hidden-folded">
          <li class="wrapper b-b m-b-sm bg-info m-t-n-xs">
            <span class="arrow top hidden-folded arrow-info"></span>
            <div>
              <p>300mb of 500mb used</p>
            </div>
            <progressbar value="60" type="white" class="progress-xs m-b-none dker"></progressbar>
          </li>
          <li>
            <a href>Settings</a>
          </li>
          <li>
            <a ui-sref="editprofile">Profile</a>
          </li>
          <li>
            <a href>
              <span class="badge bg-danger pull-right">3</span>
              Notifications
            </a>
          </li>
          <li class="divider"></li>
          <li>
            <a ui-sref="logout"> Logout</a>
          </li>
        </ul>
        <!-- / dropdown -->
      </div>
      <div class="line dk hidden-folded"></div>
    </div>
    <!-- / user -->

    <!-- nav -->
    <nav ui-nav class="navi">
<!-- first -->
<ul class="nav">
  <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
    <span translate="aside.nav.HEADER">Navigation</span>
  </li>
  <li>
    <a ui-sref="dashboard">
      <i class="glyphicon glyphicon-stats icon text-primary-dker"></i>
      <span class="font-bold" translate="aside.nav.DASHBOARD">Dashboard</span>
    </a>
  </li>
  <?php if ($role == 'Administrator') { ?> 
  <li ng-class="{active:$state.includes('app.users')}">
    <a href class="auto">
      <span class="pull-right text-muted">
        <i class="fa fa-fw fa-angle-right text"></i>
        <i class="fa fa-fw fa-angle-down text-active"></i>
      </span>
      <i class="glyphicon glyphicon-user icon"></i>
      <span class="font-bold" translate="aside.nav.users.USERS">Users</span>
<?php } ?>
    </a>
    <ul class="nav nav-sub dk">
      <li ui-sref-active="active">
        <a ui-sref="userscreate">
          <span translate="aside.nav.users.CREATE_USER">Create User</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="userlist">
          <span translate="aside.nav.users.USER_LIST">User List</span>
        </a>
      </li>
    </ul>
  </li>
  <li ng-class="{active:$state.includes('app.center')}">
<?php if ($role == 'Administrator') { ?> 
    <a href class="auto">
      <span class="pull-right text-muted">
        <i class="fa fa-fw fa-angle-right text"></i>
        <i class="fa fa-fw fa-angle-down text-active"></i>
      </span>
      <i class="glyphicon glyphicon-inbox icon"></i>
      <span class='font-bold' translate='aside.nav.center.CENTER'>Center</span>  
    </a>
    <ul class="nav nav-sub dk">
      <li ui-sref-active="active">
        <a ui-sref="createcenter">
          <span translate="aside.nav.center.CREATE_CENTER">Create Center</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="managecenter({userid: '<?php echo $username["bnb_userid"];?>'})">
          <span translate="aside.nav.center.MANAGE_CENTER">Manage Center</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="regiondistrict">
          <span translate="aside.nav.center.REGIONDISTRICT">Region</span>
        </a>
      </li>
    </ul>
<?php } elseif ($role == 'Center Manager') { ?>
          <a href class="auto" ui-sref="centerview({centerid: '<?php echo $username["bnb_centerid"];?>',uifrom: 'none'})">
            <span class="pull-right text-muted">
              <i class="fa fa-fw fa-angle-down text-active"></i>
            </span>
            <i class="glyphicon glyphicon-inbox icon"></i>
            <span class='font-bold'>My Center</span>  
          </a>
<?php } elseif ($role == 'Region Manager' || $role == 'District Manager') { ?>
          <a href class="auto" ui-sref="managecenter({userid: '<?php echo $username["bnb_userid"];?>'})">
            <span class="pull-right text-muted">
              <i class="fa fa-fw fa-angle-down text-active"></i>
            </span>
            <i class="glyphicon glyphicon-inbox icon"></i>
            <span class='font-bold'>My Center</span>  
          </a>
<?php } ?>
  </li>
  <?php if ($role == 'Administrator') { ?> 
  <li ng-class="{active:$state.includes('app.pages')}">
    <a href class="auto">
      <span class="pull-right text-muted">
        <i class="fa fa-fw fa-angle-right text"></i>
        <i class="fa fa-fw fa-angle-down text-active"></i>
      </span>
      <i class="glyphicon glyphicon-th-large icon"></i>
      <span class="font-bold" translate="aside.nav.pages.PAGES">Pages</span>
    </a>
    <ul class="nav nav-sub dk">
      <li ui-sref-active="active">
        <a ui-sref="createpage">
          <span translate="aside.nav.pages.CREATE_PAGE">Create Page</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="managepage">
          <span translate="aside.nav.pages.MANAGE_PAGE">Manage Page</span>
        </a>
      </li>
    </ul>
  </li>
  <?php } ?>
  <?php if ($role == 'Administrator' || $role == 'Editor') { ?> 
  <li ng-class="{active:$state.includes('app.news')}">
    <a href class="auto">
      <span class="pull-right text-muted">
        <i class="fa fa-fw fa-angle-right text"></i>
        <i class="fa fa-fw fa-angle-down text-active"></i>
      </span>
      <i class="glyphicon glyphicon-edit icon"></i>
      <span class="font-bold" translate="aside.nav.news.NEWS">News</span>
    </a>
    <ul class="nav nav-sub dk">
      <li ui-sref-active="active">
        <a ui-sref="createnews">
          <span translate="aside.nav.news.CREATE_NEWS">Create News</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="managenews">
          <span translate="aside.nav.news.MANAGE_NEWS">Manage News</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="createcategory">
          <span translate="aside.nav.news.NEWS_CATEGORY">News Category</span>
        </a>
      </li>
    </ul>
  </li>
   <?php } ?>
  <?php if ($role == 'Administrator') { ?>
  <li>
    <a ui-sref="createworkshop({tab: '3'})">
      <i class="fa fa-group"></i>
      <span class="font-bold" translate="aside.nav.workshop.WORKSHOP">Workshop</span>
    </a>
  </li>
  <?php } ?>
  <?php if ($role == 'Administrator') { ?> 
  <li ui-sref-active="active">
    <a ui-sref="managecontacts">
       <i class="glyphicon  glyphicon-phone-alt icon"></i>
      <span class="font-bold" translate="aside.nav.contacts.MANAGE_CONTACTS">Manage Contacts</span>
    </a>
  </li>
  <li ng-class="{active:$state.includes('app.pages')}">
    <a href class="auto">
      <span class="pull-right text-muted">
        <i class="fa fa-fw fa-angle-right text"></i>
        <i class="fa fa-fw fa-angle-down text-active"></i>
      </span>
      <i class="glyphicon glyphicon-envelope icon"></i>
       <span class="font-bold" translate="aside.nav.newsletter.NEWSLETTER">NEWS LETTER</span>
    </a>
    <ul class="nav nav-sub dk">
      <li ui-sref-active="active">
        <a ui-sref="createnewsletter">
          <span>Create News Letter</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="managenewsletter">
          <span>Manage News Letter</span>
        </a>
      </li>
    </ul>
  </li>
  <li ui-sref-active="active">
    <a ui-sref="allintrosession({userid: '<?php echo $username["bnb_userid"];?>'})">
       <i class="icon-user-follow"></i>
      <span class="font-bold" translate="aside.1ON1">1ON1</span>
    </a>
  </li>
  <li ui-sref-active="active">
    <a ui-sref="allgroupclasssession({userid: '<?php echo $username["bnb_userid"];?>'})">
       <i class="icon-users"></i>
      <span class="font-bold" translate="aside.1GRP">1GRP</span>
    </a>
  </li>
  <li ui-sref-active="active">
    <a ui-sref="allbeneplace({userid: '<?php echo $username["bnb_userid"];?>'})">
       <i class="icon-paper-plane"></i>
      <span class="font-bold" translate="aside.BENEPLACE">BENEPLACE</span>
    </a>
  </li>
  <?php } ?>

  <?php if ($role == 'Administrator' || $role == 'Editor') { ?> 
      <li ui-sref-active="active">
        <a ui-sref="managestories">
         <i class="glyphicon glyphicon-bullhorn icon"></i>
          <span class="font-bold" translate="aside.nav.story.MANAGE_STORY">Manage Stories</span>
        </a>
      </li>
  <?php } ?>
  <li class="line dk"></li>

  <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
    <span translate="aside.nav.components.COMPONENTS">Components</span>
  </li>

  <?php if ($role == 'Administrator') { ?> 
  <li ng-class="{active:$state.includes('app.income')}">
    <a class="auto">      
      <span class="pull-right text-muted">
        <i class="fa fa-fw fa-angle-right text"></i>
        <i class="fa fa-fw fa-angle-down text-active"></i>
      </span>
      <i class="glyphicon  glyphicon-usd"></i>
      <span class="font-bold">Center Income</span>
    </a>
    <ul class="nav nav-sub dk">
      <li ui-sref-active="active">
        <a ui-sref="centerincome">
          <span translate="aside.nav.centerincome.VIEW">Vision</span>
        </a>
      </li>
    </ul>
    <ul class="nav nav-sub dk">
      <li ui-sref-active="active">
        <a ui-sref="centerincome_franchise">
          <span translate="aside.nav.centerincome.FRANCHISE">Franchise</span>
        </a>
      </li>
    </ul>
  </li>
  <?php } ?>

  <li ng-class="{active:$state.includes('app.center')}">
<?php if ($role == 'Administrator') { ?> 
    <a href class="auto">
      <span class="pull-right text-muted">
        <i class="fa fa-fw fa-angle-right text"></i>
        <i class="fa fa-fw fa-angle-down text-active"></i>
      </span>
      <i class="glyphicon glyphicon-inbox icon"></i>
      <span class='font-bold' translate='aside.nav.center.GPRICING'>Global Pricing</span>  
    </a>
    <ul class="nav nav-sub dk">
      <li ui-sref-active="active">
        <a ui-sref="globalpricing">
          <span translate="aside.nav.center.SPRICING">SPRICING</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="beneplacepricing">
          <span translate="aside.nav.center.BPRICING">BPRICING</span>
        </a>
      </li>
    </ul>
  </li>
<?php } ?>
  <li class="line dk hidden-folded"></li>

  <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">          
    <span translate="aside.nav.your_stuff.OTHERS">Your Stuff</span>
  </li>  
  <li>
    <a ui-sref="editprofile">
      <i class="icon-user icon text-success-lter"></i>
      <!-- <b class="badge bg-success dk pull-right">30%</b> -->
      <span translate="aside.nav.your_stuff.PROFILE">Profile</span>
    </a>
  </li>
<?php if ($role == 'Administrator') { ?> 
  <li>
    <a ui-sref="manageauditlog"> <!-- app.docs-->
      <i class="glyphicon glyphicon-tasks icon"></i>
      <span translate="aside.nav.your_stuff.LOGS">System Logs</span>
    </a>
  </li>
  <li>
    <a ui-sref="settings"> <!-- app.docs-->
      <i class="glyphicon glyphicon-cog icon"></i>
      <span translate="aside.nav.your_stuff.SETTINGS">Settings</span>
    </a>
  </li>

<?php } ?>
</ul>
<!-- / third -->

    </nav>
    <!-- nav -->

  </div>
</div>
  </div>
  <!-- / menu -->
  <!-- content -->
  <div class="app-content">
    <div ui-butterbar></div>
    <a href class="off-screen-toggle hide" ui-toggle-class="off-screen" data-target=".app-aside" ></a>
    <div class="app-content-body fade-in-up" ui-view></div>    

  </div>
  <!-- /content -->
  <!-- aside right -->
  <div class="app-aside-right pos-fix no-padder w-md w-auto-xs bg-white b-l animated fadeInRight hide">
    <div class="vbox">
      <div class="wrapper b-b b-light m-b">
        <a href class="pull-right text-muted text-md" ui-toggle-class="show" target=".app-aside-right"><i class="icon-close"></i></a>
        Chat
      </div>
      <div class="row-row">
        <div class="cell">
          <div class="cell-inner padder">
            <!-- chat list -->
            <div class="m-b">
              <a href class="pull-left thumb-xs avatar"><img src="/img/a2.jpg" alt="..."></a>
              <div class="clear">
                <div class="pos-rlt wrapper-sm b b-light r m-l-sm">
                  <span class="arrow left pull-up"></span>
                  <p class="m-b-none">Hi John, What's up...</p>
                </div>
                <small class="text-muted m-l-sm"><i class="fa fa-ok text-success"></i> 2 minutes ago</small>
              </div>
            </div>
            <div class="m-b">
              <a href class="pull-right thumb-xs avatar"><img src="/img/a3.jpg" class="img-circle" alt="..."></a>
              <div class="clear">
                <div class="pos-rlt wrapper-sm bg-light r m-r-sm">
                  <span class="arrow right pull-up arrow-light"></span>
                  <p class="m-b-none">Lorem ipsum dolor :)</p>
                </div>
                <small class="text-muted">1 minutes ago</small>
              </div>
            </div>
            <div class="m-b">
              <a href class="pull-left thumb-xs avatar"><img src="/img/a2.jpg" alt="..."></a>
              <div class="clear">
                <div class="pos-rlt wrapper-sm b b-light r m-l-sm">
                  <span class="arrow left pull-up"></span>
                  <p class="m-b-none">Great!</p>
                </div>
                <small class="text-muted m-l-sm"><i class="fa fa-ok text-success"></i>Just Now</small>
              </div>
            </div>
            <!-- / chat list -->
          </div>
        </div>
      </div>
      <div class="wrapper m-t b-t b-light">
        <form class="m-b-none">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Say something">
            <span class="input-group-btn">
              <button class="btn btn-default" type="button">SEND</button>
            </span>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!-- / aside right -->

  <!-- footer -->
  <div class="app-footer wrapper b-t bg-light">
    <span class="pull-right">{[{app.version}]} <a href ui-scroll="app" class="m-l-sm text-muted"><i class="fa fa-long-arrow-up"></i></a></span>
    &copy; 2015 Copyright.
  </div>
  <!-- / footer -->

</div>
<!-- JS -->

{{ javascript_include('vendors/jquery/dist/jquery.min.js') }}
<!-- angular -->

{{ javascript_include('vendors/angular/angular.min.js') }}
{{ javascript_include('vendors/angular-cookies/angular-cookies.min.js') }}
{{ javascript_include('vendors/angular-animate/angular-animate.min.js') }}
{{ javascript_include('be/js/angular/angular-ui-router.min.js') }}
{{ javascript_include('be/js/angular/angular-translate.js') }}
{{ javascript_include('be/js/angular/ngStorage.min.js') }}
{{ javascript_include('be/js/angular/ui-load.js') }}
{{ javascript_include('be/js/angular/ui-jq.js') }}
{{ javascript_include('be/js/angular/ui-validate.js') }}
{{ javascript_include('be/js/angular/ui-bootstrap-tpls.min.js') }}

{{ javascript_include('be/js/jquery/inputmask.js') }}
{{ javascript_include('vendors/moment/moment.js') }}
{{ javascript_include('vendors/angular-moment/angular-moment.min.js') }}
{{ javascript_include('vendors/angular-audio/app/angular.audio.js') }}
{{ javascript_include('be/js/angularjs-toaster/toaster.js') }}

<!-- xeditable -->
{{ javascript_include('vendors/angular-xeditable/dist/js/xeditable.min.js') }}

<!-- APP -->
{{ javascript_include('be/js/scripts/app.js') }}
{{ javascript_include('be/js/scripts/factory/factory.js') }}
{{ javascript_include('be/js/scripts/controllers/controllers.js') }}
{{ javascript_include('be/js/scripts/directives/directives.js') }}
{{ javascript_include('be/js/scripts/config.js') }}
{{ javascript_include('be/js/scripts/controllers/notification/notification.js') }}
{{ javascript_include('be/js/scripts/factory/notification/notification.js') }}
{{ javascript_include('be/js/scripts/factory/center/allintrosession.js') }}
{{ javascript_include('be/js/scripts/factory/center/allgroupclasssession.js') }}
{{ javascript_include('be/js/scripts/factory/center/allbeneplace.js') }}
{{ javascript_include('be/js/scripts/filter/htmlfilter.js') }}
<!-- CKeditor -->
{{ javascript_include('be/js/ckeditor/ckeditor.js') }}
{{ javascript_include('be/js/ckeditor/styles.js') }}

<!-- other plugins -->
<!-- ng upload -->
{{ javascript_include('vendors/ng-file-upload/ng-file-upload.min.js') }}
{{ javascript_include('vendors/ng-file-upload/ng-file-upload-shim.js') }}

<!-- angular-google-map -->
{{ javascript_include('vendors/angular-google-maps/dist/angular-google-maps.min.js') }}
{{ javascript_include('vendors/lodash/lodash.min.js') }}

<!-- xeditable -->
{{ javascript_include('vendors/angular-xeditable/dist/js/xeditable.min.js') }}

<!-- ngImgCrop -->
{{ javascript_include('vendors/ngImgCrop/compile/unminified/ng-img-crop.js') }}

<!-- HIGH CHARTS -->
<script src="https://code.highcharts.com/highcharts.src.js"></script>
{{ javascript_include('vendors/highcharts-ng/dist/highcharts-ng.min.js') }}
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.1.0/chosen.jquery.min.js"></script>
<script src="/vendors/angular-chosen/angular-chosen.js"></script>

<script>
 function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result)
                        .width(200)
                        .height(200);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
</script>


<!-- neil scripts -->
<script>
$('.phone_us').mask('(000) 000-0000');
</script>

<script type="text/ng-template" id="myTemplateWithData.html">
<p class="toast-title">{[{toaster.data}]}</p>
<p>{[{toaster.data2}]}</p>
</script>

<script type="text/ng-template" id="beneplaceView.html">
  <div ng-include="'/be/tpl/beneplaceView.html'"></div>
</script>

<script type="text/ng-template" id="groupsessionView.html">
  <div ng-include="'/be/tpl/groupsessionView.html'"></div>
</script>

<script type="text/ng-template" id="sessionView.html">
  <div ng-include="'/be/tpl/sessionView.html'"></div>
</script>

<script type="text/ng-template" id="contactReview.html">
  <div ng-include="'/be/tpl/contactReview.html'"></div>
</script>

</body>
</html>