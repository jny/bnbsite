{{ content() }}
<div class="bg-light lter b-b wrapper-md" ng-init="userid('<?php echo $username["bnb_userid"];?>')">
  <h1 class="m-n font-thin h3">Notification</h1>
  <a id="top"></a>
</div>
<div class="hbox hbox-auto-xs hbox-auto-sm">
  <div class="wrapper-md">
      <div class="row">
        <div class="col-sm-12">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Manage Notification 
              <a href ng-click="isread()"><span class="pull-right">Read <i class="fa fa-circle text-success"></i></span></a>
              <span class="pull-right">&nbsp;</span><span class="pull-right">&nbsp;</span><span class="pull-right">&nbsp;</span>
              <a href ng-click="isunread()"><span class="pull-right">Unread <i class="fa fa-circle text-warning"></i></span></a>
              <span class="pull-right">&nbsp;</span><span class="pull-right">&nbsp;</span><span class="pull-right">&nbsp;</span>
              <a href ng-click="isall()"><span class="pull-right">All <i class="fa fa-circle text-primary"></i></span></a>
            </div>
            <div class="panel-body">
               <alert ng-repeat="alert in alerts" type="{[{ alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
              <div class="row wrapper">
                <div class="col-sm-5 m-b-xs">
                  <form name="rEquired">
                    <div class="input-group">
                      <input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext" required>
                      <span class="input-group-btn">
                        <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)" ng-disabled="rEquired.$invalid">Go!</button>
                        <button type="button" class="btn btn-sm btn-bg btn-default" data-toggle="tooltip" data-placement="bottom" data-title="Refresh" data-original-title="" title="RESET" ng-click="resetsearch()"><i class="fa fa-refresh"></i></button>
                      </span>
                    </div>
                  </form>
                </div>
                <div class="line line-dashed b-b line-lg"></div>
                <div class="col-sm-5 m-b-xs">
                  Filter by
                  <div class="input-group">
                    <select ng-model="searchtext2" class="form-control m-b ng-pristine ng-valid ng-touched" ng-change="search(searchtext2)">
                      <option value="" style="display:none">Filter Center</option>
                      <option value="story">Testimonies</option>
                      <option value="message">Message</option>
                      <option value="introsession">1 on 1 intro session</option>
                      <option value="groupsession">1 group + 1 intro session</option>
                      <option value="workshop">workshop</option>
                    </select>
                  </div>
                </div>
              </div>

              <style type="text/css">
                .notidiv {
                  position: relative;
                  ...
                }
                .notidiv .close {
                  position: absolute;
                  top: 50%;
                  right: 14px;
                  z-index: 100;
                }
              </style>
              <div ng-repeat="list in nofiticationlist" class="notidiv">
                    <span class="close"><i class="fa fa-trash-o pull-right" ng-click="deletenotification(list.notificationid)"></i></span>
                    <a ng-if="list.notificationstatus == 0" href class="media list-group-item bg-light" ng-click="viewnotification(list.notificationid,list.itemid,list.type,list.centerid,list[0].uigo)">
                      <!-- <i class="fa fa-circle text-warning pull-right text-xs m-t-sm"></i> -->
                      <!-- <i class="fa fa-trash-o pull-right" ng-click="expression()"></i> -->
                      <i class="fa fa-circle pull-right text-warning"></i>
                      <span class="pull-left thumb-sm" ng-if="list.type == 'introsession'">
                        <!-- <img src="/img/a0.jpg" alt="..." class="img-circle"> -->
                        <i class="icon-user-follow font30"></i>
                      </span>
                      <span class="media-body block m-b-none" ng-if="list.type == 'introsession'">
                        {[{list.name}]}<br>
                        Enrolled to 1-on-1 Intro Session<br>
                        <i class="fa fa-clock-o"></i> <small class="text-muted">{[{list[0].timeago}]}</small>
                      </span>
                      <!-- end of introsession -->

                      <span class="pull-left thumb-sm" ng-if="list.type == 'groupsession'">
                        <!-- <img src="/img/a0.jpg" alt="..." class="img-circle"> -->
                        <i class="icon-users font30"></i>
                      </span>
                      <span class="media-body block m-b-none" ng-if="list.type == 'groupsession'">
                        {[{list.name}]}<br>
                        Enrolled to 1 Group Class + 1-on-1 Intro Session<br>
                        <i class="fa fa-clock-o"></i> <small class="text-muted">{[{list[0].timeago}]}</small>
                      </span>
                      <!-- end of groupsession -->

                      <span class="pull-left thumb-sm" ng-if="list.type == 'beneplace'">
                        <!-- <img src="/img/a0.jpg" alt="..." class="img-circle"> -->
                        <i class="icon-paper-plane font30"></i>
                      </span>
                      <span class="media-body block m-b-none" ng-if="list.type == 'beneplace'">
                        {[{list.name}]}<br>
                        Enrolled to Beneplace<br>
                        <i class="fa fa-clock-o"></i> <small class="text-muted">{[{list[0].timeago}]}</small>
                      </span>
                      <!-- end of beneplace -->

                      <span class="pull-left thumb-sm" ng-if="list.type == 'message'">
                        <!-- <img src="/img/a0.jpg" alt="..." class="img-circle"> -->
                        <i class="icon-envelope font30"></i>
                      </span>
                      <span class="media-body block m-b-none" ng-if="list.type == 'message'">
                        {[{list.name}]}<br>
                        Sent you a new message<br>
                        <i class="fa fa-clock-o"></i> <small class="text-muted">{[{list[0].timeago}]}</small>
                      </span>
                      <!-- end of message -->

                      <span class="pull-left thumb-sm" ng-if="list.type == 'workshop'">
                        <!-- <img src="/img/a0.jpg" alt="..." class="img-circle"> -->
                        <i class="icon-users font30"></i>
                      </span>
                      <span class="media-body block m-b-none" ng-if="list.type == 'workshop'">
                        {[{list.name}]}<br>
                        Enrolled to Workshop<br>
                        <i class="fa fa-clock-o"></i> <small class="text-muted">{[{list[0].timeago}]}</small>
                      </span>
                      <!-- end of workshop -->
                      <span class="pull-left thumb-sm" ng-if="list.type == 'story'">
                        <!-- <img src="/img/a0.jpg" alt="..." class="img-circle"> -->
                        <i class="icon-bubbles font30"></i>
                      </span>
                      <span class="media-body block m-b-none" ng-if="list.type == 'story'">
                        {[{list.name}]}<br>
                        Sent you Testimonies<br>
                        <i class="fa fa-clock-o"></i> <small class="text-muted">{[{list[0].timeago}]}</small>
                      </span>
                      <!-- end of story -->
                    </a>

                    

                    <a ng-if="list.notificationstatus == 1" href class="media list-group-item" ng-click="viewnotification(list.notificationid,list.itemid,list.type,list.centerid,list[0].uigo)">
                     
                      <i class="fa fa-circle pull-right text-success"></i>
                      <span class="pull-left thumb-sm" ng-if="list.type == 'introsession'">
                        <!-- <img src="/img/a0.jpg" alt="..." class="img-circle"> -->
                        <i class="icon-user-follow font30"></i>
                      </span>
                      <span class="media-body block m-b-none" ng-if="list.type == 'introsession'">
                        {[{list.name}]}<br>
                        Enrolled to 1-on-1 Intro Session<br>
                        <i class="fa fa-clock-o"></i> <small class="text-muted">{[{list[0].timeago}]}</small>
                      </span>
                      <!-- end of introsession -->

                      <span class="pull-left thumb-sm" ng-if="list.type == 'groupsession'">
                        <!-- <img src="/img/a0.jpg" alt="..." class="img-circle"> -->
                        <i class="icon-users font30"></i>
                      </span>
                      <span class="media-body block m-b-none" ng-if="list.type == 'groupsession'">
                        {[{list.name}]}<br>
                        Enrolled to 1 Group Class + 1-on-1 Intro Session<br>
                        <i class="fa fa-clock-o"></i> <small class="text-muted">{[{list[0].timeago}]}</small>
                      </span>
                      <!-- end of groupsession -->

                      <span class="pull-left thumb-sm" ng-if="list.type == 'beneplace'">
                        <!-- <img src="/img/a0.jpg" alt="..." class="img-circle"> -->
                        <i class="icon-paper-plane font30"></i>
                      </span>
                      <span class="media-body block m-b-none" ng-if="list.type == 'beneplace'">
                        {[{list.name}]}<br>
                        Enrolled to Beneplace<br>
                        <i class="fa fa-clock-o"></i> <small class="text-muted">{[{list[0].timeago}]}</small>
                      </span>
                      <!-- end of beneplace -->

                      <span class="pull-left thumb-sm" ng-if="list.type == 'message'">
                        <!-- <img src="/img/a0.jpg" alt="..." class="img-circle"> -->
                        <i class="icon-envelope font30"></i>
                      </span>
                      <span class="media-body block m-b-none" ng-if="list.type == 'message'">
                        {[{list.name}]}<br>
                        Sent you a new message<br>
                        <i class="fa fa-clock-o"></i> <small class="text-muted">{[{list[0].timeago}]}</small>
                      </span>
                      <!-- end of message -->

                      <span class="pull-left thumb-sm" ng-if="list.type == 'workshop'">
                        <!-- <img src="/img/a0.jpg" alt="..." class="img-circle"> -->
                        <i class="icon-users font30"></i>
                      </span>
                      <span class="media-body block m-b-none" ng-if="list.type == 'workshop'">
                        {[{list.name}]}<br>
                        Enrolled to Workshop<br>
                        <i class="fa fa-clock-o"></i> <small class="text-muted">{[{list[0].timeago}]}</small>
                      </span>
                      <!-- end of workshop -->
                      <span class="pull-left thumb-sm" ng-if="list.type == 'story'">
                        <!-- <img src="/img/a0.jpg" alt="..." class="img-circle"> -->
                        <i class="icon-bubbles font30"></i>
                      </span>
                      <span class="media-body block m-b-none" ng-if="list.type == 'story'">
                        {[{list.name}]}<br>
                        Sent you Testimonies<br>
                        <i class="fa fa-clock-o"></i> <small class="text-muted">{[{list[0].timeago}]}</small>
                      </span>
                      <!-- end of story -->
                    </a>
                   
                  </div>
                  <!-- <a ng-click="incrementLimit()" href class="media bg-light" style="height:20px;padding-top:1px;overflow:hidden;">
                  <center>
                    <span style="padding:5px;" ng-show="notiloadertext">loadmore...</span>
                    <span class="ouro ouro2" style="padding-top:1px;overflow:hidden;" ng-show="notiloader">
                      <span class="left"><span class="anim"></span></span>
                      <span class="right"><span class="anim"></span></span>
                    </span>
                  </center>
                  </a> -->

            </div> <!-- end of panel body -->


          </div>
        </div>

      </div>

      <div class="row">
        <div class="panel-body">
            <footer class="panel-footer text-center bg-light lter">
              <pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
            </footer>
        </div>
      </div>

      <!-- <div class="row">
        <div class="panel-body">
            <footer class="panel-footer text-center bg-light lter">
              <pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
            </footer>
        </div>
      </div> -->

  </div>
</div>

