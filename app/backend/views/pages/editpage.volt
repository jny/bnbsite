{{ content() }}

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Edit Page</h1>
  <a id="top"></a>
</div>
<form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="savePage(page)" name="formpage">
<fieldset ng-disabled="isSaving">
  <div class="wrapper-md">
    <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>

      <div class="row">

        <div class="col-sm-9">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Page Information
            </div>
              <div class="panel-body">
              <input type="hidden" id="title" name="title" ng-model="page.pageid" >
                Title
                <input type="text" id="title" name="title" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="page.title" required="required" ng-keyup="onpagetitle(page.title)">
                <br>
                <b>Page Slugs: </b>

                
                <input type="text" class="form-control" ng-model="page.slugs" required>
                <!-- <span ng-bind="page.slugs"></span> -->

                <div class="line line-dashed b-b line-lg"></div>

                 Meta Title 
                <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="page.metatitle" required="required">
                <br>
                <div class="line line-dashed b-b line-lg"></div>
                Meta Tags
                <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="page.metatags" required="required">
                <span class="help-block m-b-none">Use comma (,) for multiple meta tags. E.g. (initial awakening, sedona healing, dahn yoga)</span>
                <br>
                <div class="line line-dashed b-b line-lg"></div>

                Meta Description
                <!-- <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="page.metadesc"> -->
                <textarea class="form-control resize_vert" ng-model="page.metadesc" rows="3"></textarea>
                <br>

                <div class="line line-dashed b-b line-lg"></div>

                Body Content
                <textarea class="ck-editor" ng-model="page.body" required="required"></textarea>
              </div>
          </div>
        </div>

        <div class="col-sm-3">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Sidebar Switch
            </div>

              <div class="panel-body">

                <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="a" ng-model="page.layout" value="1" ng-click="radio('1')">
                    <i></i>
                    Full Screen Page Layout
                  </label>
                </div>

                <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="a" ng-model="page.layout" value="2" ng-click="radio('2')">
                    <i></i>
                    Page with Sidebar Layout
                  </label>
                </div>

               
              
              </div>

          </div>
          
        </div>

        <div class="col-sm-3" ng-show="sidebar">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Left Sidebar
            </div>

              <div class="panel-body" ng-init="page.leftbar = 0" >
                <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="b" value="0" ng-model="page.leftbar">
                    <i></i>
                    None
                  </label>
                </div>

                <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="b" value="1" ng-model="page.leftbar">
                    <i></i>
                    About
                  </label>
                </div>

                <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="b" value="2" ng-model="page.leftbar">
                    <i></i>
                    Classes
                  </label>
                </div>

                <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="b" value="3" ng-model="page.leftbar">
                    <i></i>
                    Workshop
                  </label>
                </div>

                <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="b" value="4" ng-model="page.leftbar">
                    <i></i>
                    Wellness at Work
                  </label>
                </div>

                <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="b" value="5" ng-model="page.leftbar">
                    <i></i>
                    Communication Action
                  </label>
                </div>

                <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="b" ng-value="6" ng-model="page.leftbar">
                    <i></i>
                    Franchising
                  </label>
                </div>

                <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="b" ng-value="8" ng-model="page.leftbar">
                    <i></i>
                    Our Company
                  </label>
                </div>

                <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="b" ng-value="9" ng-model="page.leftbar">
                    <i></i>
                    Personal Services
                  </label>
                </div>

                <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="b" ng-value="7" ng-model="page.leftbar">
                    <i></i>
                    Miscellaneous
                  </label>
                </div>
               
              </div>


          </div>
        </div>


        <div class="col-sm-3" ng-show="sidebar">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Right Sidebar
            </div>


              <div class="panel-body">
                <div class="checkbox">
                  <label class="i-checks">
                    <input type="checkbox" name="r1" ng-click="rr1(r1)" ng-model = "r1">
                    <i></i>
                    Try our Classes Right Sidebar
                  </label>
                </div>

                <div class="checkbox">
                  <label class="i-checks">
                    <input type="checkbox" name="r2" ng-click="rr2(r2)" ng-model = "r2">
                    <i></i>
                    Initial Awakening Right Sidebar
                  </label>
                </div>

                <div class="checkbox">
                  <label class="i-checks">
                    <input type="checkbox" name="r3" ng-click="rr3(r3)" ng-model = "r3">
                    <i></i>
                    Success Stories Right Sidebar
                  </label>
                </div>

                <div class="checkbox">
                  <label class="i-checks">
                    <input type="checkbox" name="r4" ng-click="rr4(r4)" ng-model = "r4">
                    <i></i>
                    Related Articles Right Sidebar
                  </label>
                </div>

                <div class="checkbox">
                  <label class="i-checks">
                    <input type="checkbox" name="r5" ng-click="rr5(r5)" ng-model = "r5">
                    <i></i>
                    Popular Features Right Sidebar
                  </label>
                </div>
              
              </div>


          </div>
        </div>


        <div class="col-sm-9">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Facebook Custom Audience Pixel Code
            </div>
            <div class="panel-body">
              <textarea class="form-control" rows="4" placeholder="Paste your Code Here" ng-model="page.fbcapc"></textarea>
            </div>
          </div>
        </div>


      </div>


      <div class="row">
        <div class="panel-body">
            <footer class="panel-footer text-right bg-light lter">
              <a ui-sref="managepage" class="btn btn-default"> Cancel </a>
              <button type="submit" class="btn btn-success" ng-disabled="formpage.$invalid">Submit</button>
            </footer>
        </div>
      </div>


      <div  class="row">
        <div class="col-sm-12">
          <div class="panel panel-default">
              <div class="panel-heading font-bold">
                Image Gallery
              </div>




                <div class="panel-body">
                  <alert ng-repeat="alert in alertss" type="{[{alert.type }]}" close="closeAlerts($index)">{[{ alert.msg }]}</alert>
                  <div class="loader" ng-show="imageloader">
                    <div class="loadercontainer">
                      
                      <div class="spinner">
                        <div class="rect1"></div>
                        <div class="rect2"></div>
                        <div class="rect3"></div>
                        <div class="rect4"></div>
                        <div class="rect5"></div>
                      </div>
                      Uploading your images please wait...

                    </div>
                    
                  </div>

                  <div ng-show="imagecontent">

                    <div class="col-sml-12">
                      <div class="dragdropcenter">
                        <div ngf-drop ngf-select ng-model="files" class="drop-box" 
                        ngf-drag-over-class="dragover" ngf-multiple="true" ngf-allow-dir="true"
                        accept="image/*,application/pdf">Drop images here or click to upload</div>
                      </div>
                    </div>

                    <div class="line line-dashed b-b line-lg"></div>

                    <div class="col-sm-3" ng-repeat="data in imagelist">
                      <input type="text" id="title" name="title" class="form-control" value="<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{data.filename}]}" onClick="this.setSelectionRange(0, this.value.length)">
                      <div class="imagegallerystyle" style="background-image: url('<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{data.filename}]}');">
                      </div>
                    </div>

                  </div>


                </div>

          </div>
        </div>

      </div>

  </div>
</fieldset>
</form>