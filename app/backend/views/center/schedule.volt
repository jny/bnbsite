<script type="text/ng-template" id="scheduleedit.html">
  <div ng-include="'/be/tpl/scheduleedit.html'"></div>
</script>

<script type="text/ng-template" id="scheduleAdd.html">
  <div ng-include="'/be/tpl/scheduleAdd.html'"></div>
</script>

<script type="text/ng-template" id="scheduleDelete.html">
  <div ng-include="'/be/tpl/scheduleDelete.html'"></div>
</script>


  <!-- header -->
  <div class="wrapper bg-light lter b-b" style="height:61px;">
    <div class="btn-group m-r-sm">
      <h1 class="m-n font-thin h3">Schedule</h1>
    </div>
     <div class="btn-group m-r-sm">
      <a ng-click="addnewschedule()" class="btn btn-sm btn-success w-xm font-bold">Add New Class Schedule</a>
    </div>
  </div>


<fieldset ng-disabled="isSaving">


  <div class="wrapper-md">
    

    <div class="row">
      <div class="col-sm-12"><alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert></div>
      <div class="col-sm-12">
      
        <div class="panel panel-primary">
          
          <div class="panel-heading font-bold">
            Schedule
          </div>

          <div class="panel-body">

            

            <div class="table-responsive">
                    <table class="table table-striped b-t b-light">
                        <thead class="tdborderbots">
                            <tr class="tdborderbot">
                               
                                <th style="width:12.2%" class="tdborderbots">Time</th>
                                <th style="width:12.2%" class="tdborderbots">Monday</th>
                                <th style="width:12.2%" class="tdborderbots">Tuesday</th>
                                <th style="width:12.2%" class="tdborderbots">Wednesday</th>
                                <th style="width:12.2%" class="tdborderbots">Thursday</th>
                                <th style="width:12.2%" class="tdborderbots">Friday</th>
                                <th style="width:12.2%" class="tdborderbots">Saturday</th>
                                <th style="width:12.2%" class="tdborderbots">Sunday</th>
                                <th style="width:12.2%" class="tdborderbots">Action</th>
                                
                            </tr>
                        </thead>
                        <tbody>

                            <tr ng-repeat="list in schedlelist">
                                <td class="schedtd tdborder">
                                   {[{list.starttime}]} {[{list.starttimeformat}]} ~ {[{list.endtime}]} {[{list.endtimeformat}]}
                                </td>
                                <td class="schedtd tdborder">
                                  <span>{[{list.mo}]}</span>
                                </td>
                                <td class="schedtd tdborder">
                                  <span>{[{list.tu}]}</span>
                                </td>
                                <td class="schedtd tdborder">
                                  <span>{[{list.we}]}</span>
                                </td>
                                <td class="schedtd tdborder">
                                  <span>{[{list.th}]}</span>
                                </td>
                                <td class="schedtd tdborder">
                                  <span>{[{list.fr}]}</span>
                                </td>
                                <td class="schedtd tdborder">
                                  <span>{[{list.sa}]}</span>
                                </td>
                                <td class="schedtd tdborder">
                                  <span>{[{list.su}]}</span>
                                </td>
                                <td class="schedtd tdborder">
                                  <a href="" ng-click="editsession(list.classid)"><span class="label bg-warning" >Edit</span></a>
                                  <a href="" ng-click="delete(list.classid)"> <span class="label bg-danger">Delete</span></a>
                                </td>
                            </tr>
                          
                        </tbody>
                    </table>
                </div>
          
          </div> <!-- END OF panel Body-->


      </div>
    </div>



  </div> <!-- end of row -->


  
</div>
</fieldset>

<script language="javascript">
  function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;
    return true;
  }
</script>