 <style type="text/css">
  html, body, #map_canvas {
    height: 500px;
    width: 100%;
    margin: 0px;
  }

  #map_canvas {
    position: relative;
  }

  .angular-google-map-container {
    position: absolute;
    top: 0;
    bottom: 0;
    right: 0;
    left: 0;
  }
  #pac-input {
    background-color: #fff;
    font-family: Roboto;
    font-size: 15px;
    font-weight: 300;
    margin-left: 12px;
    margin: 25px 0 0 0;
    padding: 0 11px 0 13px;
    text-overflow: ellipsis;
    width: 80%;
  }

  .inputBox {
    padding: 4px;
    border: 1px solid #cad5e0;
  }

  #profile_pic {
    display:none;
  }
  .label_profile_pic {
    width:100px; height:35px;
    background:rgba(0, 0, 0, .6);
    color:white;
    font-weight:500;
    font-size:16px;
    text-align:center;
    position:Relative; top:-35px;
    clear:both;
    padding:5px 0 0 0;
    display:table-cell;
    cursor:pointer;
  }
  #blah:hover ~ .label_profile_pic, .label_profile_pic:hover {
    visibility:visible;
  }

.socialcolumn {

}
</style>

<script type="text/ng-template" id="searchbox.tpl.html">
  <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" id="pac-input" placeholder="Search Box">
</script>

<script type="text/ng-template" id="addSocialLink.html">
  <div ng-include="'/be/tpl/addSocialLink.html'"></div>
</script>

<script type="text/ng-template" id="deleteSocialLink.html">
  <div ng-include="'/be/tpl/deleteSocialLink.html'"></div>
</script>

<!-- header -->
<div class="wrapper bg-light lter b-b" style="height:61px;">
  <div class="btn-group m-r-sm">
    <h1 class="m-n font-thin h3">Map & Direction</h1>
    <a id="top"></a>
  </div>
</div>

<fieldset ng-disabled="isSaving" class="fade-in-up">
  <div class="wrapper-md">
    <div class="row">
      <div class="col-md-12 wrapper-md" >
        <div class="panel panel-default">
          <form class="bs-example form-horizontal" name="formdesc" method="POST" ng-submit="savedescription(centerdesc.centerdesc)" >
            <div class="panel-heading font-bold">
              Center Description
            </div>
            <div class="panel-body">

              <alert ng-show="descalert" type="success" close="desccloseAlert($index)">Successfully Saved!</alert>

              <textarea class="form-control resize_vert" ng-model="centerdesc.centerdesc" row="6"></textarea>
              <br>
              <button type="submit" class="btn btn-success btn-md pull-right" title="Save">
                Save
              </button>
            </div> <!-- end of panel-body -->
          </form>
        </div>
      </div>

      <div class="col-sm-12">
        <div class="panel panel-default">
          <div class="panel-heading font-bold">
            Center Location <em>(location is autosave upon dragging pin)</em>
          </div>

          <div class="panel-body" ng-init="center.centertype = 1">

            <div id="map_canvas">
                <ui-gmap-google-map center="map.center" zoom="map.zoom" draggable="true" options="options">
                <ui-gmap-search-box template="searchbox.template" events="searchbox.events"></ui-gmap-search-box>
                <ui-gmap-marker coords="marker.coords" options="marker.options" events="marker.events" idkey="marker.id">
                </ui-gmap-marker>
                </ui-gmap-google-map>
               
              <div ng-cloak>
                <ul>
                  <li>coords update ctr: {[{coordsUpdates}]}</li>
                  <li>dynamic move ctr: {[{dynamicMoveCtr}]}</li>
                </ul>
              </div>
            </div>
               
          </div>
        </div>
      </div> <!-- end of col sm 12 --> 
    </div> <!-- end of row -->
  </div> <!-- end of wrapper-md -->
</fieldset>

<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                .attr('src', e.target.result)
                .width(100)
                .height(100);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

</script>

<script type="text/javascript">

  var options =  {onKeyPress: function(cep, e, field, options){

    $('#phone').mask('(000) 000-0000');
  }};
  $( document ).ready(function() {
    $('#phone').mask('(000) 000-0000', options);
  });

</script>


