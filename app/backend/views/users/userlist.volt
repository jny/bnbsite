{{ content() }}
<script type="text/ng-template" id="userDelete.html">
  <div ng-include="'/be/tpl/userDelete.html'"></div>
</script>
<script type="text/ng-template" id="userUpdate.html">
  <div ng-include="'/be/tpl/userUpdate.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">User List</h1>
  <a id="top"></a>
</div>
<div class="wrapper-md" ng-controller="UserListCtrl">
<alert ng-repeat="alert in alerts" type="{[{ alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
  <div class="panel panel-default">
    <div class="panel-heading">
      Manage Users
    </div>
    <div class="row wrapper">
      <div class="col-sm-6">
        <div class="input-group">
          <span class="input-group-addon">
            Search
          </span>
          <input class="form-control" placeholder="Search Username/First Name/Last Name" type="text" name="searchtext" ng-model="searchtext" ng-change="search(searchtext)">
          <!-- <span class="input-group-btn">
            <button class="btn btn-sm btn-default" type="button" >Go!</button>
          </span> -->
        </div>
      </div>
    </div>
    <div class="table-responsive">
     <!--  <input type="hidden" ng-init='userdata = {{ data }}'> -->
      <table class="table table-striped b-t b-light">
        <thead>
          <tr>
<!--             <th style="width:20px;">
              <label class="i-checks m-b-none">
                <input type="checkbox"><i></i>
              </label>
            </th> -->
            <th>Name</th>
            <th>Email</th>
            <th>Username</th>
            <th>Status</th>
            <th>User Role</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="user in data.data" ng-if="user.username != 'superagent'">
            <!-- <td><label class="i-checks m-b-none"><input name="post[]" type="checkbox"><i></i></label></td> -->
            <td>{[{ user.first_name }]} {[{ user.last_name }]}</td>
            <td>{[{ user.email }]}</td>
            <td>{[{ user.username }]}</td>
            <td>
              <div class="pagestatuscontent fade-in-out">
                <span class="bg-success btn btn-xs" ng-if="user.status == 1">Active</span>
                <span class="btn btn-xs bg-danger" ng-if="user.status == 0">Inactive</span>
              </div>
              <div class="checkstatuscontent">
                <label class="i-switch bg-info m-t-xs m-r">
                  <input type="checkbox" ng-true-value="'1'" ng-false-value="'0'" ng-model="user.status" ng-click="setstatus(user.id,user.status)">
                  <i></i>
                </label>  
              </div>
              <!-- <div id="{[{user.id}]}" class="wew"><spand class="fade-in-out"><i class="fa fa-check"></i></spand></div> -->
            </td>
            <td>
              <span ng-class="">{[{ user.task }]}</span></td>
            <td>
              <button ng-click="update(user.id)" class="btn m-b-xs btn-xs btn-info btn-addon">Edit</button>
              <button ng-click="delete(user.id)" class="btn m-b-xs btn-xs btn-danger btn-addon">Delete</button>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <footer class="panel-footer">
      <div class="row">
        <!-- <div class="col-sm-4 text-left text-center-xs">                
          <ul class="pagination">
            <li id="DataTables_Table_0_previous" tabindex="0" aria-controls="DataTables_Table_0" ng-class="{ 'disabled' : data.index == 1  }" class="paginate_button previous">
              <a class="fa fa-chevron-left" href="" ng-click="numpages(data.before)"></a>
            </li>
            <li ng-repeat="page in data.pages" ng-class="{ 'active' : data.index == page.num  }" class="paginate_button">
              <a ng-click="numpages(page.num)"> {[{ page.num }]}</a>
            </li>
            <li id="DataTables_Table_0_next" tabindex="0" aria-controls="DataTables_Table_0" ng-class="{ 'disabled' : data.index == data.last  }" class="paginate_button next">
              <a href="" class="fa fa-chevron-right" ng-click="numpages(data.next)"> </a>
            </li>
          </ul>
        </div> -->
        <div class="col-sm-4 text-center">
          <small class="text-muted inline m-t-sm m-b-sm">showing 20-30 of 50 items</small>
        </div>

        <pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>

      </div>
    </footer>
  </div>
</div>